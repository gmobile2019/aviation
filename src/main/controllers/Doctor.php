<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Administrator
 *
 * @author Manuel
 */
class Doctor extends CI_Controller{
    
    function __construct() {
        parent::__construct();
       
        $this->load->library('upload');
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $this->data['title']="Home";
        $this->data['content']='doctor/dashboard';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function medical_applications(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        if ($this->input->post('applicationRef')) {
               $key['applicationRef'] = $this->input->post('applicationRef');
               $this->data['applicationRef']=$this->input->post('applicationRef');
        }

        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['applicationRef'] = $exp[1];
            $this->data['applicationRef']=$key['applicationRef'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['status'] = $exp[7];
            $this->data['status']=$key['status'];
            

            $docType = $exp[9];
            $this->data['docType']=$docType;
        }

        $applicationRef =$key['applicationRef'];
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];
        
        if($docType == 1){
            
            $data=$this->User_model->medical_apps($applicationRef,NULL,$start,$end,$status,$this->session->userdata('institution'));
            require_once 'reports/medical_applications_pdf.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Doctor/index/ref_".$key['applicationRef']."_strt_".$key['start']."_end_".$key['end']."_status_".$key['status']."/";
        $config["total_rows"] =$this->User_model->application_info_count($applicationRef,NULL,$start,$end,$status,$this->session->userdata('institution'));
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->User_model->application_info($applicationRef,NULL,$start,$end,$status,$this->session->userdata('institution'),$page,$limit);
        
        $this->data['title']="Applications";
        $this->data['content']='doctor/applications';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function applicationDetails($applicationRef,$export){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $bg_details=$this->User_model->background_details($applicationRef,NULL);
        $mh_details=$this->User_model->medicalHistory_details($applicationRef,NULL);
        $oc_details=$this->User_model->occupation_details($applicationRef,NULL);
        $ph_details=$this->User_model->personalhealth_details($applicationRef,NULL);
        $ac_details=$this->User_model->aircraft_details($applicationRef,NULL);
        $apm_details=$this->User_model->appearanceMeasurement_details($applicationRef);
        $py_details=$this->User_model->physical_details($applicationRef);
        $vs_details=$this->User_model->visual_details($applicationRef);
        $ey_details=$this->User_model->eye_details($applicationRef);
        $au_details=$this->User_model->audio_details($applicationRef);
        $ot_details=$this->User_model->other_details($applicationRef);
        $examination=$this->User_model->medical_applications($applicationRef,NULL);
        $bs_details=$this->User_model->registrationInfo(NULL,$examination[0]->applicant);
            
        if($export == 'export'){
            
            require_once 'reports/medical_application_details_pdf.php';
        }
        
        $this->data['bs_details']=$bs_details;
        $this->data['bg_details']=$bg_details;
        $this->data['mh_details']=$mh_details;
        $this->data['oc_details']=$oc_details;
        $this->data['ph_details']=$ph_details;
        $this->data['ac_details']=$ac_details;
        $this->data['apm_details']=$apm_details;
        $this->data['py_details']=$py_details;
        $this->data['vs_details']=$vs_details;
        $this->data['ey_details']=$ey_details;
        $this->data['au_details']=$au_details;
        $this->data['ot_details']=$ot_details;
        $this->data['examination']=$examination;
        $this->data['appRef']=$applicationRef;
        $this->data['title']="Application Details";
        $this->data['content']='doctor/applicationDetails';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function medical_certificate($applicationRef,$export){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $examination=$this->User_model->medical_applications($applicationRef,NULL);
        
        if($examination[0]->status <> 'certified'){
            
            redirect('Doctor/medical_applications','refresh');
        }
        
        $ac_details=$this->User_model->aircraft_details($applicationRef,NULL);
        $apm_details=$this->User_model->appearanceMeasurement_details($applicationRef);
        $ot_details=$this->User_model->other_details($applicationRef);
        
        $bs_details=$this->User_model->registrationInfo(NULL,$examination[0]->applicant);
        $lType=$this->User_model->getlicenceTypes($ac_details[0]->licenceType);
        $examination_date=explode(" ",$examination[0]->attendedOn);
        $age=$this->User_model->dateDifference($examination_date[0],$bs_details[0]->dob)->y;
            if($lType[0]->certClass === 'Class 1'){
                
                if($age >= 40){
                    
                    $validity='6 months';
                    
                }else{
                    $validity='12 months';
                }
            }else if($lType[0]->certClass === 'Class 2'){
                
                if($ac_details[0]->licenceType == 7){
                    
                    $validity='12 months';
                    
                }else if( $ac_details[0]->licenceType <> 7 && $age >= 40){
                    
                     $validity='12 months';
                     
                }else{
                    
                     $validity='24 months';
                }
            }else{
                
                if($age >= 40){
                    
                    $validity='12 months';
                    
                }else{
                    $validity='24 months';
                }
                
            }
            
            if($age < 30){
                    
                $ecg_validity='5 years';

            }else if($age >= 30 && $age < 50){
                
                $ecg_validity='2 years';
                
            }else{
                
                $ecg_validity='1 year';
                
            }
            
            if($age < 40){
                    
                $audio_validity='5 years';

            }else{
                
                $audio_validity='2 years';
                
            }
            
        $ecg=$this->User_model->getLastEcgExamination($examination[0]->applicant);  
        $audio=$this->User_model->getLastAudioExamination($examination[0]->applicant);
        $doctor=$this->SuperAdministration_model->get_member_info(NULL,$examination[0]->attendedBy);
        
        if($export === 'export'){
            
            require_once 'reports/medical_certificate_pdf.php';
        }
        
        $this->data['bs_details']=$bs_details;
        $this->data['ac_details']=$ac_details;
        $this->data['apm_details']=$apm_details;
        $this->data['ot_details']=$ot_details;
        $this->data['validity']=$validity;
        $this->data['ecg']=$ecg;
        $this->data['audio']=$audio;
        $this->data['ecg_validity']=$ecg_validity;
        $this->data['audio_validity']=$audio_validity;
        $this->data['examination']=$examination;
        $this->data['appRef']=$applicationRef;
        $this->data['lType']=$lType;
        $this->data['doctor']=$doctor;
        $this->data['title']="Medical Certificate";
        $this->data['content']='doctor/medical_certificate';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function retrieveNewApplication(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $bg_details=NULL;
        $mh_details=NULL;
        $oc_details=NULL;
        $ph_details=NULL;
        $ac_details=NULL;
        $examination=NULL;
        $bs_details=NULL;
        
        if($this->input->post('retrieve')){
            
            $applicationRef=$this->input->post('applicationRef');
            
            if($applicationRef <> NULL){
                
                $bg_details=$this->User_model->background_details($applicationRef,NULL);
                $mh_details=$this->User_model->medicalHistory_details($applicationRef,NULL);
                $oc_details=$this->User_model->occupation_details($applicationRef,NULL);
                $ph_details=$this->User_model->personalhealth_details($applicationRef,NULL);
                $ac_details=$this->User_model->aircraft_details($applicationRef,NULL);
                $examination=$this->User_model->medical_applications($applicationRef,NULL,NULL,NULL,'complete');
                $bs_details=$this->User_model->registrationInfo(NULL,$examination[0]->applicant);
            }
            
            $this->data['bg_details']=$bg_details;
            $this->data['mh_details']=$mh_details;
            $this->data['oc_details']=$oc_details;
            $this->data['ph_details']=$ph_details;
            $this->data['ac_details']=$ac_details;
            $this->data['bs_details']=$bs_details;
            $this->data['examination']=$examination;
            $this->data['applicationRef']=$applicationRef;
        }
        
        $this->data['content']='doctor/appDetails';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function examine($applicationRef){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $applicant=$this->User_model->medical_applications($applicationRef,NULL,NULL,NULL,NULL);
        
        if($applicant[0]->status <> 'complete' && $applicant[0]->status <> 'onprogress'){
            
            redirect('Doctor/medical_applications','refresh');
        }
        
        
        $bs_details=$this->User_model->registrationInfo(NULL,$applicant[0]->applicant);
        $this->data['bs_details']=$bs_details;
        
        $ac_details=$this->User_model->aircraft_details($applicationRef,NULL);
        $this->data['ac_details']=$ac_details;
        
        $lType=$this->User_model->getlicenceTypes($ac_details[0]->licenceType);
        $this->data['lType']=$lType;
        
        if($this->User_model->appearanceMeasurement_details($applicationRef) == NULL){
           
            $this->data['next']="physical";
        }else if($this->User_model->physical_details($applicationRef) == NULL){
            
            $this->data['next']="physicalDetailed";
        }else if($this->User_model->visual_details($applicationRef) == NULL){
            
            $this->data['next']="visual";
        }else if($this->User_model->eye_details($applicationRef) == NULL){
            
            $this->data['examination']=$this->User_model->medical_applications(NULL,$applicant[0]->applicant,NULL,NULL,'certified');
            $this->data['next']="eye";
        }else if($this->User_model->audio_details($applicationRef) == NULL){
            
            $this->data['next']="audio";
        }else if($this->User_model->other_details($applicationRef) == NULL){
            
            $this->data['next']="other";
        }else{
            
            $this->data['next']="approval";
        }
        
        $this->data['applicant']=$applicant[0]->applicant;
        $this->data['applicationRef']=$applicationRef;
        $this->data['content']='doctor/examine';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function completeExamination($applicationRef){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $applicant=$this->User_model->medical_applications($applicationRef,NULL,NULL,NULL,NULL);
        if($applicant[0]->status <> 'complete' && $applicant[0]->status <> 'onprogress'){
            
            redirect('Doctor/medical_applications','refresh');
        }
        
        $this->form_validation->set_rules('certStatus','Certification Status','required|xss_clean');
        $this->form_validation->set_rules('certStatusRemark','Certification Remark','xss_clean');
        $this->form_validation->set_rules('certClass','Class','required|xss_clean');
           
        if ($this->form_validation->run() == TRUE){

            $certStatus=$this->input->post('certStatus');
            $certStatusRemark=$this->input->post('certStatusRemark');
            $certClass=$this->input->post('certClass');

            $data=array(
                'status'=>$certStatus,
                'certRemark'=>$certStatusRemark<>null?$certStatusRemark:null,
                'certClass'=>$certClass,
                'attendedBy'=>$this->session->userdata('username'),
                'attendedOn'=>date('Y-m-d H:i:s'),
            );


            if($this->User_model->save_application($data,$applicationRef)){

                redirect('Doctor/medical_applications','refresh');
            }
        }
            
            redirect('Doctor/examine','refresh');
    }
    
    function profile(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $this->data['title']="User Profile";
        $this->data['data']=$this->SuperAdministration_model->current_user_info();
        $this->data['content']='doctor/profile';
        $this->load->view('doctor/templateNew',$this->data);
    }
    
    function save_profile(){
       
        $dbField=$this->input->post('name');
        $dbValue=trim($this->input->post('value'));
       
        if($dbValue == NULL || $dbValue == ''){
            
            echo 'empty field data';
            exit;
        }
        
        if($dbField == 'fname'){
            
            $dbField='first_name';
        }
        
        if($dbField == 'mname'){
            
            $dbField='middle_name';
        }
        
        if($dbField == 'surname'){
            
            $dbField='last_name';
        }
        
        if($dbField == 'mobile'){
            
            $dbField='msisdn';
        }
        
        $account=array(
            $dbField=>$dbValue,
            'modifiedby'=>$this->session->userdata('user_id'),
            'modifiedon'=>date('Y-m-d H:i:s')
        );
        
        
        $sve=$this->SuperAdministration_model->save_accountDetails($account,$this->session->userdata('user_id'));
        
        if(!$sve){
           
            echo "Error Saving Data!";
            exit;
        }
        
        echo "Data Successfully Saved!";
        exit;
    }
    
    function save_appearanceMeasurement(){
        
         $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'pHeight'=>$this->input->post('pHeight'),
            'pWeight'=>$this->input->post('pWeight'),
            'pChestInsp'=>$this->input->post('pChestInsp'),
            'pChestExp'=>$this->input->post('pChestExp'),
            'pWaist'=>$this->input->post('pWaist'),
            'pMSTD'=>$this->input->post('pMSTD'),
            'hairColor'=>$this->input->post('hairColor'),
            'eyesColor'=>$this->input->post('eyesColor'),
            'physicalImpression'=>$this->input->post('physicalImpression'),
            'attendedBy'=>$this->session->userdata('username'),
            'applicant'=>$this->input->post('applicant')
        );
        
        $id=$this->User_model->appearanceMeasurement_details($this->input->post('applicationRef'));
        
        $application=array(
                   'status'=>'onprogress',
                   'facility'=>$this->session->userdata('institution'),
               );
        
        $sve=$this->User_model->save_appearanceMeasurement($array,$application,$id[0]->id);
        if($sve){
            
            echo "Saved";exit;
        }
    }
    
    function save_physicalDetailed(){
        
        
         $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'headNeck'=>$this->input->post('headNeck'),
            'headNeckRemark'=>$this->input->post('headNeckRemark') <> NULL?$this->input->post('headNeckRemark'):NULL,
            'mtt'=>$this->input->post('mtt'),
            'mttRemark'=>$this->input->post('mttRemark') <> NULL?$this->input->post('mttRemark'):NULL,
            'sinuses'=>$this->input->post('sinuses'),
            'sinusesRemark'=>$this->input->post('sinusesRemark') <> NULL?$this->input->post('sinusesRemark'):NULL,
            'edv'=>$this->input->post('edv'),
            'edvRemark'=>$this->input->post('edvRemark') <> NULL?$this->input->post('edvRemark'):NULL,
            'lungsChest'=>$this->input->post('lungsChest'),
            'lungsChestRemark'=>$this->input->post('lungsChestRemark') <> NULL?$this->input->post('lungsChestRemark'):NULL,
            'pheart'=>$this->input->post('pheart'),
            'pheartRemark'=>$this->input->post('pheartRemark') <> NULL?$this->input->post('pheartRemark'):NULL,
            'vSystem'=>$this->input->post('vSystem'),
            'vSystemRemark'=>$this->input->post('vSystemRemark') <> NULL?$this->input->post('vSystemRemark'):NULL,
            'pRate'=>$this->input->post('pRate'),
            'pRateRemark'=>$this->input->post('pRateRemark') <> NULL?$this->input->post('pRateRemark'):NULL,
            'bp'=>$this->input->post('bp'),
            'bpRemark'=>$this->input->post('bpRemark') <> NULL?$this->input->post('bpRemark'):NULL,
            'abdomenHernia'=>$this->input->post('abdomenHernia'),
            'abdomenHerniaRemark'=>$this->input->post('abdomenHerniaRemark') <> NULL?$this->input->post('abdomenHerniaRemark'):NULL,
            'livSplGla'=>$this->input->post('livSplGla'),
            'livSplGlaRemark'=>$this->input->post('livSplGlaRemark') <> NULL?$this->input->post('livSplGlaRemark'):NULL,
            'anusRectum'=>$this->input->post('anusRectum'),
            'anusRectumRemark'=>$this->input->post('anusRectumRemark') <> NULL?$this->input->post('anusRectumRemark'):NULL,
            'genito'=>$this->input->post('genito'),
            'genitoRemark'=>$this->input->post('genitoRemark') <> NULL?$this->input->post('genitoRemark'):NULL,
            'endocrine'=>$this->input->post('endocrine'),
            'endocrineRemark'=>$this->input->post('endocrineRemark') <> NULL?$this->input->post('endocrineRemark'):NULL,
            'upperLowerJoints'=>$this->input->post('upperLowerJoints'),
            'upperLowerJointsRemark'=>$this->input->post('upperLowerJointsRemark') <> NULL?$this->input->post('upperLowerJointsRemark'):NULL,
            'spine'=>$this->input->post('spine'),
            'spineRemark'=>$this->input->post('spineRemark') <> NULL?$this->input->post('spineRemark'):NULL,
            'neurology'=>$this->input->post('neurology'),
            'neurologyRemark'=>$this->input->post('neurologyRemark') <> NULL?$this->input->post('neurologyRemark'):NULL,
            'skin'=>$this->input->post('skin'),
            'skinRemark'=>$this->input->post('skinRemark') <> NULL?$this->input->post('skinRemark'):NULL,
            'psychiatric'=>$this->input->post('psychiatric'),
            'psychiatricRemark'=>$this->input->post('psychiatricRemark') <> NULL?$this->input->post('psychiatricRemark'):NULL,
            'attendedBy'=>$this->session->userdata('username'),
            'applicant'=>$this->input->post('applicant')
        );
         
        $id=$this->User_model->physical_details($this->input->post('applicationRef'));
        $sve=$this->User_model->save_detailedPhysical($array,$id[0]->id);
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function save_visualDetails(){
        
        $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'LPLMF'=>$this->input->post('LPLMF'),
            'LPLMFRemark'=>$this->input->post('LPLMFRemark') <> NULL?$this->input->post('LPLMFRemark'):NULL,
            'dVisionWithGRight'=>$this->input->post('dVisionWithGRight'),
            'dVisionWithGLeft'=>$this->input->post('dVisionWithGLeft'),
            'dVisionWithoutGRight'=>$this->input->post('dVisionWithoutGRight'),
            'dVisionWithoutGLeft'=>$this->input->post('dVisionWithoutGLeft'),
            'nVisionWithGRight'=>$this->input->post('nVisionWithGRight'),
            'nVisionWithGLeft'=>$this->input->post('nVisionWithGLeft'),
            'nVisionWithoutGRight'=>$this->input->post('nVisionWithoutGRight'),
            'nVisionWithoutGLeft'=>$this->input->post('nVisionWithoutGLeft'),
            'accomodationWithGRight'=>$this->input->post('accomodationWithGRight'),
            'accomodationWithGLeft'=>$this->input->post('accomodationWithGLeft'),
            'accomodationWithoutGRight'=>$this->input->post('accomodationWithoutGRight'),
            'accomodationWithoutGLeft'=>$this->input->post('accomodationWithoutGLeft'),
            'glassesP'=>$this->input->post('glassesP'),
            'distantSright'=>$this->input->post('distantSright') <> NULL?$this->input->post('distantSright'):NULL,
            'distantCright'=>$this->input->post('distantCright') <> NULL?$this->input->post('distantCright'):NULL,
            'distantAright'=>$this->input->post('distantAright') <> NULL?$this->input->post('distantAright'):NULL,
            'distantSleft'=>$this->input->post('distantSleft') <> NULL?$this->input->post('distantSleft'):NULL,
            'distantCleft'=>$this->input->post('distantCleft') <> NULL?$this->input->post('distantCleft'):NULL,
            'distantAleft'=>$this->input->post('distantAleft') <> NULL?$this->input->post('distantAleft'):NULL,
            'nearSright'=>$this->input->post('nearSright') <> NULL?$this->input->post('nearSright'):NULL,
            'nearCright'=>$this->input->post('nearCright') <> NULL?$this->input->post('nearCright'):NULL,
            'nearAright'=>$this->input->post('nearAright') <> NULL?$this->input->post('nearAright'):NULL,
            'nearSleft'=>$this->input->post('nearSleft') <> NULL?$this->input->post('nearSleft'):NULL,
            'nearCleft'=>$this->input->post('nearCleft') <> NULL?$this->input->post('nearCleft'):NULL,
            'nearAleft'=>$this->input->post('nearAleft') <> NULL?$this->input->post('nearAleft'):NULL,
            'fieldVision'=>$this->input->post('fieldVision'),
            'fieldVisionRemark'=>$this->input->post('fieldVisionRemark') <> NULL?$this->input->post('fieldVisionRemark'):NULL,
            'powerConvergence'=>$this->input->post('powerConvergence') <> NULL?$this->input->post('powerConvergence'):NULL,
            'attendedBy'=>$this->session->userdata('username'),
            'applicant'=>$this->input->post('applicant')
        );
         
        $id=$this->User_model->visual_details($this->input->post('applicationRef'));
        $sve=$this->User_model->save_visual($array,$id[0]->id);
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function save_eyeDetails(){
        
        $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'manifestHyperRight'=>$this->input->post('manifestHyperRight'),
            'manifestHyperLeft'=>$this->input->post('manifestHyperLeft'),
            'cPerceptionIshihara'=>$this->input->post('cPerceptionIshihara'),
            'cPerceptionIshiharaRemark'=>$this->input->post('cPerceptionIshiharaRemark') <> NULL?$this->input->post('cPerceptionIshiharaRemark'):NULL,
            'cPerceptionLantem'=>$this->input->post('cPerceptionLantem'),
            'cPerceptionLantemRemark'=>$this->input->post('cPerceptionLantemRemark') <> NULL?$this->input->post('cPerceptionLantemRemark'):NULL,
            'maddoxRodExo'=>$this->input->post('maddoxRodExo'),
            'maddoxRodEso'=>$this->input->post('maddoxRodEso'),
            'maddoxRodHyper'=>$this->input->post('maddoxRodHyper'),
            'maddoxWingExo'=>$this->input->post('maddoxWingExo'),
            'maddoxWingEso'=>$this->input->post('maddoxWingEso'),
            'maddoxWingHyper'=>$this->input->post('maddoxWingHyper'),
            'attendedBy'=>$this->session->userdata('username'),
            'applicant'=>$this->input->post('applicant')
        );
         
        $id=$this->User_model->eye_details($this->input->post('applicationRef'));
        $sve=$this->User_model->save_eye($array,$id[0]->id);
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function save_audioDetails(){
        
        $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'hearingDifficulty'=>$this->input->post('hearingDifficulty'),
            'hearingDifficultyRemark'=>$this->input->post('hearingDifficultyRemark') <> NULL?$this->input->post('hearingDifficultyRemark'):NULL,
            'forcedWhisperRight'=>$this->input->post('forcedWhisperRight'),
            'forcedWhisperLeft'=>$this->input->post('forcedWhisperLeft'),
            'audiometry3000Right'=>$this->input->post('audiometry3000Right'),
            'audiometry3000Left'=>$this->input->post('audiometry3000Left'),
            'audiometry3000Remark'=>$this->input->post('audiometry3000Remark') <> NULL?$this->input->post('audiometry3000Remark'):NULL,
            'audiometry2000Right'=>$this->input->post('audiometry2000Right'),
            'audiometry2000Left'=>$this->input->post('audiometry2000Left'),
            'audiometry2000Remark'=>$this->input->post('audiometry2000Remark') <> NULL?$this->input->post('audiometry2000Remark'):NULL,
            'audiometry1000Right'=>$this->input->post('audiometry1000Right'),
            'audiometry1000Left'=>$this->input->post('audiometry1000Left'),
            'audiometry1000Remark'=>$this->input->post('audiometry1000Remark') <> NULL?$this->input->post('audiometry1000Remark'):NULL,
            'audiometry500Right'=>$this->input->post('audiometry500Right'),
            'audiometry500Left'=>$this->input->post('audiometry500Left'),
            'audiometry500Remark'=>$this->input->post('audiometry500Remark') <> NULL?$this->input->post('audiometry500Remark'):NULL,
            'attendedBy'=>$this->session->userdata('username'),
            'applicant'=>$this->input->post('applicant')
        );
         
        $id=$this->User_model->audio_details($this->input->post('applicationRef'));
        $sve=$this->User_model->save_audio($array,$id[0]->id);
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function save_otherDetails(){
        
        $error=FALSE;
        if($_FILES['ecgReportFile']['name'] <> NULL){
            
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'pdf';
            $filename='ECG'.time().'.pdf';
            $config['file_name']=$filename;
            $config['overwrite']=true;
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('ecgReportFile')){
                $error=TRUE;
            }
        }
        
        if(!$error){
            
            $array=array(
                'applicationRef'=>$this->input->post('applicationRef'),
                'menstruationDate'=>$this->input->post('menstruationDate') <> NULL?$this->input->post('menstruationDate'):NULL,
                'menstruationDateRemark'=>$this->input->post('menstruationDateRemark') <> NULL?$this->input->post('menstruationDateRemark'):NULL,
                'ecgReport'=>$this->input->post('ecgReport'),
                'ecgNextDate'=>$this->input->post('ecgNextDate'),
                'cxrReport'=>$this->input->post('cxrReport'),
                'audioReport'=>$this->input->post('audioReport'),
                'audioNextDate'=>$this->input->post('audioNextDate'),
                'urinalysisProtein'=>$this->input->post('urinalysisProtein'),
                'urinalysisGlucose'=>$this->input->post('urinalysisGlucose'),
                'urinalysisOther'=>$this->input->post('urinalysisOther'),
                'urinalysisRemarks'=>$this->input->post('urinalysisRemarks'),
                'mhRemarks'=>$this->input->post('mhRemarks'),
                'attendedBy'=>$this->session->userdata('username'),
                'applicant'=>$this->input->post('applicant')
            );
            
            if($filename <> NULL){
                
                $array['ecgReportFile']=$filename;
            }
            
            $id=$this->User_model->other_details($this->input->post('applicationRef'));
            $sve=$this->User_model->save_other($array,$id[0]->id);
            if($sve){

                echo "Saved";exit;
            }
        }
    }
    
    function sendEmail($message,$address){
                
        $this->email->from($this->config->item('from_email_address'),$this->config->item('from_name'),$this->config->item('failure_reply_email_address'));
        $this->email->reply_to($this->config->item('reply_to_email_address'),$this->config->item('reply_to_name'));
        $this->email->to($address);
        $this->email->subject("AVIATION MEDICAL EXAMINATION ACCOUNT ACTIVATION");
        $this->email->message($message);
        return $this->email->send(TRUE);
    }
    
    function applications(){
        
        $referenceNo=$this->input->post('applicationRef');
        $applicant=$this->input->post('applicant');
        $facility=$this->input->post('facility');
       
        $applications=$this->User_model->medical_apps($referenceNo,$applicant,$facility);
        
        $data="";
       
        foreach($applications as $key=>$value){
            
            $data .=$value->applicationRef."=_";
        }
        
        $data=rtrim($data,"=_");
        echo $data;
        exit;
    }
    
    function check_session_account_validity(){
            
        if (!$this->ion_auth->logged_in())
           {
                   return FALSE;
           }

           if(!$this->ion_auth->in_group('Doctor')){

               return FALSE;
           }

           return TRUE;
   }
}
