<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Engine extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Engine_model');
    }
    
    public function userRegistrationLoadBalancer(){
        $lock =file_exists('./locks/userRegistrationLoadBalancer.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/userRegistrationLoadBalancer.lock','w'); 
        }
        
        
       
        $loopcount=0;
        while(++$loopcount <= $this->config->item('script_loopcount')){
            
            $ureg=$this->Engine_model->user_registrations(NULL,NULL,TRUE);
       
            if($ureg == NULL){
                continue;
            }
            
            $i=1;
            $array=array();
            foreach ($ureg as $key=>$value){

                $index=$i%$this->config->item('prEmailInstances');

                if($index == 0){
                    $index=$this->config->item('prEmailInstances');
                     $i=0;
                }

                $array[]=array(
                                'prEmailInstance'=>$index,
                                'id'=>$value->id,
                            );
                $i++;
            }

            $this->Engine_model->save_balanced_registrations($array);
        }
        
        fclose($lk);
        unlink('./locks/userRegistrationLoadBalancer.lock');
    }
    
    public function userRegistrations($prid){
        $lock =file_exists('./locks/userRegistrations_'.$prid.'.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/userRegistrations_'.$prid.'.lock','w'); 
        }
        $this->load->library('email');
        $lgFile='./logs/userRegistrations_'.date('Y-m-d').'.log';
        
        $logfile=file_exists($lgFile);
               
        if($logfile){

            $filesize=filesize($lgFile);

            if($filesize > 10485760){
                rename($lgFile,"./logs/userRegistrations_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        if($this->config->item('app_debug')){
            
            error_log("Instance $prid, ".date('Y-m-d H:i:s')."\n",3, $lgFile);
        }
        
        echo "Instance $prid, ".date('Y-m-d H:i:s')."\n";
        $loopcount=0;
        while(++$loopcount <= $this->config->item('script_loopcount')){
            $array=array();
            $ureg=$this->Engine_model->user_registrations(3,$prid);
            
            if($ureg == NULL){
                if($this->config->item('app_debug')){
            
                    error_log("no pendings available!|$prid\n",3, $lgFile);
                }
                echo "no pendings available!|$prid\n";
                continue;
            }
            
            foreach($ureg as $key=>$value){
                
                if($this->config->item('app_debug')){
            
                    error_log("$value->fname:$value->surname:$value->email!|$prid\n",3, $lgFile);
                }
                
                echo "$value->fname:$value->surname:$value->email!|$prid\n";
                
                $mailMessage=sprintf($this->config->item('mailMessage'),$value->fname.' '.$value->surname,$value->activationLink);
                
                $this->email->from($this->config->item('from_email_address'),$this->config->item('from_name'),$this->config->item('failure_reply_email_address'));
                $this->email->reply_to($this->config->item('reply_to_email_address'),$this->config->item('reply_to_name'));
                $this->email->to($value->email);
                $this->email->subject("AVIATION MEDICAL EXAMINATION ACCOUNT ACTIVATION");
                $this->email->message($mailMessage);
                $send=$this->email->send(TRUE);
                
                if($send){
                    
                    $array[]=array(
                        'status'=>2,
                        'lastupdate'=>date('Y-m-d H:i:s'),
                        'id'=>$value->id
                    );
                    
                    
                    if($this->config->item('app_debug')){
            
                        error_log("Email Sent ($value->email)!|$prid\n",3, $lgFile);
                    }

                    echo "Email Sent ($value->email)!|$prid\n";
                    
                }else{
                    
                    if($this->config->item('app_debug')){
            
                        error_log("Email Not Sent ($value->email)!|$prid\n",3, $lgFile);
                    }

                    echo "Email Not Sent ($value->email)!|$prid\n";
                    
                }
            }
            
            if($array == NULL){
                
                continue;
            }
            
            $sve=$this->Engine_model->save_sent_email_registrations($array);
            
            if($sve){
                
                if($this->config->item('app_debug')){
            
                    error_log("Data saved!|$prid\n",3, $lgFile);
                }

                echo "Data saved!|$prid\n";
            }else{
                
                if($this->config->item('app_debug')){
            
                    error_log("Data not saved!|$prid\n",3, $lgFile);
                }

                echo "Data not saved!|$prid\n";
            }
            
            sleep(3);
        }
        
        fclose($lk);
        unlink('./locks/userRegistrations_'.$prid.'.lock');
    }
    
    function removeExpiredRegistrations(){
        
        $i=1;
        while($i++ <= 5){
            
            $ureg=$this->Engine_model->user_registrations(3);
            
            if($ureg <> NULL){
                
                foreach($ureg as $key=>$value){
                    
                    if($value->expiryTimestamp <= date('Y-m-d H:i:s')){
                        
                        $this->User_model->delete_registration($value->sessionID);
                    }
                }
            }
        }
    }
}