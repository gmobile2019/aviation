<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $this->data['title']="Home";
        $this->data['content']='user/dashboard';
        $this->load->view('user/templateNew',$this->data);
    }
    
    function medical_applications(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        if ($this->input->post('applicationRef')) {
               $key['applicationRef'] = $this->input->post('applicationRef');
               $this->data['applicationRef']=$this->input->post('applicationRef');
        }

        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }

        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['applicationRef'] = $exp[1];
            $this->data['applicationRef']=$key['applicationRef'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['status'] = $exp[7];
            $this->data['status']=$key['status'];

            $docType = $exp[9];
            $this->data['docType']=$docType;
        }

        
        $applicationRef =$key['applicationRef'];
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];
        
        if($docType == 1){
            
            $data=$this->User_model->medical_apps($applicationRef,$this->session->userdata('username'),$start,$end,$status,NULL);
            require_once 'reports/medical_applications_pdf.php';
        }
        $config["base_url"] = base_url() . "index.php/User/medical_applications/ref_".$key['applicationRef']."_strt_".$key['start']."_end_".$key['end']."_status_".$key['status']."/";
        $config["total_rows"] =$this->User_model->application_info_count($applicationRef,$this->session->userdata('username'),$start,$end,$status,NULL);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->User_model->application_info($applicationRef,$this->session->userdata('username'),$start,$end,$status,NULL,$page,$limit);
        
        $this->data['title']="Applications";
        $this->data['content']='user/applications';
        $this->load->view('user/templateNew',$this->data);
    }
    
    function apply(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        

        $this->data['title']="Medical Examination Application";
        $this->data['licenceTypes']=$this->User_model->getlicenceTypes(NULL,NULL,'active');
        $this->data['initialExamination']=$this->User_model->medical_applications(NULL,$this->session->userdata('username'));
        $this->data['content']='user/apply_now';
        $this->load->view('user/templateNew',$this->data);
    }
    
    function reviewApplication($applicationRef){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $bg_details=$this->User_model->background_information(NULL,$this->session->userdata('username'));
        $mh_details=$this->User_model->medicalHistory_information(NULL,$this->session->userdata('username'));
        $oc_details=$this->User_model->occupation_details($applicationRef,$this->session->userdata('username'));
        $ph_details=$this->User_model->personalhealth_details($applicationRef,$this->session->userdata('username'));
        $ac_details=$this->User_model->aircraft_details($applicationRef,$this->session->userdata('username'));
        
        if($bg_details == NULL){
            
            $this->data['reviewContinue']='background';
        }else if($mh_details == NULL){
            
            $this->data['reviewContinue']='medicalHistory';
        }else if($oc_details == NULL){
            
            $this->data['reviewContinue']='employment';
        }else if( $ph_details == NULL){
            
            $this->data['reviewContinue']='personalHealth';
        }else if($ac_details == NULL){
          
            $this->data['reviewContinue']='airCraft';
        }else{
            
            $this->data['reviewContinue']='declaration';
        }
        
        $this->data['title']="Review Application";
        $this->data['referenceNo']=$applicationRef;
        $this->data['licenceTypes']=$this->User_model->getlicenceTypes(NULL,NULL,'active');
        $this->data['initialExamination']=$this->User_model->medical_applications(NULL,$this->session->userdata('username'));
        $this->data['content']='user/review_application';
        $this->load->view('user/templateNew',$this->data);
    }
    
    function applicationDetails($applicationRef,$export){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        $bs_details=$this->User_model->registrationInfo(NULL,$this->session->userdata('username'));
        $bg_details=$this->User_model->background_details($applicationRef,$this->session->userdata('username'));
        $mh_details=$this->User_model->medicalHistory_details($applicationRef,$this->session->userdata('username'));
        $oc_details=$this->User_model->occupation_details($applicationRef,$this->session->userdata('username'));
        $ph_details=$this->User_model->personalhealth_details($applicationRef,$this->session->userdata('username'));
        $ac_details=$this->User_model->aircraft_details($applicationRef,$this->session->userdata('username'));
        $apm_details=$this->User_model->appearanceMeasurement_details($applicationRef);
        $py_details=$this->User_model->physical_details($applicationRef);
        $vs_details=$this->User_model->visual_details($applicationRef);
        $ey_details=$this->User_model->eye_details($applicationRef);
        $au_details=$this->User_model->audio_details($applicationRef);
        $ot_details=$this->User_model->other_details($applicationRef);
        $examination=$this->User_model->medical_applications($applicationRef,$this->session->userdata('username'));
            
        if($export == 'export'){
            
            require_once 'reports/medical_application_details_pdf.php';
        }
        
        $this->data['title']="Application Details";
        $this->data['bs_details']=$bs_details;
        $this->data['bg_details']=$bg_details;
        $this->data['mh_details']=$mh_details;
        $this->data['oc_details']=$oc_details;
        $this->data['ph_details']=$ph_details;
        $this->data['ac_details']=$ac_details;
        $this->data['apm_details']=$apm_details;
        $this->data['py_details']=$py_details;
        $this->data['vs_details']=$vs_details;
        $this->data['ey_details']=$ey_details;
        $this->data['au_details']=$au_details;
        $this->data['ot_details']=$ot_details;
        $this->data['examination']=$examination;
        $this->data['appRef']=$applicationRef;
        $this->data['content']='user/applicationDetails';
        $this->load->view('user/templateNew',$this->data);
    }
    
    function profile(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $this->data['title']="User Profile";
        $this->data['data']=$this->User_model->registrationInfo(NULL,$this->session->userdata('username'),1);
        $this->data['content']='user/profile';
        $this->load->view('user/templateNew',$this->data);
    }
    
    function save_profile(){
       
        $dbField=$this->input->post('name');
        $dbValue=trim($this->input->post('value'));
        $key=$this->session->userdata('username');
       
        if($dbValue == NULL || $dbValue == ''){
            
            echo 'empty field data';
            exit;
        }
        
        $user=array(
            $dbField=>$dbValue,
            'modifiedby'=>$this->session->userdata('user_id'),
            'modifiedon'=>date('Y-m-d H:i:s')
        );
        
        if($dbField == 'fname'){
            $account=array(
                'first_name'=>$dbValue
            );
        }
        
        if($dbField == 'mname'){
            $account=array(
                'middle_name'=>$dbValue
            );
        }
        
        if($dbField == 'surname'){
            $account=array(
                'last_name'=>$dbValue
            );
        }
        
        if($dbField == 'mobile'){
            $account=array(
                'msisdn'=>$dbValue
            );
        }
        
        $sve=$this->User_model->save_userData($user,$account,$key);
        
        if(!$sve){
           
            echo "Error Saving Data!";
            exit;
        }
        
        echo "Data Successfully Saved!";
        exit;
    }
    
    function save_background(){
        
        $armedForce=$this->input->post('armedForce');
        $pilotExperience=$this->input->post('pilotExperience');
        $referenceNo=$this->input->post('applicationRef') <> NULL?$this->input->post('applicationRef'):$this->getReferenceNo();
        $timestamp=date('Y-m-d H:i:s');
        
        $data=array(
            'pilotExperience'=>$pilotExperience,
            'armedForce'=>$armedForce,
            'applicationRef'=>$referenceNo,
            'createdOn'=>$timestamp,
            'applicant'=>$this->session->userdata('username')
        );
        
        $application=array(
            'applicationRef'=>$referenceNo,
            'applicant'=>$this->session->userdata('username'),
            'appliedOn'=>$timestamp,
            'status'=>'pending',
        );
        
        $id=$this->User_model->background_details($referenceNo,$this->session->userdata('username'));
        $sve=$this->User_model->save_background($data,$application,$id[0]->id);
        
        if($sve){
            
            echo $referenceNo;exit;
        }
        
    }
    
    function save_medicalHistory(){
        
        
         $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'fsHeadaches'=>$this->input->post('fsHeadaches'),
            'fsHeadachesRemark'=>$this->input->post('fsHeadachesRemark') <> NULL?$this->input->post('fsHeadachesRemark'):NULL,
            'dfu'=>$this->input->post('dfu'),
            'dfuRemark'=>$this->input->post('dfuRemark') <> NULL?$this->input->post('dfuRemark'):NULL,
            'eyeTrouble'=>$this->input->post('eyeTrouble'),
            'eyeTroubleRemark'=>$this->input->post('eyeTroubleRemark') <> NULL?$this->input->post('eyeTroubleRemark'):NULL,
            'hayFever'=>$this->input->post('hayFever'),
            'hayFeverRemark'=>$this->input->post('hayFeverRemark') <> NULL?$this->input->post('hayFeverRemark'):NULL,
            'asthma'=>$this->input->post('asthma'),
            'asthmaRemark'=>$this->input->post('asthmaRemark') <> NULL?$this->input->post('asthmaRemark'):NULL,
            'heartTrouble'=>$this->input->post('heartTrouble'),
            'heartTroubleRemark'=>$this->input->post('heartTroubleRemark') <> NULL?$this->input->post('heartTroubleRemark'):NULL,
            'bloodPressure'=>$this->input->post('bloodPressure'),
            'bloodPressureRemark'=>$this->input->post('bloodPressureRemark') <> NULL?$this->input->post('bloodPressureRemark'):NULL,
            'stomachTrouble'=>$this->input->post('stomachTrouble'),
            'stomachTroubleRemark'=>$this->input->post('stomachTroubleRemark') <> NULL?$this->input->post('stomachTroubleRemark'):NULL,
            'ksbu'=>$this->input->post('ksbu'),
            'ksbuRemark'=>$this->input->post('ksbuRemark') <> NULL?$this->input->post('ksbuRemark'):NULL,
            'su'=>$this->input->post('su'),
            'suRemark'=>$this->input->post('suRemark') <> NULL?$this->input->post('suRemark'):NULL,
            'epilepsy'=>$this->input->post('epilepsy'),
            'epilepsyRemark'=>$this->input->post('epilepsyRemark') <> NULL?$this->input->post('epilepsyRemark'):NULL,
            'nervousTrouble'=>$this->input->post('nervousTrouble'),
            'nervousTroubleRemark'=>$this->input->post('nervousTroubleRemark') <> NULL?$this->input->post('nervousTroubleRemark'):NULL,
            'motionSickness'=>$this->input->post('motionSickness'),
            'motionSicknessRemark'=>$this->input->post('motionSicknessRemark') <> NULL?$this->input->post('motionSicknessRemark'):NULL,
            'lifeInsurance'=>$this->input->post('lifeInsurance'),
            'lifeInsuranceRemark'=>$this->input->post('lifeInsuranceRemark') <> NULL?$this->input->post('lifeInsuranceRemark'):NULL,
            'hospitalAdmission'=>$this->input->post('hospitalAdmission'),
            'hospitalAdmissionRemark'=>$this->input->post('hospitalAdmissionRemark') <> NULL?$this->input->post('hospitalAdmissionRemark'):NULL,
            'otherIllness'=>$this->input->post('otherIllness'),
            'otherIllnessRemark'=>$this->input->post('otherIllnessRemark') <> NULL?$this->input->post('otherIllnessRemark'):NULL,
            'headInjury'=>$this->input->post('headInjury'),
            'headInjuryRemark'=>$this->input->post('headInjuryRemark') <> NULL?$this->input->post('headInjuryRemark'):NULL,
            'MTDiseases'=>$this->input->post('MTDiseases'),
            'MTDiseasesRemark'=>$this->input->post('MTDiseasesRemark') <> NULL?$this->input->post('MTDiseasesRemark'):NULL,
            'addiction'=>$this->input->post('addiction'),
            'addictionRemark'=>$this->input->post('addictionRemark') <> NULL?$this->input->post('addictionRemark'):NULL,
            'familyHistory'=>$this->input->post('familyHistory'),
            'familyHistoryRemark'=>$this->input->post('familyHistoryRemark') <> NULL?$this->input->post('familyHistoryRemark'):NULL,
            'flyingLicense'=>$this->input->post('flyingLicense'),
            'flyingLicenseRemark'=>$this->input->post('flyingLicenseRemark') <> NULL?$this->input->post('flyingLicenseRemark'):NULL,
            'discharge'=>$this->input->post('discharge'),
            'dischargeRemark'=>$this->input->post('dischargeRemark') <> NULL?$this->input->post('dischargeRemark'):NULL,
            'applicant'=>$this->session->userdata('username')
        );
         
        $id=$this->User_model->medicalHistory_details($this->input->post('applicationRef'),$this->session->userdata('username'));
        $sve=$this->User_model->save_medicalHistory($array,$id[0]->id);
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function save_occupation(){
        
        $referenceNo=$this->input->post('applicationRef') <> NULL?$this->input->post('applicationRef'):$this->getReferenceNo();
        $appStart=$this->input->post('applicationRef') <> NULL?FALSE:TRUE;
        
        $array=array(
            'applicationRef'=>$referenceNo,
            'employer'=>$this->input->post('employer') <> NULL?$this->input->post('employer'):NULL,
            'occupation'=>$this->input->post('occupation') <> NULL?$this->input->post('occupation'):NULL,
            'applicant'=>$this->session->userdata('username')
        );
        
        $application=array(
            'applicationRef'=>$referenceNo,
            'applicant'=>$this->session->userdata('username'),
            'appliedOn'=>date('Y-m-d H:i:s'),
            'status'=>'pending',
        );
        
        //print_r($application);exit;
        $id=$this->User_model->occupation_details($referenceNo);
        $sve=$this->User_model->save_occupation($array,$application,$id[0]->id,$appStart);
       
        if($sve){
            
            echo $referenceNo;exit;
        }
        
    }
    
    function save_personalHealth(){
        
        $noIllAccDis=$this->input->post('noIllAccDis');
        $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'gp'=>$this->input->post('gp'),
            'gpName'=>$this->input->post('gpName') <> NULL?$this->input->post('gpName'):NULL,
            'gpAddress'=>$this->input->post('gpAddress') <> NULL?$this->input->post('gpAddress'):NULL,
            'gpContact'=>$this->input->post('gpContact') <> NULL?$this->input->post('gpContact'):NULL,
            'medication'=>$this->input->post('medication'),
            'medDescription'=>$this->input->post('medDescription') <> NULL?$this->input->post('medDescription'):NULL,
            'medPurpose'=>$this->input->post('medPurpose') <> NULL?$this->input->post('medPurpose'):NULL,
            'medDoctor'=>$this->input->post('medDoctor') <> NULL?$this->input->post('medDoctor'):NULL,
            'noIllAccDis'=>$this->input->post('noIllAccDis'),
            'applicant'=>$this->session->userdata('username')
        );
        
        $illAccDate=NULL;
        $illAccDetails=NULL;
        $illAccDoctorName=NULL;
        $illAccDisDoctorAddress=NULL;
        
        if($noIllAccDis > 0){
            
            for($i=1;$i<=$noIllAccDis;$i++){
            
                $illAccDate .=$this->input->post('IllAccDisDate'.$i).'&&';
                $illAccDetails .=$this->input->post('IllAccDisDetails'.$i).'&&';
                $illAccDoctorName .=$this->input->post('IllAccDisDoctorName'.$i).'&&';
                $illAccDisDoctorAddress .=$this->input->post('IllAccDisDoctorAddress'.$i).'&&';
            }

            $illAccDate=rtrim($illAccDate,'&&');
            $illAccDetails=rtrim($illAccDetails,'&&');
            $illAccDoctorName=rtrim($illAccDoctorName,'&&');
            $illAccDisDoctorAddress=rtrim($illAccDisDoctorAddress,'&&');
        }
        
        $array['IllAccDisDate']=$illAccDate;
        $array['IllAccDisDetails']=$illAccDetails;
        $array['IllAccDisDoctorName']=$illAccDoctorName;
        $array['IllAccDisDoctorAddress']=$illAccDisDoctorAddress;
        
        if($this->input->post('applicationRef') <> NULL){
            
            $id=$this->User_model->personalhealth_details($this->input->post('applicationRef'));
        }
                
        $sve=$this->User_model->save_personalHealth($array,$id[0]->id);
        
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function save_aircraftExperience(){
        
        $noAirType=$this->input->post('noAirType');
        $noAirRoute=$this->input->post('noAirRoute');
        $noAirAccident=$this->input->post('noAirAccident');
        
        $array=array(
            'applicationRef'=>$this->input->post('applicationRef'),
            'licenceType'=>$this->input->post('licenceType'),
            'licenseNumber'=>$this->input->post('licenseNumber'),
            'expDteLMC'=>$this->input->post('expDteLMC'),
            'expDte5YrL'=>$this->input->post('expDte5YrL'),
            'hrsFlown'=>$this->input->post('hrsFlown'),
            'airAccident'=>$this->input->post('airAccident'),
            'noAircraftAccident'=>$this->input->post('noAirAccident'),
            'noAircraftType'=>$this->input->post('noAirType'),
            'noAircraftRoute'=>$this->input->post('noAirRoute'),
            'applicant'=>$this->session->userdata('username')
        );
        
        $airAccDate=NULL;
        $airAccLocation=NULL;
        
        if($noAirAccident > 0){
                
            for($i=1;$i<=$noAirAccident;$i++){
            
                $airAccDate .=$this->input->post('airAccidentDte'.$i).'&&';
                $airAccLocation .=$this->input->post('airAccidentLocation'.$i).'&&';
            }
            
            $array['aircraftAccidentDate']=rtrim($airAccDate,'&&');
            $array['airAccidentLocation']=rtrim($airAccLocation,'&&');
        }
        
        if($noAirRoute > $noAirType){
            
            $rows=$noAirRoute;
        }else if($noAirType > $noAirRoute){
            
            $rows=$noAirType;
        }else{
            
            $rows=$noAirType;
        }
        
        $route=NULL;
        $aircraftType=NULL;
        
        if($rows > 0){
            
            for($i=1;$i<=$rows;$i++){
            
                $route .=$this->input->post('route'.$i).'&&';
                $aircraftType .=$this->input->post('aircraftType'.$i).'&&';
            }

            $array['aircraftRoute']=rtrim($route,'&&');
            $array['aircraftType']=rtrim($aircraftType,'&&');
            
        }
        
        if($this->input->post('applicationRef') <> NULL){
            
            $id=$this->User_model->aircraft_details($this->input->post('applicationRef'));
        }
        
        $sve=$this->User_model->save_aircraftExperience($array,$id[0]->id);
        
        if($sve){
            
            echo "Saved";exit;
        }
        
    }
    
    function background_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->background_details($this->input->post('ref'),$this->session->userdata('username'),'Array');
            
            if($data == NULL){
                $data=$this->User_model->background_information(NULL,$this->session->userdata('username'),'Array');
            }
            echo json_encode($data);
        }
        
        exit;
    }
    
    function medical_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->medicalHistory_details($this->input->post('ref'),$this->session->userdata('username'),'Array');
            
            if($data == NULL){
                $data=$this->User_model->medicalHistory_information(NULL,$this->session->userdata('username'),'Array');
            }
            echo json_encode($data);
        }
        
        exit;
    }
    
    function occupation_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->occupation_details($this->input->post('ref'),$this->session->userdata('username'),'Array');
          
            echo json_encode($data);
        }
        
        exit;
    }
    
    function personalHealth_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->personalhealth_details($this->input->post('ref'),$this->session->userdata('username'),'Array');
          
            echo json_encode($data);
        }
        
        exit;
    }
    
    function aircraft_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->aircraft_details($this->input->post('ref'),$this->session->userdata('username'),'Array');
          
            echo json_encode($data);
        }
        
        exit;
    }
    
    function appearanceMeasurement_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->appearanceMeasurement_details($this->input->post('ref'),NULL,'Array');
            
            if($data <> NULL){
                
                echo json_encode($data);
            }
            
        }
        
        exit;
    }
    
    function physical_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->physical_details($this->input->post('ref'),NULL,'Array');
            
            if($data <> NULL){
                
                echo json_encode($data);
            }
            
        }
        
        exit;
    }
    
    function visual_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->visual_details($this->input->post('ref'),NULL,'Array');
            
            if($data <> NULL){
                
                echo json_encode($data);
            }
            
        }
        
        exit;
    }
    
    function eye_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->eye_details($this->input->post('ref'),NULL,'Array');
            
            if($data <> NULL){
                
                echo json_encode($data);
            }
            
        }
        
        exit;
    }
    
    function audio_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->audio_details($this->input->post('ref'),NULL,'Array');
            
            if($data <> NULL){
                
                echo json_encode($data);
            }
            
        }
        
        exit;
    }
    
    function other_details(){
        
        if($this->input->post('ref') <> NULL){
            
            $data=$this->User_model->other_details($this->input->post('ref'),NULL,'Array');
            
            if($data <> NULL){
                
                echo json_encode($data);
            }
            
        }
        
        exit;
    }
    
    function complete_application(){
    
        $array=array(
            'completedOn'=>date('Y-m-d H:i:s'),
            'status'=>'complete',
        );
        
        $sve=$this->User_model->save_application($array,$this->input->post('applicationRef'));
        
        if($sve){
            
            echo $this->input->post('applicationRef');exit;
        }
    }
    
    function check_session_account_validity(){
            
        if (!$this->ion_auth->logged_in())
           {
                   return FALSE;
           }

           if(!$this->ion_auth->in_group('User')){

               return FALSE;
           }

           return TRUE;
   }
   
   function getReferenceNo(){
       
       while(true){
          $day=  strtoupper(date("D"));
        
            switch($day){
                case "MON":
                    $str="ADGKNRVW";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
                    break;
                case "TUE":
                    $str="BFIMQTXZ";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
                    break;
                case "WED":
                    $str="CEJLOUYS";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
                    break;
                case "THU":
                    $str="FGMNSTYZ";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
                    break;
                case "FRI":
                    $str="ABDEHIRS";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
                    break;
                case "SAT":
                    $str="XSTUPCFC";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
                    break;
                default :
                    $str="IZKMJNBA";
                    $str=substr(str_shuffle($str),-4);
                    $ref=$str.mt_rand(1000,9000);
            }
            
            $check=$this->User_model->medical_applications($ref);
            if($check == NULL){
                
                break;
            }
        }
       
        return $ref;
   }
}