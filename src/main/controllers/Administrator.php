<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Administrator
 *
 * @author Manuel
 */
class Administrator extends CI_Controller{
    
    function __construct() {
        parent::__construct();
       
        $this->load->library('email');
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $this->data['title']='Home';
        $this->data['content']='administrator/dashboard';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function medical_applications(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        if ($this->input->post('applicationRef')) {
               $key['applicationRef'] = $this->input->post('applicationRef');
               $this->data['applicationRef']=$this->input->post('applicationRef');
        }

        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('facility')) {
            $key['facility'] = $this->input->post('facility');
            $this->data['facility']=$this->input->post('facility');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['applicationRef'] = $exp[1];
            $this->data['applicationRef']=$key['applicationRef'];

            $key['start'] = $exp[3];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[5];
            $this->data['end']=$key['end'];

            $key['status'] = $exp[7];
            $this->data['status']=$key['status'];
            
            $key['facility'] = $exp[9];
            $this->data['facility']=$key['facility'];

            $docType = $exp[11];
            $this->data['docType']=$docType;
        }

        $applicationRef =$key['applicationRef'];
        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];
        $facility =$key['facility'];

        if($docType == 1){
            
            $data=$this->User_model->medical_apps($applicationRef,NULL,$start,$end,$status,$facility);
            require_once 'reports/medical_applications_pdf.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administrator/index/ref_".$key['applicationRef']."_strt_".$key['start']."_end_".$key['end']."_status_".$key['status']."_facility_".$key['facility']."/";
        $config["total_rows"] =$this->User_model->application_info_count($applicationRef,NULL,$start,$end,$status,$facility);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->User_model->application_info($applicationRef,NULL,$start,$end,$status,$facility,$page,$limit);
        
        $this->data['title']="Applications";
        $this->data['institutions']=$this->SuperAdministration_model->institutions();
        $this->data['content']='administrator/applications';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function applicationDetails($applicationRef,$export){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $bg_details=$this->User_model->background_details($applicationRef,NULL);
        $mh_details=$this->User_model->medicalHistory_details($applicationRef,NULL);
        $oc_details=$this->User_model->occupation_details($applicationRef,NULL);
        $ph_details=$this->User_model->personalhealth_details($applicationRef,NULL);
        $ac_details=$this->User_model->aircraft_details($applicationRef,NULL);
        $apm_details=$this->User_model->appearanceMeasurement_details($applicationRef);
        $py_details=$this->User_model->physical_details($applicationRef);
        $vs_details=$this->User_model->visual_details($applicationRef);
        $ey_details=$this->User_model->eye_details($applicationRef);
        $au_details=$this->User_model->audio_details($applicationRef);
        $ot_details=$this->User_model->other_details($applicationRef);
        $examination=$this->User_model->medical_applications($applicationRef,NULL);
        $bs_details=$this->User_model->registrationInfo(NULL,$examination[0]->applicant);
            
        if($export == 'export'){
            
            require_once 'reports/medical_application_details_pdf.php';
        }
        
        $this->data['title']="Application Details";
        $this->data['bs_details']=$bs_details;
        $this->data['bg_details']=$bg_details;
        $this->data['mh_details']=$mh_details;
        $this->data['oc_details']=$oc_details;
        $this->data['ph_details']=$ph_details;
        $this->data['ac_details']=$ac_details;
        $this->data['apm_details']=$apm_details;
        $this->data['py_details']=$py_details;
        $this->data['vs_details']=$vs_details;
        $this->data['ey_details']=$ey_details;
        $this->data['au_details']=$au_details;
        $this->data['ot_details']=$ot_details;
        $this->data['examination']=$examination;
        $this->data['appRef']=$applicationRef;
        $this->data['content']='administrator/applicationDetails';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function user_feedback(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['start'] = $exp[1];
            $this->data['start']=$key['start'];

            $key['end'] = $exp[3];
            $this->data['end']=$key['end'];

            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $docType = $exp[7];
            $this->data['docType']=$docType;
        }

        $start =$key['start'];
        $end =$key['end'];
        $status =$key['status'];

        if($docType == 1){
            
            $data=$this->User_model->medical_apps($applicationRef,NULL,$start,$end,$status,$facility);
            require_once 'reports/medical_applications_pdf.php';
        }
        
        $config["base_url"] = base_url() . "index.php/Administrator/user_feedback/strt_".$key['start']."_end_".$key['end']."_status_".$key['status']."/";
        $config["total_rows"] =$this->User_model->feedback_info_count($start,$end,$status);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->User_model->feedback_info($start,$end,$status,$page,$limit);
        
        $this->data['title']="User Feedback";
        $this->data['content']='administrator/user_feedback';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function openFeedback($id,$export){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $this->User_model->save_feedback(array('readon'=>date('Y-m-d H:i:s'),'status'=>'Read'),$id);
        $data=$this->User_model->user_feedback($id);
        
            
        if($export == 'export'){
            
            require_once 'reports/user_feedback_details_pdf.php';
        }
        
        $this->data['title']="Feedback Details";
        $this->data['data']=$data;
        $this->data['content']='administrator/feedbackDetails';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function users(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('grp')) {
               $key['grp'] = $this->input->post('grp');
               $this->data['grp']=$this->input->post('grp');
        }

        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }

        if ($this->input->post('institution')) {
            $key['institution'] = $this->input->post('institution');
            $this->data['institution']=$this->input->post('institution');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];

            $key['grp'] = $exp[3];
            $this->data['start']=$key['grp'];

            $key['institution'] = $exp[5];
            $this->data['institution']=$key['institution'];

            $key['status'] = $exp[7];
            $this->data['status']=$key['status'];

            $docType = $exp[9];
            $this->data['docType']=$docType;
        }

        $name =$key['name'];
        $group =$key['grp'];
        $institution =$key['institution'];
        $status =$key['status'];

        $config["base_url"] = base_url() . "index.php/Administrator/users/name_".$key['name']."_grp_".$key['grp']."_inst_".$key['institution']."_status_".$key['status']."/";
        $config["total_rows"] =$this->SuperAdministration_model->users_info_count($name,$group,$institution,$status);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->SuperAdministration_model->users_info($name,$group,$institution,$status,$page,$limit);
        
        $this->data['title']="System Users";
        $this->data['groups']=$this->SuperAdministration_model->groups();
        $this->data['institutions']=$this->SuperAdministration_model->institutions();
        $this->data['content']='administrator/users';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function add_user($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
          
           $error=FALSE;
           $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
           $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
           $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
           $this->form_validation->set_rules('institution','Institution','required|xss_clean');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
           $this->form_validation->set_rules('group','User Group','required|xss_clean');
           
            if($id <> null){

               if($this->input->post('password') <> null){

                 $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                 $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');   
               }
           }else{

             $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
             $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');  
           }

           
           
           
            if ($this->form_validation->run() == TRUE)
           {

                $firstname=$this->input->post('first_name');
                $middlename=$this->input->post('middle_name');
                $lastname=$this->input->post('last_name');
                $username=$this->input->post('username');
                $institution=$this->input->post('institution');
                $email=$this->input->post('email');
                $mobile=$this->input->post('mobile');
                $group=$this->input->post('group');
                
                $password=$this->input->post('password');

                       $username=$id <> NULL?$this->input->post('username'):$this->getUsername($institution);
                       $user=array(
                           'first_name'=>$firstname,
                           'middle_name'=>$middlename<>null?$middlename:null,
                           'ip_address'=>$this->input->ip_address(),
                           'last_name'=>$lastname,
                           'institution'=>$institution,
                           'username'=>$username,
                           'email'=>$email,
                           'msisdn'=>$mobile,
                       );

                       $user_group=array(
                           'group_id'=>$group
                       );
                       
                     
                        if($this->SuperAdministration_model->system_user_registration($user,$user_group,$id,$password)){
                            
                            if($id == NULL){
                                $mailMessage=sprintf($this->config->item('mailMessage2'),$firstname.' '.$lastname,$this->config->item('loginLink'),$username,$password);
                            
                                if($this->sendEmail($mailMessage,$email)){

                                    $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">registration completed</div>';
                                }else{

                                    $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">registration incomplete(mail not sent)</div>';
                                }
                            }else{
                                
                                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved</div>';
                            }
                            
                        }else{
                            $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                        }
           }
           
           
           $this->data['title']="Add/Edit User";
           $this->data['id']="$id";
           $this->data['member']=$this->SuperAdministration_model->get_member_info($id);
           $this->data['groups']=$this->SuperAdministration_model->get_registration_groups($this->session->userdata('group'));
           $this->data['userInfo']=$this->SuperAdministration_model->current_user_info();
           $this->data['institutions']=$this->SuperAdministration_model->institutions(NULL,NULL,1);
           $this->data['user_menu']='active';
           $this->data['content']='administrator/add_user';
           $this->load->view('administrator/templateNew',$this->data);
    }
    
    function activate_deactivate_user($id,$status){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
                
        $this->SuperAdministration_model->activate_deactivate_users($id,$status);
        redirect('Administrator/users','refresh');
    }
    
    function institutions(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];

            $key['status'] = $exp[3];
            $this->data['status']=$key['status'];

            $docType = $exp[5];
            $this->data['docType']=$docType;
        }

        $name =$key['name'];
        $status =$key['status'];

        $config["base_url"] = base_url() . "index.php/Administrator/institutions/name_".$key['name']."_status_".$key['status']."/";
        $config["total_rows"] =$this->SuperAdministration_model->institutions_info_count($name,$status);
        $config["per_page"] =30;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->SuperAdministration_model->institutions_info($name,$status,$page,$limit);
        
        $this->data['title']="Institutions";
        $this->data['content']='administrator/institutions';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function add_institution($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->form_validation->set_rules('institutionname','Institution Name','required|xss_clean');
           $this->form_validation->set_rules('institutionaddress','Institution Address','xss_clean|required');
           $this->form_validation->set_rules('institutionlocation','Institution Location','required|xss_clean');
           $this->form_validation->set_rules('institutioncontact','Institution Contact','required|xss_clean');
           
            if ($this->form_validation->run() == TRUE){

                $institutionname=$this->input->post('institutionname');
                $institutionaddress=$this->input->post('institutionaddress');
                $institutionlocation=$this->input->post('institutionlocation');
                $institutioncontact=$this->input->post('institutioncontact');
                

                $data=array(
                    'institutionname'=>$institutionname,
                    'institutionaddress'=>$institutionaddress,
                    'institutionlocation'=>$institutionlocation,
                    'institutioncontact'=>$institutioncontact,
                );

                 if($this->SuperAdministration_model->add_institution($data,$id)){

                     $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
                     
                 }else{
                     
                     $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                 }
           }
           
           
        $this->data['title']="Add/Edit Institution Information";
        $this->data['id']="$id";
        $this->data['data']=$this->SuperAdministration_model->institutions($id);
        $this->data['content']='administrator/add_institution';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function activate_deactivate_institution($id,$status){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
                
        $this->SuperAdministration_model->activate_deactivate_institution($id,$status);
        redirect('Administrator/institutions','refresh');
    }
    
    function profile(){
        if (!$this->check_session_account_validity()){
                //redirect them to the login page
                redirect('Home/logout', 'refresh');
        }
        
        
        $this->data['title']="User Profile";
        $this->data['data']=$this->SuperAdministration_model->current_user_info();
        $this->data['content']='administrator/profile';
        $this->load->view('administrator/templateNew',$this->data);
    }
    
    function save_profile(){
       
        $dbField=$this->input->post('name');
        $dbValue=trim($this->input->post('value'));
       
        if($dbValue == NULL || $dbValue == ''){
            
            echo 'empty field data';
            exit;
        }
        
        if($dbField == 'fname'){
            
            $dbField='first_name';
        }
        
        if($dbField == 'mname'){
            
            $dbField='middle_name';
        }
        
        if($dbField == 'surname'){
            
            $dbField='last_name';
        }
        
        if($dbField == 'mobile'){
            
            $dbField='msisdn';
        }
        
        $account=array(
            $dbField=>$dbValue,
            'modifiedby'=>$this->session->userdata('user_id'),
            'modifiedon'=>date('Y-m-d H:i:s')
        );
        
        
        $sve=$this->SuperAdministration_model->save_accountDetails($account,$this->session->userdata('user_id'));
        
        if(!$sve){
           
            echo "Error Saving Data!";
            exit;
        }
        
        echo "Data Successfully Saved!";
        exit;
    }
    
    function sendEmail($message,$address){
                
        $this->email->from($this->config->item('from_email_address'),$this->config->item('from_name'),$this->config->item('failure_reply_email_address'));
        $this->email->reply_to($this->config->item('reply_to_email_address'),$this->config->item('reply_to_name'));
        $this->email->to($address);
        $this->email->subject("AVIATION MEDICAL EXAMINATION ACCOUNT ACTIVATION");
        $this->email->message($message);
        return $this->email->send(TRUE);
    }
    
    function applications(){
        
        $referenceNo=$this->input->post('applicationRef');
        $applicant=$this->input->post('applicant');
        $facility=$this->input->post('facility');
       
        $applications=$this->User_model->medical_apps($referenceNo,$applicant,$facility);
        
        $data="";
       
        foreach($applications as $key=>$value){
            
            $data .=$value->applicationRef."=_";
        }
        
        $data=rtrim($data,"=_");
        echo $data;
        exit;
    }
    
    function check_session_account_validity(){
            
        if (!$this->ion_auth->logged_in())
           {
                   return FALSE;
           }

           if(!$this->ion_auth->in_group('Administrator')){

               return FALSE;
           }

           return TRUE;
   }
   
   function getUsername($institution){
       
       return 'D'.$institution.substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),-2);
   }
}
