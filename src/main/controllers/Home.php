<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        
        $this->load->view('public/publicNew');
    }
    
    public function user_signup(){
        
        $this->form_validation->set_rules('title','Title','required|xss_clean');
        $this->form_validation->set_rules('fname','First Name','required|xss_clean');
        $this->form_validation->set_rules('mname','Middle Name','xss_clean');
        $this->form_validation->set_rules('surname','Surname','required|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
        $this->form_validation->set_rules('gender', 'Gender', 'required|xss_clean');
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'required|xss_clean|callback_dob');
        $this->form_validation->set_rules('pob', 'Place Of Birth', 'required|xss_clean');
        $this->form_validation->set_rules('nationality', 'Nationality', 'required|xss_clean');
        $this->form_validation->set_rules('paddress', 'Permanent Address', 'required|xss_clean');
        $this->form_validation->set_rules('postal', 'Postal Address', 'required|xss_clean');
           
        if ($this->form_validation->run() == TRUE){
           
            $fname=ucwords(strtolower(trim($this->input->post('fname'))));
            $title=$this->input->post('title');
            $mname=ucwords(strtolower(trim($this->input->post('mname'))));
            $surname=ucwords(strtolower(trim($this->input->post('surname'))));
            $email=trim($this->input->post('email'));
            $mobile=$this->input->post('mobile');
            $gender=$this->input->post('gender');
            $dob=$this->input->post('dob');
            $pob=trim($this->input->post('pob'));
            $nationality=ucwords(strtolower(trim($this->input->post('nationality'))));
            $paddress=trim($this->input->post('paddress'));
            $postal=trim($this->input->post('postal'));
            $sessionId=time();
            $activationLink=$this->config->item('activationLinkBackBone').'?session='.$sessionId;
            
            
            $timestamp=date('Y-m-d H:i:s');
            $expirytimesatmp = new DateTime($timestamp);
                            $interval=new DateInterval('PT'.$this->config->item('expiring_period').'M');
                            $expirytimesatmp->add($interval);
                            $expirytimesatmp=$expirytimesatmp->format('Y-m-d H:i:s');
                            
            $user=array(
                       'title'=>$title,
                       'fname'=>$fname,
                       'mname'=>$mname<>null?$mname:null,
                       'ipaddress'=>$this->input->ip_address(),
                       'surname'=>$surname,
                       'email'=>$email,
                       'mobile'=>$mobile,
                       'gender'=>$gender,
                       'dob'=>$dob,
                       'pob'=>$pob,
                       'nationality'=>$nationality,
                       'postal'=>$postal,
                       'paddress'=>$paddress,
                       'createdon'=>$timestamp,
                       'activationLink'=>$activationLink,
                       'sessionID'=>$sessionId,
                       'expiryTimestamp'=>$expirytimesatmp,
                       'status'=>3,
                   );
            
            $sve=$this->User_model->save_signUp($user);
        
            if($sve){
                
                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">account creation successfull, please check your email for account activation link!</div>';
            }else{
                
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">sorry account creation failure,please try again later!</div>';
            }
        }
        
        $this->data['flag']='signup';
        $this->load->view('public/publicNew',$this->data);
    }
    
    public function activateAccount(){
        
        if($flag <> 'register'){
            
            $sessionID=$this->input->get('session');
        
            $info=$this->User_model->registrationInfo($sessionID);

            if($info == null){
                
                redirect('Home/errorPage/101','refresh');
            }
            
            if($info[0]->status == 1){
                
                redirect('Home/errorPage/104','refresh');
            }
            
            if($info[0]->expiryTimestamp <= date('Y-m-d H:i:s') && $info[0]->status == 3){
                
                $this->User_model->delete_registration($sessionID);
                redirect('Home/errorPage/103','refresh');
            }
        }
        
        if($flag == 'register'){
            
            $sessionID=$this->input->post('sessionID');
        }
        
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
        $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('sessionID', '', 'required');
        
        if($this->form_validation->run() == true){

                $password=$this->input->post('password');

                $info=$this->User_model->registrationInfo($this->input->post('sessionID'));

                       $user=array(
                           'first_name'=>$info[0]->fname,
                           'middle_name'=>$info[0]->mname<>null?$info[0]->mname:null,
                           'ip_address'=>$this->input->ip_address(),
                           'last_name'=>$info[0]->surname,
                           'username'=>$info[0]->email,
                           'email'=>$info[0]->email,
                           'msisdn'=>$info[0]->	mobile,
                       );

                       $user_group=array(
                           'group_id'=>$this->config->item('user_group_id')
                       );
                       
                       $resp=$this->SuperAdministration_model->system_user_registration($user,$user_group,$id,$password);

                        if($resp){
                            $data=array(
                                'sessionID'=>$this->input->post('sessionID'),
                                'lastupdate'=>date('Y-m-d H:i:s'),
                                'status'=>1
                            );

                            $this->User_model->close_registration($data);
                            $this->data['message']='<span class="alert alert-success " role="alert" style="text-align:center;">account activation successfull, please '.  anchor('Home/user_signIn', '<span style="font-style:italic;font-weight:bold">click here<span>').' to Sign In!</span>';
                        }else{
                            $this->data['message']='<span class="alert alert-danger " role="alert" style="text-align:center;">sorry account activation failed!</span>';
                        }
        }
        
        $this->data['title']='User Account Activation';
        $this->data['sessionID']=$sessionID;
        $this->load->view('public/activateAccountNew',$this->data);
    }
    
    public function errorPage($code){
        
        
        $this->data['title']='Error';
        $this->data['message']='<span class="alert alert-danger " role="alert">'.$this->getErrMessage($code).'</span>';;
        $this->load->view('public/errorPageNew',$this->data);
    }
    
    public function user_signin(){
        
        $this->form_validation->set_rules('usrnme', 'Username/Email', 'required|xss_clean');
        $this->form_validation->set_rules('pswd', 'Password', 'required|xss_clean');
        $this->form_validation->set_rules('remember', '', 'xss_clean');
           
        if ($this->form_validation->run() == true)
		{ //check to see if the user is logging in
			//check for "remember me"
            
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('usrnme'),$this->input->post('pswd'), $remember))
			{ //if the login is successful
                                
                            
				//redirect them to a specific  module
				if($this->ion_auth->in_group('Administrator')){
                                   redirect('Administrator/','refresh');
                                }
                                
                                if($this->ion_auth->in_group('User')){
                                   redirect('User/','refresh');
                                }
                                
                                if($this->ion_auth->in_group('Doctor')){
                                   redirect('Doctor/','refresh');
                                }
                                
			}else{ //if the login was un-successful
				
                            $this->data['msg']=$this->ion_auth->errors();
			}
		}
		else
		{  //the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['msg'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                }
        
        $this->data['flag']='signin';
        $this->load->view('public/publicNew',$this->data);
    }
    
    public function login(){
        
        $this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
        $this->form_validation->set_rules('remember', '', 'xss_clean');
           
        if ($this->form_validation->run() == true)
		{ //check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('username'),$this->input->post('password'), $remember))
			{ //if the login is successful
                                
                            
				//redirect them to a specific  module
				if($this->ion_auth->in_group('Administrator')){
                                   redirect('Administrator/','refresh');
                                }
                                
                                if($this->ion_auth->in_group('Doctor')){
                                   redirect('Doctor/','refresh');
                                }
                                
			}else{ //if the login was un-successful
				
                            $this->data['msg']=$this->ion_auth->errors();
			}
		}
		else
		{  //the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['msg'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                }
        
        $this->data['flag']='admin';
        $this->load->view('public/public',$this->data);
    }
    
    public function logout(){
        
            $this->Ion_auth_model->logout_users($this->session->userdata('user_id'));
            $logout = $this->ion_auth->logout();
            redirect('Home/user_signin', 'refresh');
    }
    
    public function about(){
        
        $this->load->view('public/about',$this->data);
    }
    
    public function feedback(){
        $this->form_validation->set_rules('fdbksubject','Subject','required|xss_clean');
        $this->form_validation->set_rules('fdbkemail','Email Address','required|valid_email|xss_clean');
        $this->form_validation->set_rules('fdbkmobile', 'Mobile Number', 'xss_clean');
        $this->form_validation->set_rules('fdbkcomm', 'Suggestions/Comments', 'required|xss_clean');
           
        if ($this->form_validation->run() == TRUE){
           
            $fdbksubject=ucwords(strtolower(trim($this->input->post('fdbksubject'))));
            $fdbkmobile=$this->input->post('fdbkmobile');
            $fdbkemail=trim($this->input->post('fdbkemail'));
            $fdbkcomm=trim($this->input->post('fdbkcomm'));
            
                            
            $feedback=array(
                       'feedbackSubject'=>$fdbksubject,
                       'mobileNumber'=>$fdbkmobile,
                       'feedbackemail'=>$fdbkemail,
                       'feedbackcomments'=>$fdbkcomm,
                       'createdon'=>date('Y-m-d H:i:s'),
                   );
            //print_r($feedback);
            $sve=$this->User_model->save_feedback($feedback);
        
            if($sve){
                
                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">Thank you for your feedback!</div>';
            }
        }
        
        $this->data['feedback']='active';
        $this->load->view('public/feedback',$this->data);
    }
    public function getErrMessage($code){
        
        switch($code){
            case 101:
                return "sorry the activation link is not invalid, please ".anchor('Home/user_signup', '<span style="font-style:italic;font-weight:bold">click here<span>')." to Sign Up!";
                break;
            case 102:
                return "System Error";
                break;
            case 103:
                return "sorry the activation link has expired, please ".anchor('Home/user_signup', '<span style="font-style:italic;font-weight:bold">click here<span>')." to Sign Up again!";
                break;
            case 104:
                return "account activation has been completed, please ".anchor('Home/user_signIn', '<span style="font-style:italic;font-weight:bold">click here<span>')." to Sign In!";
                break;
        }
    }
    
    function dob($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('start', "The %s format must be YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('start', "The %s format must be YYYY-MM-DD");
                return FALSE;
            }
    }
}