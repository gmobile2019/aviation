<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Medical Applications</h3>';


    $html.='<table border="1">
                <tr>
                    <td style="width:150px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:320px;text-align:center"><b> &nbsp;App Reference</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Applicant</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Examination Facility</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Application Date</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Doctor</b></td>
                    <td style="width:250px;text-align:center"><b> &nbsp;Status</b></td>
                </tr>';
$i = 1;
$total=0;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $applcnt=$this->User_model->registrationInfo(NULL,$value->applicant);
        $fclty=$value->facility <> NULL?$this->SuperAdministration_model->institutions(NULL,$value->facility):"";
        $appDate=explode(' ',$value->appliedOn);
        $applicationDate=explode('-',$appDate[0]);

        $doc=$value->attendedBy <> NULL?$this->SuperAdministration_model->get_member_info(NULL,$value->attendedBy):"";
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->applicationRef . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' .$applcnt[0]->fname.' '.$applcnt[0]->surname . ' &nbsp; </td>
                    <td>&nbsp;&nbsp;'.$fclty[0]->institutionname.'</td>
                    <td>&nbsp;&nbsp;'.$applicationDate[2].'/'.$applicationDate[1].'/'.$applicationDate[0].'</td>
                    <td>&nbsp;&nbsp;'.$doc[0]->first_name.' '.$doc[0]->last_name.'</td>
                    <td>&nbsp;&nbsp;'.$value->status.'</td>
                </tr>';
      }

$html.='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Medical Applications.pdf', 'D');
exit;
?>
