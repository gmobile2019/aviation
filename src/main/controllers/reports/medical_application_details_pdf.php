<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
$img_file ='./img/plane1.jpg';
$this->pdf->Image($img_file, 5, 33, 200, 250, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$this->pdf->setPageMark();
//heading
$html  = '<h2 align="center">Medical Application Details</h2>';
$html  .= '<h4>Application Reference : '.$applicationRef.'</h4>';

    $html  .= '<h4 style="text-align:center;font-style:italic;">A. Basic Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Title</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->title.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;First Name</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->fname.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Middle Name</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->mname.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Surname</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->surname.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Email</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->email.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Mobile</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->mobile.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Gender</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->gender.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Age</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$this->User_model->dateDifference(date('Y-m-d'),$bs_details[0]->dob)->y.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Place of birth</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->pob.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Nationality</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->nationality.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Permanent Address</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->paddress.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Postal Address</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bs_details[0]->postal.'</td>
                </tr>
            </table><p></p>';
    
    $html  .= '<h4 style="text-align:center;font-style:italic">B. Background Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Have you ever served in Armed Forces?</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bg_details[0]->armedForce.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Have you ever had previous Pilot or Aircrew experience?</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$bg_details[0]->pilotExperience.'</td>
                </tr>
            </table><p></p>';
    
    $fsRmrk=$mh_details[0]->fsHeadaches <> 'No'?$mh_details[0]->fsHeadachesRemark:'N/A';
    $dfuRmrk=$mh_details[0]->dfu <> 'No'?$mh_details[0]->dfuRemark:'N/A';
    $ETRmrk=$mh_details[0]->eyeTrouble <> 'No'?$mh_details[0]->eyeTroubleRemark:'N/A';
    $HRmrk=$mh_details[0]->hayFever <> 'No'?$mh_details[0]->hayFeverRemark:'N/A';
    $ASRmrk=$mh_details[0]->asthma <> 'No'?$mh_details[0]->asthmaRemark:'N/A';
    $HTRmrk=$mh_details[0]->heartTrouble <> 'No'?$mh_details[0]->heartTroubleRemark:'N/A';
    $BPRmrk=$mh_details[0]->bloodPressure <> 'No'?$mh_details[0]->bloodPressureRemark:'N/A';
    $STRmrk=$mh_details[0]->stomachTrouble <> 'No'?$mh_details[0]->stomachTroubleRemark:'N/A';
    $KSRmrk=$mh_details[0]->ksbu <> 'No'?$mh_details[0]->ksbuRemark:'N/A';
    $SURmrk=$mh_details[0]->su <> 'No'?$mh_details[0]->suRemark:'N/A';
    $EPRmrk=$mh_details[0]->epilepsy <> 'No'?$mh_details[0]->epilepsyRemark:'N/A';
    $NTRmrk=$mh_details[0]->nervousTrouble <> 'No'?$mh_details[0]->nervousTroubleRemark:'N/A';
    $MSRmrk=$mh_details[0]->motionSickness <> 'No'?$mh_details[0]->motionSicknessRemark:'N/A';
    $LIRmrk=$mh_details[0]->lifeInsurance <> 'No'?$mh_details[0]->lifeInsuranceRemark:'N/A';
    $HARmrk=$mh_details[0]->hospitalAdmission <> 'No'?$mh_details[0]->hospitalAdmissionRemark:'N/A';
    $OIRmrk=$mh_details[0]->otherIllness <> 'No'?$mh_details[0]->otherIllnessRemark:'N/A';
    $HIRmrk=$mh_details[0]->headInjury <> 'No'?$mh_details[0]->headInjuryRemark:'N/A';
    $MTRmrk=$mh_details[0]->MTDiseases <> 'No'?$mh_details[0]->MTDiseasesRemark:'N/A';
    $ARmrk=$mh_details[0]->addiction <> 'No'?$mh_details[0]->addictionRemark:'N/A';
    $FHRmrk=$mh_details[0]->familyHistory <> 'No'?$mh_details[0]->familyHistoryRemark:'N/A';
    $FIRmrk=$mh_details[0]->flyingLicense <> 'No'?$mh_details[0]->flyingLicenseRemark:'N/A';
    $DRmrk=$mh_details[0]->discharge <> 'No'?$mh_details[0]->dischargeRemark:'N/A';
    
    $html  .= '<h4 style="text-align:center;font-style:italic">C. Medical History Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of frequent or severe headaches?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->fsHeadaches.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$fsRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Dizziness,Fainting or Unconciousness?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->dfu.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$dfuRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Eye trouble?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->eyeTrouble.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$ETRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Hay Fever?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->hayFever.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$HRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Asthma?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->asthma.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$ASRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Heart trouble?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->heartTrouble.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$HTRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of High or Low Blood Pressure?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->bloodPressure.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$BPRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Stomach trouble?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->stomachTrouble.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$STRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Kidney Stones or Blood in Urine?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->ksbu.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$KSRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Sugar in Urine?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->su.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$SURmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Epilepsy?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->epilepsy.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$EPRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Nervous trouble of any sort?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->nervousTrouble.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$NTRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Motion Sickness requiring Drugs?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->motionSickness.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$MSRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Have you ever been refused Life Insurance?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->lifeInsurance.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$LIRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Have you ever been admitted to Hospital?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->hospitalAdmission.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$HARmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of any other Illness?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->otherIllness.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$OIRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Head Injury?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->headInjury.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$HIRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of Malaria/Tropical Diseases?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->MTDiseases.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$MTRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Have you ever been treated for Alcoholism/Drug addiction?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->addiction.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$ARmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Any member of the family with history of Diabetes,Epilepsy or Tuberculosis?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->familyHistory.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$FHRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Have you ever been refused a flying license?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->flyingLicense.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$FIRmrk.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Do you have a history of being discharged from Armed Forces on Medical Grounds?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$mh_details[0]->discharge.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$DRmrk.'</td>
                </tr>
            </table><p></p>';
    
    $html  .= '<h4 style="text-align:center;font-style:italic">D. Occupation/Employment Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Occupation</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$oc_details[0]->occupation.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Employer</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$oc_details[0]->employer.'</td>
                </tr>
            </table><p></p>';
    
    $html.='<br pagebreak="true">';
    $this->pdf->writeHTML($html);
    
    $this->pdf->SetY(35);
    $this->pdf->SetX(2);
    $this->pdf->SetFont('', '', 8);
    $this->pdf->Image($img_file, 5, 33, 200, 250, '', '', '', false, 300, '', false, false, 0);
    // set the starting point for the page content
    $this->pdf->setPageMark();
    
    $html  = '<h2 align="center">Medical Application Details</h2>';
    $html  .= '<h4>Application Reference : '.$applicationRef.'</h4>';
    $html  .= '<h4 style="text-align:center;font-style:italic">E. Health/Welfare Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Do you have an own General Practitioner?</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->gp.'</td>
                </tr>';
    
    if($ph_details[0]->gp === 'Yes'){
        
        $html.='<tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Name of General Practitioner</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->gpName.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Name of General Practitioner</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->gpAddress.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Name of General Practitioner</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->gpContact.'</td>
                </tr>';
    }
    
    $html.='<tr>
                <td style="width:1050px;">&nbsp;&nbsp;Do you have any medication presently being prescribed?</td>
                <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->medication.'</td>
            </tr>';
    
    if($ph_details[0]->medication === 'Yes'){
        
        $html.='<tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Description of prescribed medication</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->medDescription.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Purpose of prescribed medication</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->medPurpose.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Medication prescribing doctor</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ph_details[0]->medDoctor.'</td>
                </tr>';
    }
    
    if($ph_details[0]->noIllAccDis >= 1){
        
        $illAccDisDate=explode('&&',$ph_details[0]->IllAccDisDate);
        $illAccDisDetails=explode('&&',$ph_details[0]->IllAccDisDetails);
        $illAccDisDoctorName=explode('&&',$ph_details[0]->IllAccDisDoctorName);
        $illAccDisDoctorAddress=explode('&&',$ph_details[0]->IllAccDisDoctorAddress);
        $html.='<tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Illness,accidents or disabilities</td>
                    <td style="width:1050px;">
                        <table>
                            <tr>
                                <th style="width:200px;">&nbsp;&nbsp;Date</th>
                                <th style="width:350px;">&nbsp;&nbsp;Details</th>
                                <th style="width:250px;">&nbsp;&nbsp;Doctor Name</th>
                                <th style="width:250px;">&nbsp;&nbsp;Doctor Address</th>
                            </tr>';
                            for($i=0;$i<$ph_details[0]->noIllAccDis;$i++){
                                $html .='<tr>
                                            <td style="width:200px;">&nbsp;&nbsp;'.$illAccDisDate[$i].'</td>
                                            <td style="width:350px;">&nbsp;&nbsp;'.$illAccDisDetails[$i].'</td>
                                            <td style="width:250px;">&nbsp;&nbsp;'.$illAccDisDoctorName[$i].'</td>
                                            <td style="width:250px;">&nbsp;&nbsp;'.$illAccDisDoctorAddress[$i].'</td>
                                        </tr>';
                            }
                $html .='</table>';
            $html .='</td>';
        $html .='</tr>';
        
    }
    $html .='</table><p></p>';
    
    $license=$this->User_model->getlicenceTypes($ac_details[0]->licenceType,NULL,NULL);
    $html  .= '<h4 style="text-align:center;font-style:italic">F. Aircraft Experience Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Type of License held or applied for</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$license[0]->licenceType.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;License Number</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ac_details[0]->licenseNumber.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Expiry Date(s) of Last Medical Certificate(s)</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ac_details[0]->expDteLMC.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Expiry Date(s) of 5 Year License(s)</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ac_details[0]->expDte5YrL.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Total hours flown since last Medical Examination</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ac_details[0]->hrsFlown.'</td>
                </tr>';
        if($ac_details[0]->hrsFlown > 0){
        
        $aircraftRoute=explode('&&',$ac_details[0]->aircraftRoute);
        $aircraftType=explode('&&',$ac_details[0]->aircraftType);
        
        $html.='<tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Aircraft type(s) and route(s) flown since last medical examination</td>
                    <td style="width:1050px;">
                        <table>
                            <tr>
                                <th style="width:525px;">&nbsp;&nbsp;Type</th>
                                <th style="width:525px;">&nbsp;&nbsp;Route</th>
                            </tr>';
                            for($i=0;$i<count($aircraftType);$i++){
                                
                                $html .='<tr>
                                            <td style="width:525px;">&nbsp;&nbsp;'.$aircraftType[$i].'</td>
                                            <td style="width:525px;">&nbsp;&nbsp;'.$aircraftRoute[$i].'</td>
                                        </tr>';
                            }
                $html .='</table>';
            $html .='</td>';
        $html .='</tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Have you been involved in an Aircraft Accident since Last Medical Examination?</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$ac_details[0]->airAccident.'</td>
                </tr>';
        
    }
        
    if($ac_details[0]->airAccident == "Yes"){
        
        $airAccLocation=explode('&&',$ac_details[0]->airAccidentLocation);
        $airAccDate=explode('&&',$ac_details[0]->aircraftAccidentDate);
        
        $html.='<tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Aircraft accident date(s) and location(s) details</td>
                    <td style="width:1050px;">
                        <table>
                            <tr>
                                <th style="width:525px;">&nbsp;&nbsp;Date</th>
                                <th style="width:525px;">&nbsp;&nbsp;Location</th>
                            </tr>';
                            for($i=0;$i<$ac_details[0]->noAircraftAccident;$i++){
                                
                                $html .='<tr>
                                            <td style="width:525px;">&nbsp;&nbsp;'.$airAccDate[$i].'</td>
                                            <td style="width:525px;">&nbsp;&nbsp;'.$airAccLocation[$i].'</td>
                                        </tr>';
                            }
                $html .='</table>';
            $html .='</td>';
        $html .='</tr>';
        
    }
    $html .='</table><p></p>';
    
if($apm_details <> NULL){
    
    $html.='<br pagebreak="true">';
    $this->pdf->writeHTML($html);
    
    $this->pdf->SetY(35);
    $this->pdf->SetX(2);
    $this->pdf->SetFont('', '', 8);
    $this->pdf->Image($img_file, 5, 33, 200, 250, '', '', '', false, 300, '', false, false, 0);
    // set the starting point for the page content
    $this->pdf->setPageMark();
    
    $html  = '<h2 align="center">Medical Application Details</h2>';
    $html  .= '<h4>Application Reference : '.$applicationRef.'</h4>';
    $html  .= '<h4 style="text-align:center;font-style:italic">G. Appearance & Measurement Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Height</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->pHeight.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Weight</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->pHeight.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Chest Inspiration</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->pChestInsp.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Chest Expiration</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->pChestExp.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Waist</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->pWaist.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Identify Marks,Scars,Tattoos,Deformities</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->pMSTD.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Color of hair</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->hairColor.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Color of eyes</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->eyesColor.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Physical Impression</td>
                    <td style="width:1050px;">&nbsp;&nbsp;'.$apm_details[0]->physicalImpression.'</td>
                </tr>
            </table><p></p>';
}

if($py_details <> NULL){
    
    $html  .= '<h4 style="text-align:center;font-style:italic">H. Physical Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Head and Neck</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->headNeck.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->headNeckRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Mouth , Throat and Teeth</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->mtt.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->mttRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Sinuses</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->sinuses.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->sinusesRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Ears,Drums and Valsalva</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->edv.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->edvRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Lungs,Chest including breasts</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->lungsChest.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->lungsChestRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Heart Size,Auscultation</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->pheart.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->pheartRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Vascular System; Varicose Veins</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->vSystem.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->vSystemRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Pulse Rate (Sitting,Standing)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->pRate.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->pRateRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Blood Pressure - Systolic/Diastolic Pulse Rate</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->bp.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->bpRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Abdomen and Hernia</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->abdomenHernia.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->abdomenHerniaRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Liver,Spleen and Glands</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->livSplGla.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->livSplGlaRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Anus and Rectum (Haemorrhoids, Fistula, Prostate)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->anusRectum.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->anusRectumRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Genito-Urinary System</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->genito.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->genitoRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Endocrine System</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->endocrine.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->endocrineRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Upper limbs, Lower limbs and Joints</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->upperLowerJoints.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->upperLowerJointsRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Spine & Spinal movements</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->spine.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->spineRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Neurology (Reflexes,equilibrium etc)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->neurology.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->neurologyRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Skin</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->skin.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->skinRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Psychiatric</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$py_details[0]->psychiatric.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$py_details[0]->psychiatricRemark.'</td>
                </tr>
            </table><p></p>';
}

if($vs_details <> NULL){
    
    $html  .= '<h4 style="text-align:center;font-style:italic">I. Visual Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Lids, Pupils, Lens, Media, Fundi</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->LPLMF.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->LPLMFRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Does the candidate possess glasses?</td>
                    <td colspan="2" style="width:1000px;">&nbsp;&nbsp;'.$vs_details[0]->glassesP.'</td>
                </tr>';
                
    if($vs_details[0]->glassesP == 'Yes'){
                 
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Distant Vision with glasses (Standard Test Types)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->dVisionWithGRight.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->dVisionWithGLeft.'</td>
                </tr>';
    }
        
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Distant Vision without glasses (Standard Test Types)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->dVisionWithoutGRight.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->dVisionWithoutGLeft.'</td>
                </tr>';
    
    if($vs_details[0]->glassesP == 'Yes'){
                 
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Near Vision with glasses (N type at 30cm to 50cm)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->nVisionWithGRight.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->nVisionWithGLeft.'</td>
                </tr>';
    }
    
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Near Vision without glasses (N type at 30cm to 50cm)</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->nVisionWithoutGRight.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->nVisionWithoutGLeft.'</td>
                </tr>';
        
    if($vs_details[0]->glassesP == 'Yes'){
                 
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Accommodation in cm (Near point 30cm) with glasses</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->accomodationWithGRight.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->accomodationWithGLeft.'</td>
                </tr>';
    }
    
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Accommodation in cm (Near point 30cm) without glasses</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->accomodationWithoutGRight.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->accomodationWithoutGLeft.'</td>
                </tr>';
        
    if($vs_details[0]->glassesP == 'Yes'){
                 
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Prescription of the glasses</td>
                    <td colspan="2" style="width:1000px;">
                        <table>
                            <tr>
                                <th style="width:140px;"></th>
                                <th style="width:140px;">&nbsp;&nbsp;S (Right)</th>
                                <th style="width:140px;">&nbsp;&nbsp;C (Right)</th>
                                <th style="width:140px;">&nbsp;&nbsp;A (Right)</th>
                                <th style="width:140px;">&nbsp;&nbsp;S (Left)</th>
                                <th style="width:140px;">&nbsp;&nbsp;C (Left)</th>
                                <th style="width:140px;">&nbsp;&nbsp;A (Left)</th>
                            </tr>
                            <tr>
                                <td style="width:140px;">Distant</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->distantSright.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->distantCright.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->distantAright.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->distantSleft.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->distantCleft.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->distantAleft.'</td>
                            </tr>
                            <tr>
                                <td style="width:140px;">Near</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->nearSright.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->nearCright.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->nearAright.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->nearSleft.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->nearCleft.'</td>
                                <td style="width:140px;">&nbsp;&nbsp;'.$vs_details[0]->nearAleft.'</td>
                            </tr>';
                $html .='</table>
                    </td>
                </tr>';
    }
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Field of vision by confrontation test</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$vs_details[0]->fieldVision.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$vs_details[0]->fieldVisionRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Field of vision by confrontation test</td>
                    <td colspan="2" style="width:1000px;">&nbsp;&nbsp;'.$vs_details[0]->powerConvergence.'</td>
                </tr>
                </table><p></p>';
}

if($ey_details <> NULL){
    
    $html.='<br pagebreak="true">';
    $this->pdf->writeHTML($html);
    
    $this->pdf->SetY(35);
    $this->pdf->SetX(2);
    $this->pdf->SetFont('', '', 8);
    $this->pdf->Image($img_file, 5, 33, 200, 250, '', '', '', false, 300, '', false, false, 0);
    // set the starting point for the page content
    $this->pdf->setPageMark();
    
    $html  = '<h2 align="center">Medical Application Details</h2>';
    $html  .= '<h4>Application Reference : '.$applicationRef.'</h4>';
    $html  .= '<h4 style="text-align:center;font-style:italic">J. Eyes Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Manifest Hypermetropia with +2.5 dioptre lens</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ey_details[0]->manifestHyperRight.'</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ey_details[0]->manifestHyperLeft.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Color Perception (Tested by pseudoisochromatic [Ishihara] plates)</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ey_details[0]->cPerceptionIshihara.'</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ey_details[0]->cPerceptionIshiharaRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Color Perception (Tested by an approved Colour Perception Lantem[giles-Archer,Martin])</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ey_details[0]->cPerceptionLantem.'</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ey_details[0]->cPerceptionLantemRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Measure of Heterophoria</td>
                    <td colspan="2" style="width:1000px;">&nbsp;&nbsp;
                        <table>
                            <tr>
                                <th style="width:250px;">&nbsp;&nbsp;Date</th>
                                <th style="width:250px;">&nbsp;&nbsp;Exophoria</th>
                                <th style="width:250px;">&nbsp;&nbsp;Esophoria</th>
                                <th style="width:250px;">&nbsp;&nbsp;Hyperphoria</th>
                            </tr>
                            <tr>
                                <td style="width:250px;">&nbsp;&nbsp;Maddox Rod</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ey_details[0]->maddoxRodExo.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ey_details[0]->maddoxRodEso.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ey_details[0]->maddoxRodHyper.'</td>
                            </tr>
                            <tr>
                                <td style="width:250px;">&nbsp;&nbsp;Maddox Wing</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ey_details[0]->maddoxWingExo.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ey_details[0]->maddoxWingEso.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ey_details[0]->maddoxWingHyper.'</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><p></p>';
}

if($au_details <> NULL){
    
    $html  .= '<h4 style="text-align:center;font-style:italic">K. Audio Details</h4>';
    $html.='<table border="1">
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Any hearing difficulty with conversational voice at 8 feet with back to examiner?</td>
                    <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->hearingDifficulty.'</td>
                    <td style="width:800px;">&nbsp;&nbsp;'.$au_details[0]->hearingDifficultyRemark.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;At what distance from the examiner can forced whisper be heard in each ear separately?</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$au_details[0]->forcedWhisperRight.'</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$au_details[0]->forcedWhisperLeft.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Audiometry</td>
                    <td colspan="2" style="width:1000px;">&nbsp;&nbsp;
                        <table>
                            <tr>
                                <th style="width:200px;">&nbsp;&nbsp;Right</th>
                                <th style="width:2000px;">&nbsp;&nbsp;Frequency</th>
                                <th style="width:200px;">&nbsp;&nbsp;Left</th>
                                <th style="width:200px;">&nbsp;&nbsp;Max Loss</th>
                                <th style="width:200px;">&nbsp;&nbsp;Remarks</th>
                            </tr>
                            <tr>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry3000Right.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;3000</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry3000Left.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;50</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry3000Remark.'</td>
                            </tr>
                            <tr>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry2000Right.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;2000</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry2000Left.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;35</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry2000Remark.'</td>
                            </tr>
                            <tr>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry1000Right.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;1000</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry1000Left.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;35</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry1000Remark.'</td>
                            </tr>
                            <tr>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry500Right.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;500</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry500Left.'</td>
                                <td style="width:200px;">&nbsp;&nbsp;35</td>
                                <td style="width:200px;">&nbsp;&nbsp;'.$au_details[0]->audiometry500Remark.'</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><p></p>';
}

if($ot_details <> NULL){
    
    $html  .= '<h4 style="text-align:center;font-style:italic">L. Other Details</h4>';
    $html.='<table border="1">';
    
            if ($bs_details[0]->gender === "Female"){

                $html .='<tr>
                            <td style="width:1100px;">&nbsp;&nbsp;Last Menstruation Date</td>
                            <td style="width:200px;">&nbsp;&nbsp;'.$ot_details[0]->menstruationDate.'</td>
                            <td style="width:800px;">&nbsp;&nbsp;'.$ot_details[0]->menstruationDateRemark.'</td>
                        </tr>';
            }
        
        
        $html .='<tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Routine ECG Report</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ot_details[0]->ecgReport.'</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ot_details[0]->ecgNextDate.' (next ECG date)</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Routine CXR Report</td>
                    <td colspan="2" style="width:1000px;">&nbsp;&nbsp;'.$ot_details[0]->cxrReport.'</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Routine Audio Report</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ot_details[0]->audioReport.'</td>
                    <td style="width:500px;">&nbsp;&nbsp;'.$ot_details[0]->audioNextDate.' (next Audio date)</td>
                </tr>
                <tr>
                    <td style="width:1100px;">&nbsp;&nbsp;Urinalysis</td>
                    <td colspan="2" style="width:1000px;">&nbsp;&nbsp;
                        <table>
                            <tr>
                                <th style="width:250px;">&nbsp;&nbsp;Protein</th>
                                <th style="width:250px;">&nbsp;&nbsp;Glucose</th>
                                <th style="width:250px;">&nbsp;&nbsp;Other</th>
                                <th style="width:250px;">&nbsp;&nbsp;Remarks</th>
                            </tr>
                            <tr>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ot_details[0]->urinalysisProtein.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ot_details[0]->urinalysisGlucose.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ot_details[0]->urinalysisOther.'</td>
                                <td style="width:250px;">&nbsp;&nbsp;'.$ot_details[0]->urinalysisRemarks.'</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table><p></p>';
}

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Medical Applications Details.pdf', 'D');
exit;
?>


