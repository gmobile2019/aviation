<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
$img_file ='./img/plane1.jpg';
$this->pdf->Image($img_file, 5, 33, 200, 250, '', '', '', false, 300, '', false, false, 0);
// set the starting point for the page content
$this->pdf->setPageMark();
$dte_exam=explode("-",$examination_date[0]);
$html  = '<h2 align="center">Aviation Medical Certicate</h2>';
$html  .= '<h4>Application Reference : '.$applicationRef.'</h4>';
$html  .= '<h4>Examiner : '.$doctor[0]->first_name.' '.$doctor[0]->middle_name.' '.$doctor[0]->last_name.'</h4>';
$html  .= '<h4>Date : '.$dte_exam[2].'/'.$dte_exam[1].'/'.$dte_exam[0].'</h4>';

$html.='<table border="1">
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Name&nbsp;:&nbsp;'.$bs_details[0]->fname.' '.$bs_details[0]->mname.' '.$bs_details[0]->surname.'</td>
                    <td style="width:1050px;">&nbsp;&nbsp;Nationality&nbsp;:&nbsp;'.$bs_details[0]->nationality.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Date of birth&nbsp;:&nbsp;'.$bs_details[0]->dob.'</td>
                    <td style="width:1050px;">&nbsp;&nbsp;Sex&nbsp;:&nbsp;'.$bs_details[0]->gender.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Height&nbsp;:&nbsp;'.$apm_details[0]->pHeight.'</td>
                    <td style="width:1050px;">&nbsp;&nbsp;Weight&nbsp;:&nbsp;'.$apm_details[0]->pWeight.'</td>
                </tr>
                <tr>
                    <td style="width:1050px;">&nbsp;&nbsp;Licence Type&nbsp;:&nbsp;'.$lType[0]->licenceType.'</td>
                    <td style="width:1050px;">&nbsp;&nbsp;Licence Number&nbsp;:&nbsp;'.$ac_details[0]->licenseNumber.'</td>
                </tr>
            </table><p></p>';

$html  .= '<h4 style="text-align:center;font-style:italic">Advisory Information</h4>';
$html.='<table border="1">
            <tr>
                <th style="width:700px;">&nbsp;&nbsp;</th>
                <th style="width:700px;">&nbsp;&nbsp;Date of Examination</th>
                <th style="width:700px;">&nbsp;&nbsp;Date of next Examination</th>
            </tr>
            <tr>
                <td style="width:700px;">&nbsp;&nbsp;<b>Electrocardiogram</b></td>
                <td style="width:700px;">&nbsp;&nbsp;'.$ecg[0]->attendedOn.'</td>
                <td style="width:700px;">&nbsp;&nbsp;'.$ecg[0]->ecgNextDate.'</td>
            </tr>
            <tr>
                <td style="width:700px;">&nbsp;&nbsp;<b>Audiogram</b></td>
                <td style="width:700px;">&nbsp;&nbsp;'.$audio[0]->attendedOn.'</td>
                <td style="width:700px;">&nbsp;&nbsp;'.$audio[0]->audioNextDate.'</td>
            </tr>
        </table><p></p>';
$html.='<table border="1">
            <tr>
                <td style="width:2100px;">&nbsp;&nbsp;<b>Medical Result&nbsp;:&nbsp;</b>'.$ot_details[0]->mhRemarks.'</td>
            </tr>
        </table><p></p>';
$html.='<div>
            <b>Limitations</b>&nbsp;:&nbsp;
            <p>__________________________________________________________________________________________________________________</p>
            <p>__________________________________________________________________________________________________________________</p>
            <p>__________________________________________________________________________________________________________________</p>
            <p>__________________________________________________________________________________________________________________</p>
            <p>__________________________________________________________________________________________________________________</p>
           
        </div><p></p>';
$html  .= '<h4 style="text-align:center;font-style:italic">Conditions</h4>';
$html .='<p style="font-style: italic;font-weight: bold;font-family: serif">
            A holder of this licence shall not exercise the privileges of his/her license and related ratings at any time when the holder
            is aware of any decrease in his medical fitness which might render the holder unable to safely and properly exercise those privileges.
        </p>';
$html  .= '<h4 style="text-align:center;font-style:italic">Validity Periods</h4>';
$html  .='<table border="1">
            <tr>
                <th style="width:700px;">&nbsp;&nbsp;<b>Class</b></th>
                <th style="width:700px;">&nbsp;&nbsp;<b>Licence Type</b></th>
                <th style="width:700px;">&nbsp;&nbsp;<b>Validity</b></th>
            </tr>
            <tr>
                <td style="width:700px;">&nbsp;&nbsp;'.$lType[0]->certClass.'</td>
                <td style="width:700px;">&nbsp;&nbsp;'.$lType[0]->licenceType.'</td>
                <td style="width:700px;">&nbsp;&nbsp;'.$validity.'</td>
            </tr>
        </table><p></p>';
$html  .='<table border="1">
            <tr>
                <th colspan="2" style="width:2100px;">&nbsp;&nbsp;<b>For all medical classes</b></th>
            </tr>
            <tr>
                <td style="width:1050px;">&nbsp;&nbsp;Electrocardiogram (ECG) Examination validity</td>
                <td style="width:1050px;">&nbsp;&nbsp;'.$ecg_validity.'</td>
            </tr>
            <tr>
                <td style="width:1050px;">&nbsp;&nbsp;Audiogram Examination validity</td>
                <td style="width:1050px;">&nbsp;&nbsp;'.$audio_validity.'</td>
            </tr>
        </table><p></p>';
$html  .='<p></p>
        <p></p>
        <p></p>
        <p></p>
        <p>
            Examiner Signature&nbsp;&nbsp;&nbsp;&nbsp;_________________________
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           Stamp&nbsp;&nbsp;&nbsp;__________________
        </p>
        <p></p>
        <p></p>
        <p>
            Examinee Signature&nbsp;&nbsp;&nbsp;&nbsp;_________________________
        </p>';


$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Aviation Medical Certificate.pdf', 'D');
exit;

