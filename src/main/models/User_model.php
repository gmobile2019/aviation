<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);
    }
    
    function save_signUp($data,$key){
           
            if($key <> null){
                
                return $this->main->update('registrations',$data,array('email'=>$key));
            }
           return $this->main->insert('registrations',$data);
    }
    
    function save_feedback($data,$id){
           
        if($id <> NULL){

            return $this->main->update('user_feedback',$data,array('id'=>$id,'status'=>'New'));
        }
            
        return $this->main->insert('user_feedback',$data);
    }
    
    function registrationInfo($sessionID,$email,$status){
        
        if($sessionID <> NULL){
            
            $where .=" AND sessionID='$sessionID'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($email <> NULL){
            
            $where .=" AND email='$email'";
        }
        
        return $this->main->query("SELECT id,title,fname,mname,surname,gender,"
                                    . "mobile,dob,pob,paddress,postal,nationality,email,activationLink,sessionID,ipaddress,"
                                    . "expiryTimestamp,createdon,status,lastupdate "
                                    . "FROM registrations "
                                    . "WHERE id is not null $where "
                                    . "ORDER BY createdon DESC ")->result();
    }
    
    function close_registration($data){
        
        return $this->main->query("UPDATE registrations "
                                    . "SET lastupdate='".$data['lastupdate']."',status='".$data['status']."'"
                                    . "WHERE sessionID='".$data['sessionID']."' $where ");
    }
    
    function delete_registration($sessionID){
        
        return $this->main->delete('registrations',array('sessionID'=>$sessionID));
    }
    
    function save_userData($user,$account,$key){
        
        if($account <> NULL){
            
            $this->main->update('users',$account,array('username'=>$key));
        }
        
        return $this->main->update('registrations',$user,array('email'=>$key));
    }
    
    function save_background($data,$applications,$id){
        
        if($id <> NULL){
                   
            
                   $data['modifiedOn']=date('Y-m-d H:i:s');
                   $this->main->update('background_information',$data,array('applicant'=>$this->session->userdata('username')));
            return $this->main->update('background_details',$data,array('id'=>$id));
        }
        
        if(!$this->save_application($applications)){
            
            return FALSE;
        }
        
        $data['createdOn']=date('Y-m-d H:i:s');

        //save basic background information
        if($this->background_information(NULL,$this->session->userdata('username')) <> NULL){
            $this->main->update('background_information',$data,array('applicant'=>$this->session->userdata('username')));
        }else{
            $this->main->insert('background_information',$data);
        }
               
        return $this->main->insert('background_details',$data);
    }
    
    function save_application($data,$referenceNo){
        
        if($referenceNo <> NULL){
            
            return $this->main->update('medical_applications',$data,array('applicationRef'=>$referenceNo));
        }
        
        return $this->main->insert('medical_applications',$data);
    }
    
    function save_medicalHistory($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
                   $this->main->update('medicalhistory_information',$data,array('applicant'=>$this->session->userdata('username')));
            return $this->main->update('medicalhistory_details',$data,array('id'=>$id));
        }
        
        $data['createdOn']=date('Y-m-d H:i:s');

        if($this->medicalhistory_information(NULL,$this->session->userdata('username')) <> NULL){
            $this->main->update('medicalhistory_information',$data,array('applicant'=>$this->session->userdata('username')));
        }else{
            $this->main->insert('medicalhistory_information',$data);
        }
            
        return $this->main->insert('medicalhistory_details',$data);
    }
    
    function save_occupation($data,$applications,$id,$appStart){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('occupation_details',$data,array('id'=>$id));
        }
        
        if(!$this->save_application($applications) && $appStart == TRUE){
           
            return FALSE;
        }
        
        if($this->background_details($data['applicationRef'], $this->session->userdata('username')) == NULL){
            
            $bg=$this->background_information(NULL,$this->session->userdata('username'));
        
            $bg_data=array(
                'pilotExperience'=>$bg[0]->pilotExperience,
                'armedForce'=>$bg[0]->armedForce,
                'applicationRef'=>$data['applicationRef'],
                'createdOn'=>date('Y-m-d H:i:s'),
                'applicant'=>$this->session->userdata('username')
            );
            
            $this->main->insert('background_details',$bg_data);
        }
       
        if($this->medicalHistory_details($data['applicationRef'],$this->session->userdata('username')) == NULL){
            
            $mh=$this->medicalhistory_information(NULL,$this->session->userdata('username'));
        
            $mh_data=array(
                'applicationRef'=>$data['applicationRef'],
                'fsHeadaches'=>$mh[0]->fsHeadaches,
                'fsHeadachesRemark'=>$mh[0]->fsHeadachesRemark,
                'dfu'=>$mh[0]->dfu,
                'dfuRemark'=>$mh[0]->dfuRemark,
                'eyeTrouble'=>$mh[0]->eyeTrouble,
                'eyeTroubleRemark'=>$mh[0]->eyeTroubleRemark,
                'hayFever'=>$mh[0]->hayFever,
                'hayFeverRemark'=>$mh[0]->hayFeverRemark,
                'asthma'=>$mh[0]->asthma,
                'asthmaRemark'=>$mh[0]->asthmaRemark,
                'heartTrouble'=>$mh[0]->heartTrouble,
                'heartTroubleRemark'=>$mh[0]->heartTroubleRemark,
                'bloodPressure'=>$mh[0]->bloodPressure,
                'bloodPressureRemark'=>$mh[0]->bloodPressureRemark,
                'stomachTrouble'=>$mh[0]->stomachTrouble,
                'stomachTroubleRemark'=>$mh[0]->stomachTroubleRemark,
                'ksbu'=>$mh[0]->ksbu,
                'ksbuRemark'=>$mh[0]->ksbuRemark,
                'su'=>$mh[0]->su,
                'suRemark'=>$mh[0]->suRemark,
                'epilepsy'=>$mh[0]->epilepsy,
                'epilepsyRemark'=>$mh[0]->epilepsyRemark,
                'nervousTrouble'=>$mh[0]->nervousTrouble,
                'nervousTroubleRemark'=>$mh[0]->nervousTroubleRemark,
                'motionSickness'=>$mh[0]->motionSickness,
                'motionSicknessRemark'=>$mh[0]->motionSicknessRemark,
                'lifeInsurance'=>$mh[0]->lifeInsurance,
                'lifeInsuranceRemark'=>$mh[0]->lifeInsuranceRemark,
                'hospitalAdmission'=>$mh[0]->hospitalAdmission,
                'hospitalAdmissionRemark'=>$mh[0]->hospitalAdmissionRemark,
                'otherIllness'=>$mh[0]->otherIllness,
                'otherIllnessRemark'=>$mh[0]->otherIllnessRemark,
                'headInjury'=>$mh[0]->headInjury,
                'headInjuryRemark'=>$mh[0]->headInjuryRemark,
                'MTDiseases'=>$mh[0]->MTDiseases,
                'MTDiseasesRemark'=>$mh[0]->MTDiseasesRemark,
                'addiction'=>$mh[0]->addiction,
                'addictionRemark'=>$mh[0]->addictionRemark,
                'familyHistory'=>$mh[0]->familyHistory,
                'familyHistoryRemark'=>$mh[0]->familyHistoryRemark,
                'flyingLicense'=>$mh[0]->flyingLicense,
                'flyingLicenseRemark'=>$mh[0]->flyingLicenseRemark,
                'discharge'=>$mh[0]->discharge,
                'dischargeRemark'=>$mh[0]->dischargeRemark,
                'createdOn'=>date('Y-m-d H:i:s'),
                'applicant'=>$this->session->userdata('username')
            );
            
            $this->main->insert('medicalhistory_details',$mh_data);
        }
        
        
                $data['createdOn']=date('Y-m-d H:i:s');
        return $this->main->insert('occupation_details',$data);
    }
    
    function save_personalHealth($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('personalhealth_details',$data,array('id'=>$id));
        }
        
               $data['createdOn']=date('Y-m-d H:i:s');
        return $this->main->insert('personalhealth_details',$data);
    }
    
    function save_aircraftExperience($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('aircraft_details',$data,array('id'=>$id));
        }
        
               $data['createdOn']=date('Y-m-d H:i:s');
        return $this->main->insert('aircraft_details',$data);
    }
    
    function save_appearanceMeasurement($data,$application,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('appearance_measurement_details',$data,array('id'=>$id));
        }
              
        if(!$this->save_application($application,$data['applicationRef'])){

             return FALSE;
        }
         
               $data['attendedOn']=date('Y-m-d H:i:s');
        return $this->main->insert('appearance_measurement_details',$data);
    }
    
    function save_detailedPhysical($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('physical_details',$data,array('id'=>$id));
        }
        
               $data['attendedOn']=date('Y-m-d H:i:s');
        return $this->main->insert('physical_details',$data);
    }
    
    function save_visual($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('visual_details',$data,array('id'=>$id));
        }
        
               $data['attendedOn']=date('Y-m-d H:i:s');
        return $this->main->insert('visual_details',$data);
    }
    
    function save_eye($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('eye_details',$data,array('id'=>$id));
        }
        
               $data['attendedOn']=date('Y-m-d H:i:s');
        return $this->main->insert('eye_details',$data);
    }
    
    function save_audio($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('audio_details',$data,array('id'=>$id));
        }
        
               $data['attendedOn']=date('Y-m-d H:i:s');
        return $this->main->insert('audio_details',$data);
    }
    
    function save_other($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedOn']=date('Y-m-d H:i:s');
            return $this->main->update('other_details',$data,array('id'=>$id));
        }
        
               $data['attendedOn']=date('Y-m-d H:i:s');
        return $this->main->insert('other_details',$data);
    }
    
    function medical_applications($referenceNo,$applicant,$doctor,$facility,$status){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($facility <> NULL){
            
            $where .=" AND facility='$facility'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,applicationRef,applicant,"
                                . "appliedOn,completedOn,status,"
                                . "attendedBy,attendedOn,facility "
                                . "FROM medical_applications WHERE id is not null $where "
                                . "ORDER BY appliedOn DESC")->result();
    }
    
    function medical_apps($referenceNo,$applicant,$start,$end,$status,$facility){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef LIKE '%$referenceNo%'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant LIKE '$applicant'";
        }
        
        if($start <> NULL){
            
            $where .=" AND appliedOn >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND appliedOn <='$end 23:59:59'";
        }
        
        if($facility <> NULL){
            
            $where .=" AND facility='$facility'";
        }
        
        return $this->main->query("SELECT id,applicationRef,applicant,"
                                . "appliedOn,completedOn,status,"
                                . "attendedBy,attendedOn,facility "
                                . "FROM medical_applications WHERE id is not null $where "
                                . "ORDER BY appliedOn DESC")->result();
    }
    
    function feedback_info($start,$end,$status,$page,$limit){
        
        if($start <> NULL){
            
            $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,feedbackSubject,mobileNumber,"
                                . "feedbackemail,status,"
                                . "feedbackcomments,createdon,readon "
                                . "FROM user_feedback WHERE id is not null $where "
                                . "ORDER BY createdon DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function feedback_info_count($start,$end,$status){
        
       
        if($start <> NULL){
            
            $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return count($this->main->query("SELECT id,feedbackSubject,mobileNumber,"
                                . "feedbackemail,status,"
                                . "feedbackcomments,createdon,readon "
                                . "FROM user_feedback WHERE id is not null $where")->result());
    }
    
    function user_feedback($id,$start,$end,$status){
        
        if($id <> NULL){
            
            $where .=" AND id ='$id'";
        }
        
        if($start <> NULL){
            
            $where .=" AND createdon >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND createdon <='$end 23:59:59'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->main->query("SELECT id,feedbackSubject,mobileNumber,"
                                . "feedbackemail,status,"
                                . "feedbackcomments,createdon,readon "
                                . "FROM user_feedback WHERE id is not null $where "
                                . "ORDER BY createdon DESC")->result();
    }
    
    function application_info($referenceNo,$applicant,$start,$end,$status,$facility,$page,$limit){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($start <> NULL){
            
            $where .=" AND appliedOn >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND appliedOn <='$end 23:59:59'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($facility <> NULL){
            
            $where .=" AND facility='$facility'";
        }
        
        return $this->main->query("SELECT id,applicationRef,applicant,"
                                . "appliedOn,completedOn,status,"
                                . "attendedBy,attendedOn,facility "
                                . "FROM medical_applications WHERE id is not null $where "
                                . "ORDER BY appliedOn DESC "
                                . "LIMIT $page,$limit")->result();
    }
    
    function application_info_count($referenceNo,$applicant,$start,$end,$status,$facility){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($start <> NULL){
            
            $where .=" AND appliedOn >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND appliedOn <='$end 23:59:59'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($facility <> NULL){
            
            $where .=" AND facility='$facility'";
        }
        
        return count($this->main->query("SELECT id,applicationRef,applicant,"
                                . "appliedOn,completedOn,status,"
                                . "attendedBy,attendedOn,facility "
                                . "FROM medical_applications WHERE id is not null $where")->result());
    }
    
    function background_details($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format === "Array"){
            
            return $this->main->query("SELECT id,applicationRef,pilotExperience,"
                                . "armedForce,applicant,createdOn,modifiedOn "
                                . "FROM background_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,pilotExperience,"
                                . "armedForce,applicant,createdOn,modifiedOn "
                                . "FROM background_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function background_information($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format === "Array"){
            
            return $this->main->query("SELECT id,applicationRef,pilotExperience,"
                                . "armedForce,applicant,createdOn,modifiedOn "
                                . "FROM background_information WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,pilotExperience,"
                                . "armedForce,applicant,createdOn,modifiedOn "
                                . "FROM background_information WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function personalhealth_details($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format === "Array"){
            
            return $this->main->query("SELECT id,applicationRef,gp,"
                                . "gpName,gpAddress,gpContact,"
                                . "medication,medDescription,medPurpose,medDoctor,"
                                . "noIllAccDis,IllAccDisDate,IllAccDisDetails,IllAccDisDoctorName,"
                                . "IllAccDisDoctorAddress,createdOn,modifiedOn,applicant "
                                . "FROM personalhealth_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,gp,"
                                . "gpName,gpAddress,gpContact,"
                                . "medication,medDescription,medPurpose,medDoctor,"
                                . "noIllAccDis,IllAccDisDate,IllAccDisDetails,IllAccDisDoctorName,"
                                . "IllAccDisDoctorAddress,createdOn,modifiedOn,applicant "
                                . "FROM personalhealth_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function medicalHistory_details($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format == 'Array'){
            
            return $this->main->query("SELECT * FROM medicalhistory_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT * FROM medicalhistory_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function medicalhistory_information($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format == 'Array'){
            
            return $this->main->query("SELECT * FROM medicalhistory_information WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT * FROM medicalhistory_information WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function occupation_details($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format === 'Array'){
           
            return $this->main->query("SELECT id,applicationRef,occupation,"
                                . "employer,applicant,createdOn,modifiedOn "
                                . "FROM occupation_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,occupation,"
                                . "employer,applicant,createdOn,modifiedOn "
                                . "FROM occupation_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function aircraft_details($referenceNo,$applicant,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($applicant <> NULL){
            
            $where .=" AND applicant='$applicant'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,licenceType,"
                                . "licenseNumber,expDteLMC,expDte5YrL,"
                                . "hrsFlown,airAccident,noAircraftAccident,aircraftAccidentDate,airAccidentLocation,"
                                . "noAircraftType,noAircraftRoute,aircraftRoute,aircraftType,"
                                . "applicant,createdOn,modifiedOn "
                                . "FROM aircraft_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,licenceType,"
                                . "licenseNumber,expDteLMC,expDte5YrL,"
                                . "hrsFlown,airAccident,noAircraftAccident,aircraftAccidentDate,airAccidentLocation,"
                                . "noAircraftType,noAircraftRoute,aircraftRoute,aircraftType,"
                                . "applicant,createdOn,modifiedOn "
                                . "FROM aircraft_details WHERE id is not null $where "
                                . "ORDER BY createdOn DESC")->result();
    }
    
    function appearanceMeasurement_details($referenceNo,$doctor,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,pHeight,"
                                . "pWeight,pChestInsp,pChestExp,"
                                . "pWaist,pMSTD,hairColor,eyesColor,physicalImpression,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM appearance_measurement_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,pHeight,"
                                . "pWeight,pChestInsp,pChestExp,"
                                . "pWaist,pMSTD,hairColor,eyesColor,physicalImpression,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM appearance_measurement_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function physical_details($referenceNo,$doctor,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,headNeck,"
                                . "headNeckRemark,mtt,mttRemark,"
                                . "sinuses,sinusesRemark,edv,edvRemark,lungsChest,lungsChestRemark,"
                                . "pheart,pheartRemark,vSystem,vSystemRemark,pRate,pRateRemark,"
                                . "bp,bpRemark,abdomenHernia,abdomenHerniaRemark,livSplGla,livSplGlaRemark,"
                                . "anusRectum,anusRectumRemark,genito,genitoRemark,endocrine,endocrineRemark,"
                                . "upperLowerJoints,upperLowerJointsRemark,spine,spineRemark,neurology,neurologyRemark,"
                                . "skin,skinRemark,psychiatric,psychiatricRemark,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM  physical_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,headNeck,"
                                . "headNeckRemark,mtt,mttRemark,"
                                . "sinuses,sinusesRemark,edv,edvRemark,lungsChest,lungsChestRemark,"
                                . "pheart,pheartRemark,vSystem,vSystemRemark,pRate,pRateRemark,"
                                . "bp,bpRemark,abdomenHernia,abdomenHerniaRemark,livSplGla,livSplGlaRemark,"
                                . "anusRectum,anusRectumRemark,genito,genitoRemark,endocrine,endocrineRemark,"
                                . "upperLowerJoints,upperLowerJointsRemark,spine,spineRemark,neurology,neurologyRemark,"
                                . "skin,skinRemark,psychiatric,psychiatricRemark,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM  physical_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function visual_details($referenceNo,$doctor,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,LPLMF,"
                                . "LPLMFRemark,dVisionWithGRight,dVisionWithGLeft,"
                                . "dVisionWithoutGRight,dVisionWithoutGLeft,nVisionWithGRight,nVisionWithGLeft,nVisionWithoutGRight,"
                                . "nVisionWithoutGLeft,accomodationWithGRight,accomodationWithGLeft,accomodationWithoutGRight,accomodationWithoutGLeft,"
                                . "glassesP,distantSright,distantCright,distantAright,distantSleft,distantCleft,"
                                . "distantAleft,nearSright,nearCright,nearAright,nearSleft,nearCleft,"
                                . "fieldVision,fieldVisionRemark,powerConvergence,"
                                . "nearAleft,applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM visual_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,LPLMF,"
                                . "LPLMFRemark,dVisionWithGRight,dVisionWithGLeft,"
                                . "dVisionWithoutGRight,dVisionWithoutGLeft,nVisionWithGRight,nVisionWithGLeft,nVisionWithoutGRight,"
                                . "nVisionWithoutGLeft,accomodationWithGRight,accomodationWithGLeft,accomodationWithoutGRight,accomodationWithoutGLeft,"
                                . "glassesP,distantSright,distantCright,distantAright,distantSleft,distantCleft,"
                                . "distantAleft,nearSright,nearCright,nearAright,nearSleft,nearCleft,"
                                . "fieldVision,fieldVisionRemark,powerConvergence,"
                                . "nearAleft,applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM visual_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function eye_details($referenceNo,$doctor,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,manifestHyperRight,"
                                . "manifestHyperLeft,cPerceptionIshihara,cPerceptionIshiharaRemark,"
                                . "cPerceptionLantem,cPerceptionLantemRemark,maddoxRodExo,maddoxRodEso,maddoxRodHyper,"
                                . "maddoxWingExo,maddoxWingEso,maddoxWingHyper,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM eye_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,manifestHyperRight,"
                                . "manifestHyperLeft,cPerceptionIshihara,cPerceptionIshiharaRemark,"
                                . "cPerceptionLantem,cPerceptionLantemRemark,maddoxRodExo,maddoxRodEso,maddoxRodHyper,"
                                . "maddoxWingExo,maddoxWingEso,maddoxWingHyper,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM eye_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function audio_details($referenceNo,$doctor,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,hearingDifficulty,"
                                . "hearingDifficultyRemark,forcedWhisperRight,forcedWhisperLeft,"
                                . "audiometry3000Right,audiometry3000Left,audiometry3000Remark,"
                                . "audiometry2000Right,audiometry2000Left,audiometry2000Remark,"
                                . "audiometry1000Right,audiometry1000Left,audiometry1000Remark,"
                                . "audiometry500Right,audiometry500Left,audiometry500Remark,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM audio_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,hearingDifficulty,"
                                . "hearingDifficultyRemark,forcedWhisperRight,forcedWhisperLeft,"
                                . "audiometry3000Right,audiometry3000Left,audiometry3000Remark,"
                                . "audiometry2000Right,audiometry2000Left,audiometry2000Remark,"
                                . "audiometry1000Right,audiometry1000Left,audiometry1000Remark,"
                                . "audiometry500Right,audiometry500Left,audiometry500Remark,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM audio_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function other_details($referenceNo,$doctor,$format){
        
        if($referenceNo <> NULL){
            
            $where .=" AND applicationRef='$referenceNo'";
        }
        
        if($doctor <> NULL){
            
            $where .=" AND attendedBy='$doctor'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,applicationRef,menstruationDate,"
                                . "menstruationDateRemark,ecgReportFile,ecgReport,ecgNextDate,"
                                . "cxrReport,audioReport,audioNextDate,"
                                . "urinalysisProtein,urinalysisGlucose,urinalysisOther,urinalysisRemarks,mhRemarks,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM other_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result_array();
        }
        
        return $this->main->query("SELECT id,applicationRef,menstruationDate,"
                                . "menstruationDateRemark,ecgReportFile,ecgReport,ecgNextDate,"
                                . "cxrReport,audioReport,audioNextDate,"
                                . "urinalysisProtein,urinalysisGlucose,urinalysisOther,urinalysisRemarks,mhRemarks,"
                                . "applicant,attendedBy,attendedOn,modifiedOn "
                                . "FROM other_details WHERE id is not null $where "
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function getlicenceTypes($id,$licenceType,$status,$class,$format){
        
        if($id <> NULL){
            
            $where .=" AND id='$id'";
        }
        
        if($licenceType <> NULL){
            
            $where .=" AND licenceType LIKE '%$licenceType%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($class <> NULL){
            
            $where .=" AND certClass='$class'";
        }
        
        if($format === 'Array'){
            
            return $this->main->query("SELECT id,licenceType,certClass,status,"
                                . "createdOn,createdBy,modifiedOn,modifiedBy "
                                . "FROM license_types WHERE id is not null $where "
                                . "ORDER BY licenceType ASC")->result_array();
        }
        
        return $this->main->query("SELECT id,licenceType,certClass,status,"
                                . "createdOn,createdBy,modifiedOn,modifiedBy "
                                . "FROM license_types WHERE id is not null $where "
                                . "ORDER BY licenceType ASC")->result();
    }
    
    function dateDifference($date_1 , $date_2 , $differenceFormat){
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);
        
        return $interval;
   
    }
    
    function getLastEcgExamination($applicant){
        
        return $this->main->query("SELECT id,applicationRef,ecgReportFile,ecgReport,DATE_FORMAT(ecgNextDate,'%d/%m/%Y') AS ecgNextDate,"
                                . "audioReport,audioNextDate,"
                                . "applicant,attendedBy,DATE_FORMAT(attendedOn,'%d/%m/%Y') AS attendedOn,modifiedOn "
                                . "FROM other_details WHERE ecgNextDate is not null AND applicant='$applicant'"
                                . "ORDER BY attendedOn DESC")->result();
    }
    
    function getLastAudioExamination($applicant){
        
        return $this->main->query("SELECT id,applicationRef,audioReport,DATE_FORMAT(audioNextDate,'%d/%m/%Y') AS audioNextDate,"
                                . "applicant,attendedBy,DATE_FORMAT(attendedOn,'%d/%m/%Y') AS attendedOn,modifiedOn "
                                . "FROM other_details WHERE audioNextDate is not null AND applicant='$applicant'"
                                . "ORDER BY attendedOn DESC")->result();
    }
}
