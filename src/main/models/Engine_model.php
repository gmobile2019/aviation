<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Engine_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);
    }
    
    function user_registrations($status,$index,$nullIndex){
           
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($index <> NULL){
            
            $where .=" AND prEmailInstance='$index'";
        }
        
        if($nullIndex){
            
            $where .=" AND prEmailInstance is null";
        }
     
        return $this->main->query("SELECT id,fname,mname,surname,"
                                    . "mobile,email,activationLink,sessionID,ipaddress,"
                                    . "expiryTimestamp,createdon,status,lastupdate "
                                    . "FROM registrations "
                                    . "WHERE id is not null $where "
                                    . "ORDER BY createdon DESC "
                                    . "LIMIT 0,".$this->config->item('db_query_limit'))->result();
    }
    
    function save_sent_email_registrations($data){
          
        $this->main->trans_start();
        
        foreach($data as $key=>$value){
            
            $this->main->query("UPDATE registrations SET status='".$value['status']."',lastupdate='".$value['lastupdate']."' WHERE id='".$value['id']."' AND status='3'");
        }
        
        $this->main->trans_complete();
        
        return $this->main->trans_status();
    }
    
    function save_balanced_registrations($data){
          
        $this->main->trans_start();
        
        foreach($data as $key=>$value){
            
            $this->main->query("UPDATE registrations SET prEmailInstance='".$value['prEmailInstance']."' WHERE id='".$value['id']."' AND prEmailInstance is null");
        }
        
        $this->main->trans_complete();
        
        return $this->main->trans_status();
    }
}