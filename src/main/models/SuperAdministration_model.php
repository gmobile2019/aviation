<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SuperAdministration_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
            $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
            $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }
    
    public function save_accountDetails($account,$key){
        
        return $this->main->update('users',$account,array('id'=>$key));
    }
    
    function current_user_info(){

           return $this->main->query("SELECT users.id,users.username,users.first_name,users.middle_name,users.last_name,users.msisdn,users.email,users.photo "
                   . "FROM users "
                   . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function profile_data(){
        
        return $this->main->query("SELECT users.id,users.password,users.salt,users.first_name,users.middle_name,"
                . "users.last_name,users.username,users.email,"
                . "users.msisdn,users.photo,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id "
                . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function users_info_count($name,$group,$institution,$status){
        
        if($name <> null){
           $where .=" AND (u.first_name LIKE '%$name%' OR u.middle_name LIKE '%$name%' OR u.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND ug.group_id='$group'"; 
        }
        
        if($institution <> null){
            
            $where .=" AND u.institution='$institution'"; 
        }
        
        if($status <> null){
            
            $where .=" AND u.active='$status'"; 
        }
        
        return count($this->main->query("SELECT u.id,u.first_name,"
                . "u.middle_name,u.last_name,u.email,"
                . "u.msisdn,u.username,u.active,ug.group_id,g.name as groupname,i.institutionname "
                . "FROM users  AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "INNER JOIN groups AS g ON g.id=ug.group_id "
                . "LEFT OUTER JOIN institutions AS i ON i.id=u.institution "
                . "WHERE u.id is not null $where "
                . "ORDER BY u.first_name ASC")->result());
    }
    
    function users_info($name,$group,$institution,$status,$page,$limit){
        
        if($name <> null){
           $where .=" AND (u.first_name LIKE '%$name%' OR u.middle_name LIKE '%$name%' OR u.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND ug.group_id='$group'"; 
        }
        
        if($institution <> null){
            
            $where .=" AND u.institution='$institution'"; 
        }
        
        if($status <> null){
            
            $where .=" AND u.active='$status'"; 
        }
        
        return $this->main->query("SELECT u.id,u.first_name,"
                . "u.middle_name,u.last_name,u.email,"
                . "u.msisdn,u.username,u.active,ug.group_id,g.name as groupname,i.institutionname "
                . "FROM users  AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "INNER JOIN groups AS g ON g.id=ug.group_id "
                . "LEFT OUTER JOIN institutions AS i ON i.institutioncode=u.institution "
                . "WHERE u.id is not null $where "
                . "ORDER BY u.first_name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function groups($id){
        
        if($id <> null){
            $where=" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description FROM groups $where")->result();
    }
    
    function registration_groups($id){
        
        if($id <> null){
            $where=" AND id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description FROM groups WHERE id !=1 $where")->result();
    }
    
    function get_registration_groups($group_id){
        
        if($group_id == 1){
            
            $where =" WHERE id IN ('1','2')";
        }
        
        return $this->main->query("SELECT id,name FROM groups $where ORDER BY name ASC")->result();
    }
    
    function get_member_info($id,$username){
        
        if($id <> null){
            
            $where .=" AND u.id='$id'";
            
        }
       
        if($username <> null){
            
            $where .=" AND u.username LIKE '$username'";
            
        }
        
        return $this->main->query("SELECT u.id,u.first_name,"
                . "u.middle_name,u.last_name,u.email,"
                . "u.msisdn,u.username,u.institution,u.active,ug.group_id,g.name as groupname,i.institutionname "
                . "FROM users  AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "INNER JOIN groups AS g ON g.id=ug.group_id "
                . "LEFT OUTER JOIN institutions AS i ON i.id=u.institution "
                . "WHERE u.id is not null $where "
                . "ORDER BY u.first_name ASC")->result();
       
    }
    
    function institutions($id,$code,$status){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        if($code <> null){
            
            $where .=" AND institutioncode='$code'";
        }
        
        if($status <> null){
            
            $where .=" AND status='$status'";
        }
       
        return $this->main->query("SELECT id,institutionname,institutioncode,"
                                    . "institutioncontact,institutionlocation,institutionaddress,"
                                    . "createdby,createdon,modifiedby,modifiedon,status "
                                    . "FROM institutions "
                                    . "WHERE id is not null $where "
                                    . "ORDER BY institutionname ASC")->result();
        
    }
    
    function institutions_info_count($name,$status){
        
        if($name <> null){
            
           $where .=" AND institutionname LIKE '%$name%'"; 
        }
        
        if($status <> null){
            
            $where .=" AND status='$status'"; 
        }
        
        return count($this->main->query("SELECT id,institutionname,institutioncode,"
                                    . "institutioncontact,institutionlocation,institutionaddress,"
                                    . "createdby,createdon,modifiedby,modifiedon,status "
                                    . "FROM institutions "
                                    . "WHERE id is not null $where "
                                    . "ORDER BY institutionname ASC")->result());
    }
    
    function institutions_info($name,$status,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND institutionname LIKE '%$name%'"; 
        }
        
        if($status <> null){
            
            $where .=" AND status='$status'"; 
        }
        
        return $this->main->query("SELECT id,institutionname,institutioncode,"
                                . "institutioncontact,institutionlocation,institutionaddress,"
                                . "createdby,createdon,modifiedby,modifiedon,status "
                                . "FROM institutions "
                                . "WHERE id is not null $where "
                                . "ORDER BY institutionname ASC "
                                . "LIMIT $page,$limit")->result();
    } 
    
    function users_groups($userid,$group){
        
        
        if($userid <> null){
            
            $where .=" AND users_groups.user_id='$userid'"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id AND users_groups.group_id !=1 AND users_groups.group_id !=2 $where "
                . "ORDER BY users.first_name ASC")->result();
    }
    
    function system_user_registration($user,$user_group,$id,$password){
        
       if($password <> null){
           
            $salt=$this->config->item('store_salt', 'ion_auth') ? $this->salt() : FALSE;
       
            $password= $this->hash_password($password, $salt);
            
            $user['password']=$password;
            $user['salt']=$salt;
           
       }
            
      if($id == null){
          
          $user['createdon']=date('Y-m-d H:i:s');
          $user['createdby']=$this->session->userdata('user_id');
          
          if($this->main->insert('users',$user)){
             
             
              $user_group['user_id']=$this->main->insert_id();
              
              return $this->main->insert('users_groups',$user_group);
          }
          return FALSE;
      } 
              $user['modifiedon']=date('Y-m-d H:i:s');
              $user['modifiedby']=$this->session->userdata('user_id');
              
              $this->main->update('users',$user,array('id'=>$id));
       return $this->main->update('users_groups',$user_group,array('user_id'=>$id));
    }
    
    function change_password($password){
        
        $salt=$this->profile_data();
        $password=$this->hash_password($password,$salt->salt);
        
        return $this->main->update('users',array('password'=>$password),array('id'=>$this->session->userdata('user_id')));
    }
    
    protected function salt(){
            return substr(sha1(uniqid(rand(), true)), 0, $this->salt_length);
    }
    
    protected function hash_password($password, $salt=false){
        
           if ($this->config->item('store_salt', 'ion_auth') && $salt)
            {
               return  sha1($password . $salt);
            }
            else
            {
                
                    $salt = $this->salt();
                    return  $salt.substr(sha1($salt.$password), 0, -$this->config->item('salt_length', 'ion_auth'));
            }
    }
    
    protected function hash_db_password($password,$hash_password_db){
       
        if ($this->config->item('store_salt', 'ion_auth'))
		{
			return sha1($password .$hash_password_db->salt);
		}
		else
		{
			$salt = substr($hash_password_db->password,0,$this->config->item('salt_length', 'ion_auth'));
                return  $salt.substr(sha1($salt.$password), 0, -$this->config->item('salt_length', 'ion_auth'));
		}
    }
    
    function activate_deactivate_users($id,$status){
        $new_status=$status == 1?2:1;
        
        return $this->main->update('users',array('active'=>$new_status),array('id'=>$id));
    }
    
    function add_institution($data,$id){
        
        if($id == null){
          
          $data['createdon']=date('Y-m-d H:i:s');
          $data['createdby']=$this->session->userdata('user_id');
          
          if($this->main->insert('institutions',$data)){
             
              $code=str_pad($this->main->insert_id(),3,'0',STR_PAD_RIGHT);
              
              return $this->main->update('institutions',array('institutioncode'=>$code),array('id'=>$this->main->insert_id()));
          }
          
          return FALSE;
        }  
              
        $data['modifiedon']=date('Y-m-d H:i:s');
        $data['modifiedby']=$this->session->userdata('user_id');
              
        return $this->main->update('institutions',$data,array('id'=>$id));
    }
    
    function activate_deactivate_institution($id,$status){
        $new_status=$status == 1?2:1;
        
                if($new_status == 2){
                    $this->main->update('users',array('active'=>$new_status),array('institution'=>$id));
                }
        return $this->main->update('institutions',array('status'=>$new_status),array('institutioncode'=>$id));
    }    
}


?>
