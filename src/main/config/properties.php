<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['script_loopcount']=1;

$config['prEmailInstances']=3;

$config['app_debug']=11;

$config['db_query_limit']=500;

$config['user_group_id']=3;

$config['expiring_period']=20;

$config['activationLinkBackBone']='https://188.64.190.121/aviation/index.php/Home/activateAccount';

$config['loginLink']='https://188.64.190.121/aviation/index.php/Home/login';

$config['mailMessage']="Hello %s,\nYour account for Aviation Online Medication Examiniation has been created.\nPlease click on the link below to activate your account\n%s\nThank for your enrollment!";

$config['mailMessage2']="Hello %s,\nWelcome to Aviation Online Medication Examiniation.\nYour credentials to access the system are :-\nLink : %s\nUsername : %s\nPassword : %s\n\nEnjoy!";

$config['from_email_address']='emmanuel.mfikwa@gmobile.co.tz';

$config['failure_reply_email_address']='info@aviationmedical.co.tz';

$config['from_name']='Aviation Info';

$config['reply_to_email_address']='no-reply@aviationmedical.co.tz';

$config['reply_to_name']='No Reply';

/*
 * patient appoitnments status
 * pending=1
 * cancelled=3
 * confirmed=2
 * expired=4
 */
