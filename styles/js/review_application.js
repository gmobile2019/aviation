$(document).ready(function(){
    var base_url='http://188.64.190.121/aviation/index.php/';
        var ref=$('input[name=applicationRef]').val();
                          
        $("form#background").submit(function(e){
            
            $.ajax({
                type:'POST',
                url: base_url+'User/save_background',
                data:$(this).serialize(),
                success: function(data){
                      
                        if(data !== ""){
                            var ref=$('input[name=applicationRef]').val();
                            //load next data
                            populateNext('medicalHistory',ref);
                            $('div#background').slideUp('slow');
                            $('div#medicalHistory').slideDown('slow');
                            $('input[name=applicationRef]').val(data);
                            
                            
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#medicalHistory").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'User/save_medicalHistory',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            populateNext('employment',ref);
                            $('div#medicalHistory').slideUp('slow');
                            $('div#employment').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#employment").submit(function(e){
           
            $.ajax({
                type:'POST',
                url:base_url+'User/save_occupation',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            populateNext('personalHealth',ref);
                            $('div#employment').slideUp('slow');
                            $('div#personalHealth').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#personalHealth").submit(function(e){
           
            $.ajax({
                type:'POST',
                url:base_url+'User/save_personalHealth',
                data:$(this).serialize(),
                success: function(data){
                       
                        if(data !== ""){
                            populateNext('airCraft',ref);
                            $('div#personalHealth').slideUp('slow');
                            $('div#airCraft').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#airCraft").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'User/save_aircraftExperience',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            $('div#airCraft').slideUp('slow');
                            $('div#declaration').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#declaration").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'User/complete_application',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            $('div#feedbackMessage').find('p').find('span').text(data);
                            $('div#declaration').slideUp('slow');
                            $('div#feedbackMessage').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("a#airCraftLink").click(function(){
        
            populateNext("personalHealth",ref);
            $('div#personalHealth').slideDown('slow');
            $('div#airCraft').slideUp('slow');
            
        });
        
        $("a#personalHealthLink").click(function(){
        
            populateNext("employment",ref);
            $('div#employment').slideDown('slow');
            $('div#personalHealth').slideUp('slow');
            
        });
        
        $("a#employmentLink").click(function(){
        
            populateNext("medicalHistory",ref);
            $('div#medicalHistory').slideDown('slow');
            $('div#employment').slideUp('slow');
            
        });
        
        $("a#medicalHistoryLink").click(function(){
        
            populateNext("background",ref);
            $('div#background').slideDown('slow');
            $('div#medicalHistory').slideUp('slow');
            
        });
        
        $("a#declarationLink").click(function(){
        
            populateNext("airCraft",ref);
            $('div#airCraft').slideDown('slow');
            $('div#declaration').slideUp('slow');
            
        });
        
        
        //load data
        function populateNext(nextDiv,reference){
            
            if(nextDiv === 'background')
            {
            
                    $.ajax({
                        type:'POST',
                        url:base_url+'User/background_details',
                        data:{ref:reference},
                        success: function(data){

                                if(data !== ""){

                                    var obj = JSON.parse(data); 
                                    $('select[name=pilotExperience]').val(obj[0].pilotExperience);
                                    $('select[name=armedForce]').val(obj[0].armedForce);

                                }
                            }
                        });
            }else if(nextDiv === 'medicalHistory')
            {
                
                $.ajax({
                type:'POST',
                url:base_url+'User/medical_details',
                data:{ref:reference},
                success: function(data){
                       
                        if(data !== ""){
                            
                            var obj = JSON.parse(data); 
                            
                            $('select[name=fsHeadaches]').val(obj[0].fsHeadaches);
                            $('textarea[name=fsHeadachesRemark]').text(obj[0].fsHeadachesRemark);
                            $('select[name=dfu]').val(obj[0].dfu);
                            $('textarea[name=dfuRemark]').text(obj[0].dfuRemark);
                            $('select[name=eyeTrouble]').val(obj[0].eyeTrouble);
                            $('textarea[name=eyeTroubleRemark]').text(obj[0].eyeTroubleRemark);
                            $('select[name=hayFever]').val(obj[0].hayFever);
                            $('textarea[name=hayFeverRemark]').text(obj[0].hayFeverRemark);
                            $('select[name=asthma]').val(obj[0].asthma);
                            $('textarea[name=asthmaRemark]').text(obj[0].asthmaRemark);
                            $('select[name=heartTrouble]').val(obj[0].heartTrouble);
                            $('textarea[name=heartTroubleRemark]').text(obj[0].heartTroubleRemark);
                            $('select[name=bloodPressure]').val(obj[0].bloodPressure);
                            $('textarea[name=bloodPressureRemark]').text(obj[0].bloodPressureRemark);
                            $('select[name=stomachTrouble]').val(obj[0].stomachTrouble);
                            $('textarea[name=stomachTroubleRemark]').text(obj[0].stomachTroubleRemark);
                            $('select[name=ksbu]').val(obj[0].ksbu);
                            $('textarea[name=ksbuRemark]').text(obj[0].ksbuRemark);
                            $('select[name=su]').val(obj[0].su);
                            $('textarea[name=suRemark]').text(obj[0].suRemark);
                            $('select[name=epilepsy]').val(obj[0].epilepsy);
                            $('textarea[name=epilepsyRemark]').text(obj[0].epilepsyRemark);
                            $('select[name=nervousTrouble]').val(obj[0].nervousTrouble);
                            $('textarea[name=nervousTroubleRemark]').text(obj[0].nervousTroubleRemark);
                            $('select[name=motionSickness]').val(obj[0].motionSickness);
                            $('textarea[name=motionSicknessRemark]').text(obj[0].motionSicknessRemark);
                            $('select[name=lifeInsurance]').val(obj[0].lifeInsurance);
                            $('textarea[name=lifeInsuranceRemark]').text(obj[0].lifeInsuranceRemark);
                            $('select[name=hospitalAdmission]').val(obj[0].hospitalAdmission);
                            $('textarea[name=hospitalAdmissionRemark]').text(obj[0].hospitalAdmissionRemark);
                            $('select[name=otherIllness]').val(obj[0].otherIllness);
                            $('textarea[name=otherIllnessRemark]').text(obj[0].otherIllnessRemark);
                            $('select[name=headInjury]').val(obj[0].headInjury);
                            $('textarea[name=headInjuryRemark]').text(obj[0].headInjuryRemark);
                            $('select[name=MTDiseases]').val(obj[0].MTDiseases);
                            $('textarea[name=MTDiseasesRemark]').text(obj[0].MTDiseasesRemark);
                            $('select[name=addiction]').val(obj[0].addiction);
                            $('textarea[name=addictionRemark]').text(obj[0].addictionRemark);
                            $('select[name=familyHistory]').val(obj[0].familyHistory);
                            $('textarea[name=familyHistoryRemark]').text(obj[0].familyHistoryRemark);
                            $('select[name=flyingLicense]').val(obj[0].flyingLicense);
                            $('textarea[name=flyingLicenseRemark]').text(obj[0].flyingLicenseRemark);
                            $('select[name=discharge]').val(obj[0].discharge);
                            $('textarea[name=dischargeRemark]').text(obj[0].dischargeRemark); 
                            
                            var fsHeadaches;
                            fsHeadaches=$('select[name=fsHeadaches]').val();
                            if(fsHeadaches === 'Yes'){

                                $('div.fsHeadaches').show();
                            }
                            var dfu;
                            dfu=$('select[name=dfu]').val();
                            if(dfu === 'Yes'){

                                $('div.dfu').show();
                            }
                            var eyeTrouble;
                            eyeTrouble=$('select[name=eyeTrouble]').val();
                            if(eyeTrouble === 'Yes'){

                                $('div.eyeTrouble').show();
                            }
                            var hayFever;
                            hayFever=$('select[name=hayFever]').val();
                            if(hayFever === 'Yes'){

                                $('div.hayFever').show();
                            }
                            var asthma;
                            asthma=$('select[name=asthma]').val();
                            if(asthma === 'Yes'){

                                $('div.asthma').show();
                            }
                            var heartTrouble;
                            heartTrouble=$('select[name=heartTrouble]').val();
                            if(heartTrouble === 'Yes'){

                                $('div.heartTrouble').show();
                            }
                            var bloodPressure;
                            bloodPressure=$('select[name=bloodPressure]').val();
                            if(bloodPressure === 'Yes'){

                                $('div.bloodPressure').show();
                            }
                            var stomachTrouble;
                            stomachTrouble=$('select[name=stomachTrouble]').val();
                            if(stomachTrouble === 'Yes'){

                                $('div.stomachTrouble').show();
                            }
                            var ksbu;
                            ksbu=$('select[name=ksbu]').val();
                            if(ksbu === 'Yes'){

                                $('div.ksbu').show();
                            }
                            var su;
                            su=$('select[name=su]').val();
                            if(su === 'Yes'){

                                $('div.su').show();
                            }
                            var epilepsy;
                            epilepsy=$('select[name=epilepsy]').val();
                            if(epilepsy === 'Yes'){

                                $('div.epilepsy').show();
                            }
                            var nervousTrouble;
                            nervousTrouble=$('select[name=nervousTrouble]').val();
                            if(nervousTrouble === 'Yes'){

                                $('div.nervousTrouble').show();
                            }
                            var motionSickness;
                            motionSickness=$('select[name=motionSickness]').val();
                            if(motionSickness === 'Yes'){

                                $('div.motionSickness').show();
                            }
                            var lifeInsurance;
                            lifeInsurance=$('select[name=lifeInsurance]').val();
                            if(lifeInsurance === 'Yes'){

                                $('div.lifeInsurance').show();
                            }
                            var hospitalAdmission;
                            hospitalAdmission=$('select[name=hospitalAdmission]').val();
                            if(hospitalAdmission === 'Yes'){

                                $('div.hospitalAdmission').show();
                            }
                            var otherIllness;
                            otherIllness=$('select[name=otherIllness]').val();
                            if(otherIllness === 'Yes'){

                                $('div.otherIllness').show();
                            }
                            var headInjury;
                            headInjury=$('select[name=headInjury]').val();
                            if(headInjury === 'Yes'){

                                $('div.headInjury').show();
                            }
                            var MTDiseases;
                            MTDiseases=$('select[name=MTDiseases]').val();
                            if(MTDiseases === 'Yes'){

                                $('div.MTDiseases').show();
                            }
                            var addiction;
                            addiction=$('select[name=addiction]').val();
                            if(addiction === 'Yes'){

                                $('div.addiction').show();
                            }
                            var familyHistory;
                            familyHistory=$('select[name=familyHistory]').val();
                            if(familyHistory === 'Yes'){

                                $('div.familyHistory').show();
                            }
                            var flyingLicense;
                            flyingLicense=$('select[name=flyingLicense]').val();
                            if(flyingLicense === 'Yes'){

                                $('div.flyingLicense').show();
                            }
                            var discharge;
                            discharge=$('select[name=discharge]').val();
                            if(discharge === 'Yes'){

                                $('div.discharge').show();
                            }
                        }
                    }
                });
               
            }else if(nextDiv === 'employment')
            {
               
                $.ajax({
                type:'POST',
                url:base_url+'User/occupation_details',
                data:{ref:reference},
                success: function(data){
                   
                        if(data !== ""){
                            
                            var obj = JSON.parse(data); 
                            $('input[name=occupation]').val(obj[0].occupation);
                            $('input[name=employer]').val(obj[0].employer);
                        }
                    }
                });
               
            }else if(nextDiv === 'personalHealth')
            {
               
                $.ajax({
                    type:'POST',
                    url:base_url+'User/personalHealth_details',
                    data:{ref:reference},
                    success: function(data){

                            if(data !== ""){

                                var obj = JSON.parse(data); 
                                
                                $('select[name=gp]').val(obj[0].gp);
                                $('input[name=gpName]').val(obj[0].gpName);
                                $('input[name=gpAddress]').val(obj[0].gpAddress);
                                $('input[name=gpContact]').val(obj[0].gpContact);
                                $('select[name=medication]').val(obj[0].medication);
                                $('textarea[name=medDescription]').text(obj[0].medDescription);
                                $('textarea[name=medPurpose]').text(obj[0].medPurpose);
                                $('input[name=medDoctor]').val(obj[0].medDoctor);
                                $('input[name=noIllAccDis]').val(obj[0].noIllAccDis);

                                var gp;

                                //general practitioner
                                gp=$('select[name=gp]').val();

                                if(gp === 'Yes'){
                                    $('div.gp').show();
                                }

                                var med;
                                med=$('select[name=medication]').val();
                               if(med === 'Yes'){

                                   $('div.medication').show();
                               }

                                
                                
                                if(obj[0].noIllAccDis >= 1){
                                    
                                    var IllAccDisDate=obj[0].IllAccDisDate.split('&&');
                                    var IllAccDisDetails=obj[0].IllAccDisDetails.split('&&');
                                    var IllAccDisDoctorName=obj[0].IllAccDisDoctorName.split('&&');
                                    var IllAccDisDoctorAddress=obj[0].IllAccDisDoctorAddress.split('&&');
                                    
                                }
                                    var table='<table class="table table-bordered table-hover table-condensed table-dark"><tbody>';
                                    for(var i=1;i<=obj[0].noIllAccDis;i++){
                                        table +='<tr>';
                                        table +='<td>'+i+'</td>';
                                        table +='<td><input type="text" class="form-control" name="IllAccDisDate'+i+'" id="IllAccDisDate'+i+'"  value="'+IllAccDisDate[i-1]+'" required/></td>';
                                        table +='<td><textarea class="form-control" name="IllAccDisDetails'+i+'" id=""IllAccDisDetails'+i+'"  required>'+IllAccDisDetails[i-1]+'</textarea></td>';
                                        table +='<td><input type="text" class="form-control" name="IllAccDisDoctorName'+i+'" id=""IllAccDisDoctorName'+i+'"  value="'+IllAccDisDoctorName[i-1]+'" required/></td>';
                                        table +='<td><input type="text" class="form-control" name="IllAccDisDoctorAddress'+i+'" id=""IllAccDisDoctorAddress'+i+'"  value="'+IllAccDisDoctorAddress[i-1]+'" required/></td>';
                                        table +='</tr>';

                                    }

                                    table +='</tbody></table>';

                                    $('div#IllAccDis').empty();
                                    $('div#IllAccDis').html(table);
                                
                                
                            }
                        }
                });
               
            }else if(nextDiv === 'airCraft'){
                
                $.ajax({
                    type:'POST',
                    url:base_url+'User/aircraft_details',
                    data:{ref:reference},
                    success: function(data){

                            if(data !== ""){

                                var obj = JSON.parse(data); 
                               
                                $('select[name=licenceType]').val(obj[0].licenceType);
                                $('input[name=licenseNumber]').val(obj[0].licenseNumber);
                                $('input[name=expDteLMC]').val(obj[0].expDteLMC);
                                $('input[name=expDte5YrL]').val(obj[0].expDte5YrL);
                                $('input[name=hrsFlown]').val(obj[0].hrsFlown);
                                $('select[name=airAccident]').val(obj[0].airAccident);
                                
                                $('input[name=noAirType]').val(obj[0].noAircraftType);
                                $('input[name=noAirRoute]').val(obj[0].noAircraftRoute);

                                $('input[name=noAirAccident]').val(obj[0].noAircraftAccident);
                                
                                if(obj[0].airAccident === 'Yes'){
                                    
                                    $('div.airAccident').show();
                                }
                                
                                var noairAccident=obj[0].noAircraftAccident;
                                if(noairAccident > 0){
                                    
                                    
                                    var airAccidentDate=[obj[0].aircraftAccidentDate];
                                    var airAccidentLocation=[obj[0].airAccidentLocation];
                                    
                                    if(noairAccident > 1){
                                        
                                        airAccidentDate=obj[0].aircraftAccidentDate.split('&&');
                                        airAccidentLocation=obj[0].airAccidentLocation.split('&&');
                                    }
                                  
                                     var table='<table class="table table-bordered table-hover table-condensed table-dark">';
                                            table +='<tr><th style="text-align:center">Accident Date</th><th style="text-align:center">Accident Location</th></tr></thead>';
                                            table +='<tbody>';
                                    for(var i=1;i<=noairAccident;i++){
                                        
                                        table +='<tr>';
                                        table +='<td><input type="text" class="form-control" name="airAccidentDte'+i+'" id="airAccidentDte'+i+'"  value="'+airAccidentDate[i-1]+'" required/></td>';
                                        table +='<td><input type="text" class="form-control" name="airAccidentLocation'+i+'" id=""airAccidentLocation'+i+'"  value="'+airAccidentLocation[i-1]+'" required/></td>';
                                        table +='</tr>';
                                    }

                                    table +='</tbody></table>';

                                    $('div#noAirAccident').empty();
                                    $('div#noAirAccident').html(table);
                                }
                                
                                var rows;
                                
                                if(obj[0].noAircraftType > 0 || obj[0].noAircraftRoute > 0){
                                    if(obj[0].noAircraftType > obj[0].noAircraftRoute){
                                        rows=obj[0].noAircraftType;

                                    }else if(obj[0].noAircraftRoute > obj[0].noAircraftType){

                                        rows=obj[0].noAircraftRoute;
                                    }else{

                                        rows=obj[0].noAircraftType;
                                    }
                                    
                                    var airType=[obj[0].aircraftType];
                                    var airRoute=[obj[0].aircraftRoute];
                                    
                                    if(rows > 1){
                                        
                                        airType=obj[0].aircraftType.split('&&');
                                        airRoute=obj[0].aircraftRoute.split('&&');
                                    }
                                    
                                   var table='<table class="table table-bordered table-hover table-condensed table-dark">';
                                         table +='<tr><th style="text-align:center">Aircraft Route</th><th style="text-align:center">Aircraft Type</th></tr></thead>';
                                         table +='<tbody>';
                                    for(i=1;i<=rows;i++){
                                        table +='<tr>';
                                        table +='<td><input type="text" class="form-control" name="route'+i+'" id="route'+i+'"  value="'+airRoute[i-1]+'" required/></td>';
                                        table +='<td><input type="text" class="form-control" name="aircraftType'+i+'" id=""aircraftType'+i+'"  value="'+airType[i-1]+'" required/></td>';
                                        table +='</tr>';
                                    }

                                    table +='</tbody></table>';

                                    $('div#airCraftTypeRoute').empty();
                                    $('div#airCraftTypeRoute').html(table);
                                }
                            }
                        }
                    });
            }else{
            
            }
        }
    });


