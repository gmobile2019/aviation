$(document).ready(function(){

var flag=$('input#flag').val();



/*
 * 
 * @type @call;$@call;val|@call;$@call;val
 * application form jquery
 */

$('div#background').hide();
$('div#medicalHistory').hide();
$('div#employment').hide();
$('div#airCraft').hide();
$('div#personalHealth').hide();
$('div#declaration').hide();
$('div#feedbackMessage').hide();


var initialExamination=$('input[name=initialExamination]').val();
var reviewContinue=$('input[name=reviewContinue]').val();

if(reviewContinue === ''){
    
   if(!initialExamination){
        $('div#background').hide();
        $('div#medicalHistory').hide();
        $('div#employment').show();
        $('div#airCraft').hide();
        $('div#personalHealth').hide();
        $('div#declaration').hide();
        $('div#feedbackMessage').hide();
    }else{
        $('div#background').show();
        $('div#medicalHistory').hide();
        $('div#employment').hide();
        $('div#airCraft').hide();
        $('div#personalHealth').hide();
        $('div#declaration').hide();
        $('div#feedbackMessage').hide();
    } 
}else{
    $('div#'+reviewContinue).show();
}


$('div.airAccident').hide();
$('div.gp').hide();
$('div.medication').hide();
$('div.fsHeadaches').hide();
$('div.dfu').hide();
$('div.eyeTrouble').hide();
$('div.hayFever').hide();
$('div.asthma').hide();
$('div.heartTrouble').hide();
$('div.bloodPressure').hide();
$('div.stomachTrouble').hide();
$('div.ksbu').hide();
$('div.su').hide();
$('div.epilepsy').hide();
$('div.nervousTrouble').hide();
$('div.motionSickness').hide();
$('div.lifeInsurance').hide();
$('div.hospitalAdmission').hide();
$('div.otherIllness').hide();
$('div.headInjury').hide();
$('div.MTDiseases').hide();
$('div.addiction').hide();
$('div.familyHistory').hide();
$('div.flyingLicense').hide();
$('div.discharge').hide();

/***********Aircraft Experience Information****/
//types of aircraft and routes flwon 
var airType=$('input[name=noAirType]').val();
var airRoute=$('input[name=noAirRoute]').val();
var rows;

if(airType > 0 || airRoute > 0){
    if(airType > airRoute){
        rows=airType;

    }else if(airRoute > airType){

        rows=airRoute;
    }else{

        rows=airType;
    }
    
    var table='<table class="table table-bordered table-hover table-condensed table-dark">';
        table +='<tr><th style="text-align:center">Aircraft Route</th><th style="text-align:center">Aircraft Type</th></tr></thead>';
        table +='<tbody>';
    for(i=1;i<=rows;i++){
        table +='<tr>';
        table +='<td><input type="text" class="form-control" name="route'+i+'" id="route'+i+'"  placeholder="Route '+i+'" required/></td>';
        table +='<td><input type="text" class="form-control" name="aircraftType'+i+'" id=""aircraftType'+i+'"  placeholder="Aircraft Type '+i+'" required/></td>';
        table +='</tr>';
    }

    table +='</tbody></table>';

    $('div#airCraftTypeRoute').empty();
    $('div#airCraftTypeRoute').html(table);
}

$('input[name=noAirType]').change(function(){
    
    airType=$(this).val();
    airRoute=$('input[name=noAirRoute]').val();
    
    if(airType > 0 || airRoute > 0){
        if(airType > airRoute){
            rows=airType;

        }else if(airRoute > airType){

            rows=airRoute;
        }else{

            rows=airType;
        }
        
        var table='<table class="table table-bordered table-hover table-condensed table-dark">';
            table +='<tr><th style="text-align:center">Aircraft Route</th><th style="text-align:center">Aircraft Type</th></tr></thead>';
            table +='<tbody>';
        for(i=1;i<=rows;i++){
            table +='<tr>';
            table +='<td><input type="text" class="form-control" name="route'+i+'" id="route'+i+'"  placeholder="Route '+i+'" required/></td>';
            table +='<td><input type="text" class="form-control" name="aircraftType'+i+'" id=""aircraftType'+i+'"  placeholder="Aircraft Type '+i+'" required/></td>';
            table +='</tr>';
        }
        
        table +='</tbody></table>';
        
        $('div#airCraftTypeRoute').empty();
        $('div#airCraftTypeRoute').html(table);
    }
    
    
});


$('input[name=noAirRoute]').change(function(){
    
    airType=$('input[name=noAirType]').val();
    airRoute=$(this).val();
    
    if(airType > 0 || airRoute > 0){
        if(airType > airRoute){
            rows=airType;

        }else if(airRoute > airType){

            rows=airRoute;
        }else{

            rows=airType;
        }
        
        var table='<table class="table table-bordered table-hover table-condensed table-dark">';
            table +='<tr><th style="text-align:center">Aircraft Route</th><th style="text-align:center">Aircraft Type</th></tr></thead>';
            table +='<tbody>';
        for(i=1;i<=rows;i++){
            table +='<tr>';
            table +='<td><input type="text" class="form-control" name="route'+i+'" id="route'+i+'"  placeholder="Route '+i+'" required/></td>';
            table +='<td><input type="text" class="form-control" name="aircraftType'+i+'" id=""aircraftType'+i+'"  placeholder="Aircraft Type '+i+'" required/></td>';
            table +='</tr>';
        }
        
        table +='</tbody></table>';
        
        $('div#airCraftTypeRoute').empty();
        $('div#airCraftTypeRoute').html(table);
    }
    
    
});

//aircraft accident
var accident;
accident=$('select[name=airAccident]').val();
if(accident === 'Yes'){
    $('div.airAccident').show();
}


$('select[name=airAccident]').change(function(){
    accident=$(this).val();

    if(accident === 'Yes'){

        $('div.airAccident').slideDown('slow');
    }

    if(accident === 'No'){

        $('div.airAccident').slideUp('slow');
        $('input[name=noAirAccident]').val(0);
        $('div#noAirAccident').empty();
    }
});

//no of air accident
var noairAccident=$('input[name=noAirAccident]').val();

if(noairAccident > 0){
       
    var table='<table class="table table-bordered table-hover table-condensed table-dark">';
        table +='<tr><th style="text-align:center">Accident Date</th><th style="text-align:center">Accident Location</th></tr></thead>';
        table +='<tbody>';
    for(var i=1;i<=noairAccident;i++){
        table +='<tr>';
        table +='<td><input type="date" class="form-control" name="airAccidentDte'+i+'" id="airAccidentDte'+i+'"  placeholder="Accident Date '+i+'" required/></td>';
        table +='<td><input type="text" class="form-control" name="airAccidentLocation'+i+'" id=""airAccidentLocation'+i+'"  placeholder="Accident Location '+i+'" required/></td>';
        table +='</tr>';
    }

    table +='</tbody></table>';

    $('div#noAirAccident').empty();
    $('div#noAirAccident').html(table);
}

$('input[name=noAirAccident]').change(function(){
    
    noairAccident=$(this).val();
    
    if(noairAccident > 0){
       
         var table='<table class="table table-bordered table-hover table-condensed table-dark">';
            table +='<tr><th style="text-align:center">Accident Date</th><th style="text-align:center">Accident Location</th></tr></thead>';
            table +='<tbody>';
        for(var i=1;i<=noairAccident;i++){
            table +='<tr>';
            table +='<td><input type="date" class="form-control" name="airAccidentDte'+i+'" id="airAccidentDte'+i+'"  placeholder="Accident Date '+i+'" required/></td>';
            table +='<td><input type="text" class="form-control" name="airAccidentLocation'+i+'" id=""airAccidentLocation'+i+'"  placeholder="Accident Location '+i+'" required/></td>';
            table +='</tr>';
        }
        
        table +='</tbody></table>';
        
        $('div#noAirAccident').empty();
        $('div#noAirAccident').html(table);
    }else{
        $('div#noAirAccident').empty();
    }
    
    
});
/***health/welfare information**/
var gp;

//general practitioner
gp=$('select[name=gp]').val();

if(gp === 'Yes'){
    $('div.gp').show();
}

$('select[name=gp]').change(function(){
    gp=$(this).val();

    if(gp === 'Yes'){

        $('div.gp').slideDown('slow');
    }else{
        $('div.gp').slideUp('slow');
    }

});

//medication
 var med;
 med=$('select[name=medication]').val();
if(med === 'Yes'){

    $('div.medication').show();
}

$('select[name=medication]').change(function(){
    med=$(this).val();

    if(med === 'Yes'){

        $('div.medication').slideDown('slow');
    }else{
        
        $('div.medication').slideUp('slow');
    }

});
 
//any illness,accident of disability
var noIllAccDis=$('input[name=noIllAccDis]').val();

if(noIllAccDis > 0){
    rows=noIllAccDis;

    var table='<table class="table table-bordered table-hover table-condensed table-dark"><tbody>';
    for(i=1;i<=rows;i++){
        table +='<tr>';
        table +='<td>'+i+'</td>';
        table +='<td><input type="text" class="form-control" name="IllAccDisDate'+i+'" id="IllAccDisDate'+i+'"  placeholder="dd/mm/yyyy '+i+'" required/></td>';
        table +='<td><textarea class="form-control" name="IllAccDisDetails'+i+'" id=""IllAccDisDetails'+i+'"  placeholder="Details '+i+'" required></textarea></td>';
        table +='<td><input type="text" class="form-control" name="IllAccDisDoctorName'+i+'" id=""IllAccDisDoctorName'+i+'"  placeholder="Doctor Name '+i+'" required/></td>';
        table +='<td><input type="text" class="form-control" name="IllAccDisDoctorAddress'+i+'" id=""IllAccDisDoctorAddress'+i+'"  placeholder="Doctor Address '+i+'" required/></td>';
        table +='</tr>';
    }

    table +='</tbody></table>';

    $('div#IllAccDis').empty();
    $('div#IllAccDis').html(table);
}else{
    $('div#IllAccDis').empty();
}

$('input[name=noIllAccDis]').change(function(){
    
    noIllAccDis=$('input[name=noIllAccDis]').val();
    
    if(noIllAccDis > 0){
        rows=noIllAccDis;
        
        var table='<table class="table table-bordered table-hover table-condensed table-dark"><tbody>';
        for(i=1;i<=rows;i++){
            table +='<tr>';
            table +='<td>'+i+'</td>';
            table +='<td><input type="text" class="form-control" name="IllAccDisDate'+i+'" id="IllAccDisDate'+i+'"  placeholder="dd/mm/yyyy '+i+'" required/></td>';
            table +='<td><textarea class="form-control" name="IllAccDisDetails'+i+'" id=""IllAccDisDetails'+i+'"  placeholder="Details '+i+'" required></textarea></td>';
            table +='<td><input type="text" class="form-control" name="IllAccDisDoctorName'+i+'" id=""IllAccDisDoctorName'+i+'"  placeholder="Doctor Name '+i+'" required/></td>';
            table +='<td><input type="text" class="form-control" name="IllAccDisDoctorAddress'+i+'" id=""IllAccDisDoctorAddress'+i+'"  placeholder="Doctor Address '+i+'" required/></td>';
            table +='</tr>';
        }
        
        table +='</tbody></table>';
        
        $('div#IllAccDis').empty();
        $('div#IllAccDis').html(table);
    }else{
        $('div#IllAccDis').empty();
    }
    
    
});

/*****************************medical history*****************************************/
    var fsHeadaches;
    fsHeadaches=$('select[name=fsHeadaches]').val();
    if(fsHeadaches === 'Yes'){

        $('div.fsHeadaches').show();
    }

    $('select[name=fsHeadaches]').change(function(){
        fsHeadaches=$(this).val();

        if(fsHeadaches === 'Yes'){

            $('div.fsHeadaches').fadeIn('slow');
        }else{

            $('div.fsHeadaches').fadeOut('slow');
        }
    });
    
    var dfu;
    dfu=$('select[name=dfu]').val();
    if(dfu === 'Yes'){

        $('div.dfu').show();
    }

    
    $('select[name=dfu]').change(function(){
        dfu=$(this).val();

        if(dfu === 'Yes'){

            $('div.dfu').fadeIn('slow');
        }else{

            $('div.dfu').fadeOut('slow');
        }
    });
    
    var eyeTrouble;
    eyeTrouble=$('select[name=eyeTrouble]').val();
    if(eyeTrouble === 'Yes'){

        $('div.eyeTrouble').show();
    }
    
    $('select[name=eyeTrouble]').change(function(){
        eyeTrouble=$(this).val();

        if(eyeTrouble === 'Yes'){

            $('div.eyeTrouble').fadeIn('slow');
        }else{

            $('div.eyeTrouble').fadeOut('slow');
        }
    });
    
    var hayFever;
    hayFever=$('select[name=hayFever]').val();
    if(hayFever === 'Yes'){

        $('div.hayFever').show();
    }
    
    $('select[name=hayFever]').change(function(){
        hayFever=$(this).val();

        if(hayFever === 'Yes'){

            $('div.hayFever').fadeIn('slow');
        }else{

            $('div.hayFever').fadeOut('slow');
        }
    });
    
    var asthma;
    asthma=$('select[name=asthma]').val();
    if(asthma === 'Yes'){

        $('div.asthma').show();
    }
    
    $('select[name=asthma]').change(function(){
        asthma=$(this).val();

        if(asthma === 'Yes'){

            $('div.asthma').fadeIn('slow');
        }else{

            $('div.asthma').fadeOut('slow');
        }
    });
    
    var heartTrouble;
    heartTrouble=$('select[name=heartTrouble]').val();
    if(heartTrouble === 'Yes'){

        $('div.heartTrouble').show();
    }
    
    $('select[name=heartTrouble]').change(function(){
        heartTrouble=$(this).val();

        if(heartTrouble === 'Yes'){

            $('div.heartTrouble').fadeIn('slow');
        }else{

            $('div.heartTrouble').fadeOut('slow');
        }
    });
    
    var bloodPressure;
    bloodPressure=$('select[name=bloodPressure]').val();
    if(bloodPressure === 'Yes'){

        $('div.bloodPressure').show();
    }

    $('select[name=bloodPressure]').change(function(){
        bloodPressure=$(this).val();

        if(bloodPressure === 'Yes'){

            $('div.bloodPressure').fadeIn('slow');
        }else{

            $('div.bloodPressure').fadeOut('slow');
        }
    });
    
    var stomachTrouble;
    stomachTrouble=$('select[name=stomachTrouble]').val();
    if(stomachTrouble === 'Yes'){

        $('div.stomachTrouble').show();
    }
    
    $('select[name=stomachTrouble]').change(function(){
        stomachTrouble=$(this).val();

        if(stomachTrouble === 'Yes'){

            $('div.stomachTrouble').fadeIn('slow');
        }else{

            $('div.stomachTrouble').fadeOut('slow');
        }
    });
    
    var ksbu;
    ksbu=$('select[name=ksbu]').val();
    if(ksbu === 'Yes'){

        $('div.ksbu').show();
    }
    
    $('select[name=ksbu]').change(function(){
        ksbu=$(this).val();

        if(ksbu === 'Yes'){

            $('div.ksbu').fadeIn('slow');
        }else{

            $('div.ksbu').fadeOut('slow');
        }
    });
    
    var su;
    su=$('select[name=su]').val();
    if(su === 'Yes'){

        $('div.su').show();
    }
    
    $('select[name=su]').change(function(){
        su=$(this).val();

        if(su === 'Yes'){

            $('div.su').fadeIn('slow');
        }else{

            $('div.su').fadeOut('slow');
        }
    });
    
    var epilepsy;
    epilepsy=$('select[name=epilepsy]').val();
    if(epilepsy === 'Yes'){

        $('div.epilepsy').show();
    }
    
    $('select[name=epilepsy]').change(function(){
        epilepsy=$(this).val();

        if(epilepsy === 'Yes'){

            $('div.epilepsy').fadeIn('slow');
        }else{

            $('div.epilepsy').fadeOut('slow');
        }
    });
    
    var nervousTrouble;
    nervousTrouble=$('select[name=nervousTrouble]').val();
    if(nervousTrouble === 'Yes'){

        $('div.nervousTrouble').show();
    }
    
    $('select[name=nervousTrouble]').change(function(){
        nervousTrouble=$(this).val();

        if(nervousTrouble === 'Yes'){

            $('div.nervousTrouble').fadeIn('slow');
        }else{

            $('div.nervousTrouble').fadeOut('slow');
        }
    });
    
    var motionSickness;
    motionSickness=$('select[name=motionSickness]').val();
    if(motionSickness === 'Yes'){

        $('div.motionSickness').show();
    }
    
    $('select[name=motionSickness]').change(function(){
        motionSickness=$(this).val();

        if(motionSickness === 'Yes'){

            $('div.motionSickness').fadeIn('slow');
        }else{

            $('div.motionSickness').fadeOut('slow');
        }
    });
    
    var lifeInsurance;
    lifeInsurance=$('select[name=lifeInsurance]').val();
    if(lifeInsurance === 'Yes'){

        $('div.lifeInsurance').show();
    }
    
    $('select[name=lifeInsurance]').change(function(){
        lifeInsurance=$(this).val();

        if(lifeInsurance === 'Yes'){

            $('div.lifeInsurance').fadeIn('slow');
        }else{

            $('div.lifeInsurance').fadeOut('slow');
        }
    });
    
    var hospitalAdmission;
    hospitalAdmission=$('select[name=hospitalAdmission]').val();
    if(hospitalAdmission === 'Yes'){

        $('div.hospitalAdmission').show();
    }
    
    $('select[name=hospitalAdmission]').change(function(){
        hospitalAdmission=$(this).val();

        if(hospitalAdmission === 'Yes'){

            $('div.hospitalAdmission').fadeIn('slow');
        }else{

            $('div.hospitalAdmission').fadeOut('slow');
        }
    });
    
    var otherIllness;
    otherIllness=$('select[name=otherIllness]').val();
    if(otherIllness === 'Yes'){

        $('div.otherIllness').show();
    }
    
    $('select[name=otherIllness]').change(function(){
        otherIllness=$(this).val();

        if(otherIllness === 'Yes'){

            $('div.otherIllness').fadeIn('slow');
        }else{

            $('div.otherIllness').fadeOut('slow');
        }
    });
    
    var headInjury;
    headInjury=$('select[name=headInjury]').val();
    if(headInjury === 'Yes'){

        $('div.headInjury').show();
    }
    
    $('select[name=headInjury]').change(function(){
        headInjury=$(this).val();

        if(headInjury === 'Yes'){

            $('div.headInjury').fadeIn('slow');
        }else{

            $('div.headInjury').fadeOut('slow');
        }
    });
    
    var MTDiseases;
    MTDiseases=$('select[name=MTDiseases]').val();
    if(MTDiseases === 'Yes'){

        $('div.MTDiseases').show();
    }
    
    $('select[name=MTDiseases]').change(function(){
        MTDiseases=$(this).val();

        if(MTDiseases === 'Yes'){

            $('div.MTDiseases').fadeIn('slow');
        }else{

            $('div.MTDiseases').fadeOut('slow');
        }
    });
    
    var addiction;
    addiction=$('select[name=addiction]').val();
    if(addiction === 'Yes'){

        $('div.addiction').show();
    }
    
    $('select[name=addiction]').change(function(){
        addiction=$(this).val();

        if(addiction === 'Yes'){

            $('div.addiction').fadeIn('slow');
        }else{

            $('div.addiction').fadeOut('slow');
        }
    });
    
    var familyHistory;
    familyHistory=$('select[name=familyHistory]').val();
    if(familyHistory === 'Yes'){

        $('div.familyHistory').show();
    }
    
    $('select[name=familyHistory]').change(function(){
        familyHistory=$(this).val();

        if(familyHistory === 'Yes'){

            $('div.familyHistory').fadeIn('slow');
        }else{

            $('div.familyHistory').fadeOut('slow');
        }
    });
    
    var flyingLicense;
    flyingLicense=$('select[name=flyingLicense]').val();
     
    if(flyingLicense === 'Yes'){

        $('div.flyingLicense').show();
    }
    
    $('select[name=flyingLicense]').change(function(){
        flyingLicense=$(this).val();

        if(flyingLicense === 'Yes'){

            $('div.flyingLicense').fadeIn('slow');
        }else{
            $('div.flyingLicense').fadeOut('slow');
        }
    });
    
    var discharge;
    discharge=$('select[name=discharge]').val();
    
    if(discharge === 'Yes'){

        $('div.discharge').show();
    }

    
    $('select[name=discharge]').change(function(){
        discharge=$(this).val();

        if(discharge === 'Yes'){

            $('div.discharge').fadeIn('slow');
        }else{
            $('div.discharge').fadeOut('slow');
        }

    });
    
});