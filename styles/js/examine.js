$(document).ready(function(){
    
   
        var base_url='https://188.64.190.121/aviation/index.php/';
        var ref=$('input[name=applicationRef]').val();
                       
        $("form#physical").submit(function(e){
           
            $.ajax({
                type:'POST',
                url: base_url+'Doctor/save_appearanceMeasurement',
                data:$(this).serialize(),
                success: function(data){
                      
                        if(data !== ""){
                            
                            //load next data
                            populateNext('physicalDetailed',ref);
                            $('div#physical').slideUp('slow');
                            $('div#physicalDetailed').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#physicalDetailed").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'Doctor/save_physicalDetailed',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            populateNext('visual',ref);
                            $('div#physicalDetailed').slideUp('slow');
                            $('div#visual').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#visual").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'Doctor/save_visualDetails',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            populateNext('eye',ref);
                            $('div#visual').slideUp('slow');
                            $('div#eye').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#eye").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'Doctor/save_eyeDetails',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            populateNext('audio',ref);
                            $('div#eye').slideUp('slow');
                            $('div#audio').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#audio").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:base_url+'Doctor/save_audioDetails',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            populateNext('other',ref);
                            $('div#audio').slideUp('slow');
                            $('div#other').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#other").submit(function(e){
            var formData = new FormData(this);

            $.ajax({
                type:'POST',
                url:base_url+'Doctor/save_otherDetails',
                data:formData,
                success: function(data){
                     
                        if(data !== ""){
                             
                            populateNext('approval',ref);
                            $('div#other').slideUp('slow');
                            $('div#approval').slideDown('slow');
                        }
                 },
                cache: false,
                contentType: false,
                processData: false
                });
                 e.preventDefault();
        });
        
        
        $("a#physicalDetailedLink").click(function(){
        
            populateNext("physical",ref);
            $('div#physical').slideDown('slow');
            $('div#physicalDetailed').slideUp('slow');
            
        });
        
        $("a#visualLink").click(function(){
        
            populateNext("physicalDetailed",ref);
            $('div#physicalDetailed').slideDown('slow');
            $('div#visual').slideUp('slow');
            
        });
        
        $("a#eyeLink").click(function(){
        
            populateNext("visual",ref);
            $('div#visual').slideDown('slow');
            $('div#eye').slideUp('slow');
            
        });
        
        $("a#audioLink").click(function(){
        
            populateNext("eye",ref);
            $('div#eye').slideDown('slow');
            $('div#audio').slideUp('slow');
            
        });
        
        $("a#otherLink").click(function(){
        
            populateNext("audio",ref);
            $('div#audio').slideDown('slow');
            $('div#other').slideUp('slow');
            
        });
        
        $("a#approvalLink").click(function(){
        
            populateNext("other",ref);
            $('div#other').slideDown('slow');
            $('div#approval').slideUp('slow');
            
        });

////////////////////////////////////////////physical detailed////////////////////////////////////////////////////////////////////////

        $('div.headNeck').hide();
        $('div.mtt').hide();
        $('div.sinuses').hide();
        $('div.edv').hide();
        $('div.lungsChest').hide();
        $('div.pheart').hide();
        $('div.vSystem').hide();
        $('div.pRate').hide();
        $('div.bp').hide();
        $('div.abdomenHernia').hide();
        $('div.livSplGla').hide();
        $('div.anusRectum').hide();
        $('div.genito').hide();
        $('div.endocrine').hide();
        $('div.upperLowerJoints').hide();
        $('div.spine').hide();
        $('div.neurology').hide();
        $('div.skin').hide();
        $('div.psychiatric').hide();
        
        var headNeck;
        headNeck=$('select[name=headNeck]').val();
        if(headNeck === 'Abnormal'){

            $('div.headNeck').show();
        }

        $('select[name=headNeck]').change(function(){
            headNeck=$(this).val();

            if(headNeck === 'Abnormal'){

                $('div.headNeck').fadeIn('slow');
            }else{

                $('div.headNeck').fadeOut('slow');
            }
        });
        
        var mtt;
        mtt=$('select[name=mtt]').val();
        if(mtt === 'Abnormal'){

            $('div.mtt').show();
        }

        $('select[name=mtt]').change(function(){
            mtt=$(this).val();

            if(mtt === 'Abnormal'){

                $('div.mtt').fadeIn('slow');
            }else{

                $('div.mtt').fadeOut('slow');
            }
        });
        
        var sinuses;
        sinuses=$('select[name=sinuses]').val();
        if(sinuses === 'Abnormal'){

            $('div.sinuses').show();
        }

        $('select[name=sinuses]').change(function(){
            sinuses=$(this).val();

            if(sinuses === 'Abnormal'){

                $('div.sinuses').fadeIn('slow');
            }else{

                $('div.sinuses').fadeOut('slow');
            }
        });
        
        var edv;
        edv=$('select[name=edv]').val();
        if(edv === 'Abnormal'){

            $('div.edv').show();
        }

        $('select[name=edv]').change(function(){
            edv=$(this).val();

            if(edv === 'Abnormal'){

                $('div.edv').fadeIn('slow');
            }else{

                $('div.edv').fadeOut('slow');
            }
        });
        
        var lungsChest;
        lungsChest=$('select[name=lungsChest]').val();
        if(lungsChest === 'Abnormal'){

            $('div.lungsChest').show();
        }

        $('select[name=lungsChest]').change(function(){
            lungsChest=$(this).val();

            if(lungsChest === 'Abnormal'){

                $('div.lungsChest').fadeIn('slow');
            }else{

                $('div.lungsChest').fadeOut('slow');
            }
        });
        
        var pheart;
        pheart=$('select[name=pheart]').val();
        if(pheart === 'Abnormal'){

            $('div.pheart').show();
        }

        $('select[name=pheart]').change(function(){
            pheart=$(this).val();

            if(pheart === 'Abnormal'){

                $('div.pheart').fadeIn('slow');
            }else{

                $('div.pheart').fadeOut('slow');
            }
        });
        
        var vSystem;
        vSystem=$('select[name=vSystem]').val();
        if(vSystem === 'Abnormal'){

            $('div.vSystem').show();
        }

        $('select[name=vSystem]').change(function(){
            vSystem=$(this).val();

            if(vSystem === 'Abnormal'){

                $('div.vSystem').fadeIn('slow');
            }else{

                $('div.vSystem').fadeOut('slow');
            }
        });
        
        var pRate;
        pRate=$('select[name=pRate]').val();
        if(pRate === 'Abnormal'){

            $('div.pRate').show();
        }

        $('select[name=pRate]').change(function(){
            pRate=$(this).val();

            if(pRate === 'Abnormal'){

                $('div.pRate').fadeIn('slow');
            }else{

                $('div.pRate').fadeOut('slow');
            }
        });
        
        var bp;
        bp=$('select[name=bp]').val();
        if(bp === 'Abnormal'){

            $('div.bp').show();
        }

        $('select[name=bp]').change(function(){
            bp=$(this).val();

            if(bp === 'Abnormal'){

                $('div.bp').fadeIn('slow');
            }else{

                $('div.bp').fadeOut('slow');
            }
        });
        
        var abdomenHernia;
        abdomenHernia=$('select[name=abdomenHernia]').val();
        if(abdomenHernia === 'Abnormal'){

            $('div.abdomenHernia').show();
        }

        $('select[name=abdomenHernia]').change(function(){
            abdomenHernia=$(this).val();

            if(abdomenHernia === 'Abnormal'){

                $('div.abdomenHernia').fadeIn('slow');
            }else{

                $('div.abdomenHernia').fadeOut('slow');
            }
        });
        
        var livSplGla;
        livSplGla=$('select[name=livSplGla]').val();
        if(livSplGla === 'Abnormal'){

            $('div.livSplGla').show();
        }

        $('select[name=livSplGla]').change(function(){
            livSplGla=$(this).val();

            if(livSplGla === 'Abnormal'){

                $('div.livSplGla').fadeIn('slow');
            }else{

                $('div.livSplGla').fadeOut('slow');
            }
        });
        
        var anusRectum;
        anusRectum=$('select[name=anusRectum]').val();
        if(anusRectum === 'Abnormal'){

            $('div.anusRectum').show();
        }

        $('select[name=anusRectum]').change(function(){
            anusRectum=$(this).val();

            if(anusRectum === 'Abnormal'){

                $('div.anusRectum').fadeIn('slow');
            }else{

                $('div.anusRectum').fadeOut('slow');
            }
        });
        
        var genito;
        genito=$('select[name=genito]').val();
        if(genito === 'Abnormal'){

            $('div.genito').show();
        }

        $('select[name=genito]').change(function(){
            genito=$(this).val();

            if(genito === 'Abnormal'){

                $('div.genito').fadeIn('slow');
            }else{

                $('div.genito').fadeOut('slow');
            }
        });
        
        var endocrine;
        endocrine=$('select[name=endocrine]').val();
        if(endocrine === 'Abnormal'){

            $('div.endocrine').show();
        }

        $('select[name=endocrine]').change(function(){
            endocrine=$(this).val();

            if(endocrine === 'Abnormal'){

                $('div.endocrine').fadeIn('slow');
            }else{

                $('div.endocrine').fadeOut('slow');
            }
        });
        
        var upperLowerJoints;
        upperLowerJoints=$('select[name=upperLowerJoints]').val();
        if(upperLowerJoints === 'Abnormal'){

            $('div.upperLowerJoints').show();
        }

        $('select[name=upperLowerJoints]').change(function(){
            upperLowerJoints=$(this).val();

            if(upperLowerJoints === 'Abnormal'){

                $('div.upperLowerJoints').fadeIn('slow');
            }else{

                $('div.upperLowerJoints').fadeOut('slow');
            }
        });
        
        var spine;
        spine=$('select[name=spine]').val();
        if(spine === 'Abnormal'){

            $('div.spine').show();
        }

        $('select[name=spine]').change(function(){
            spine=$(this).val();

            if(spine === 'Abnormal'){

                $('div.spine').fadeIn('slow');
            }else{

                $('div.spine').fadeOut('slow');
            }
        });
        
        var neurology;
        neurology=$('select[name=neurology]').val();
        if(neurology === 'Abnormal'){

            $('div.neurology').show();
        }

        $('select[name=neurology]').change(function(){
            neurology=$(this).val();

            if(neurology === 'Abnormal'){

                $('div.neurology').fadeIn('slow');
            }else{

                $('div.neurology').fadeOut('slow');
            }
        });
        
        var skin;
        skin=$('select[name=skin]').val();
        if(skin === 'Abnormal'){

            $('div.skin').show();
        }

        $('select[name=skin]').change(function(){
            skin=$(this).val();

            if(skin === 'Abnormal'){

                $('div.skin').fadeIn('slow');
            }else{

                $('div.skin').fadeOut('slow');
            }
        });
        
        var psychiatric;
        psychiatric=$('select[name=psychiatric]').val();
        if(psychiatric === 'Abnormal'){

            $('div.psychiatric').show();
        }

        $('select[name=psychiatric]').change(function(){
            psychiatric=$(this).val();

            if(psychiatric === 'Abnormal'){

                $('div.psychiatric').fadeIn('slow');
            }else{

                $('div.psychiatric').fadeOut('slow');
            }
        });

    ////////////////////////////////////////////////visual////////////////////////////////////////////////////////////////////////////
    
    $('div.glassesP').hide();
    $('div.LPLMF').hide();
    $('div.fieldVision').hide();
   
    if($('select[name=LPLMF]').val() === 'Abnormal'){

        $('div.LPLMF').show();
    }

    $('select[name=LPLMF]').change(function(){
        
        if($(this).val() === 'Abnormal'){

            $('div.LPLMF').fadeIn('slow');
        }else{

            $('div.LPLMF').fadeOut('slow');
        }
    });
    
    if($('select[name=fieldVision]').val() === 'Abnormal'){

        $('div.fieldVision').show();
    }

    $('select[name=fieldVision]').change(function(){
        
        if($(this).val() === 'Abnormal'){

            $('div.fieldVision').fadeIn('slow');
        }else{

            $('div.fieldVision').fadeOut('slow');
        }
    });
    
    if($('select[name=glassesP]').val() === 'Yes'){

        $('div.glassesP').show();
    }

    $('select[name=glassesP]').change(function(){
        
        if($(this).val() === 'Yes'){

            $('div.glassesP').fadeIn('slow');
        }else{

            $('div.glassesP').fadeOut('slow');
        }
    });
    
//////////////////////////////////////////////////eye details////////////////////////////////////////////////////////////////////////////////////////////////
    
    $('div.cPerceptionIshihara').hide();
    $('div.cPerceptionLantem').hide();
    
    if($('select[name=cPerceptionIshihara]').val() === 'Abnormal'){

        $('div.cPerceptionIshihara').show();
    }

    $('select[name=cPerceptionIshihara]').change(function(){
        
        if($(this).val() === 'Abnormal'){

            $('div.cPerceptionIshihara').fadeIn('slow');
        }else{

            $('div.cPerceptionIshihara').fadeOut('slow');
        }
    });
    
    if($('select[name=cPerceptionLantem]').val() === 'Abnormal'){

        $('div.cPerceptionLantem').show();
    }

    $('select[name=cPerceptionLantem]').change(function(){
        
        if($(this).val() === 'Abnormal'){

            $('div.cPerceptionLantem').fadeIn('slow');
        }else{

            $('div.cPerceptionLantem').fadeOut('slow');
        }
    });
    
////////////////////////////////////////////////////////////////////audio details////////////////////////////////////////////////////////////////////////

    $('div.hearingDifficulty').hide();
    if($('select[name=hearingDifficulty]').val() === 'Yes'){

        $('div.hearingDifficulty').show();
    }

    $('select[name=hearingDifficulty]').change(function(){
        
        if($(this).val() === 'Yes'){

            $('div.hearingDifficulty').fadeIn('slow');
        }else{

            $('div.hearingDifficulty').fadeOut('slow');
        }
    });
        //load data
        function populateNext(nextDiv,reference){
            
            if(nextDiv === 'physical'){
               
                $.ajax({
                type:'POST',
                url:base_url+'User/appearanceMeasurement_details',
                data:{ref:reference},
                success: function(data){
                       
                        if(data !== ""){
                            
                            var obj = JSON.parse(data); 
                            
                            $('input[name=pHeight]').val(obj[0].pHeight);
                            $('input[name=pWeight]').val(obj[0].pWeight);
                            $('input[name=pChestInsp]').val(obj[0].pChestInsp);
                            $('input[name=pChestExp]').val(obj[0].pChestExp);
                            $('input[name=pWaist]').val(obj[0].pWaist);
                            $('textarea[name=pMSTD]').text(obj[0].pMSTD);
                            $('textarea[name=hairColor]').text(obj[0].hairColor);
                            $('textarea[name=eyesColor]').text(obj[0].eyesColor);
                            $('textarea[name=physicalImpression]').text(obj[0].physicalImpression);
                            
                        }
                    }
                });
               
            }else if(nextDiv === 'physicalDetailed')
            {
               
                $.ajax({
                type:'POST',
                url:base_url+'User/physical_details',
                data:{ref:reference},
                success: function(data){
                   
                        if(data !== ""){
                            
                            var obj = JSON.parse(data); 
                            $('select[name=headNeck]').val(obj[0].headNeck);
                            $('textarea[name=headNeckRemark]').val(obj[0].headNeckRemark);
                            $('select[name=mtt]').val(obj[0].mtt);
                            $('textarea[name=mttRemark]').val(obj[0].mttRemark);
                            $('select[name=sinuses]').val(obj[0].sinuses);
                            $('textarea[name=sinusesRemark]').val(obj[0].sinusesRemark);
                            $('select[name=edv]').val(obj[0].edv);
                            $('textarea[name=edvRemark]').val(obj[0].edvRemark);
                            $('select[name=lungsChest]').val(obj[0].lungsChest);
                            $('textarea[name=lungsChestRemark]').val(obj[0].lungsChestRemark);
                            $('select[name=pheart]').val(obj[0].pheart);
                            $('textarea[name=pheartRemark]').val(obj[0].pheartRemark);
                            $('select[name=vSystem]').val(obj[0].vSystem);
                            $('textarea[name=vSystemRemark]').val(obj[0].vSystemRemark);
                            $('select[name=pRate]').val(obj[0].pRate);
                            $('textarea[name=pRateRemark]').val(obj[0].pRateRemark);
                            $('select[name=bp]').val(obj[0].bp);
                            $('textarea[name=bpRemark]').val(obj[0].bpRemark);
                            $('select[name=abdomenHernia]').val(obj[0].abdomenHernia);
                            $('textarea[name=abdomenHerniaRemark]').val(obj[0].abdomenHerniaRemark);
                            $('select[name=livSplGla]').val(obj[0].livSplGla);
                            $('textarea[name=livSplGlaRemark]').val(obj[0].livSplGlaRemark);
                            $('select[name=anusRectum]').val(obj[0].anusRectum);
                            $('textarea[name=genitoRemark]').val(obj[0].genitoRemark);
                            $('select[name=genito]').val(obj[0].genito);
                            $('textarea[name=pheartRemark]').val(obj[0].pheartRemark);
                            $('select[name=endocrine]').val(obj[0].endocrine);
                            $('textarea[name=endocrineRemark]').val(obj[0].endocrineRemark);
                            $('select[name=upperLowerJoints]').val(obj[0].upperLowerJoints);
                            $('textarea[name=upperLowerJointsRemark]').val(obj[0].upperLowerJointsRemark);
                            $('select[name=spine]').val(obj[0].spine);
                            $('textarea[name=spineRemark]').val(obj[0].spineRemark);
                            $('select[name=neurology]').val(obj[0].neurology);
                            $('textarea[name=neurologyRemark]').val(obj[0].neurologyRemark);
                            $('select[name=skin]').val(obj[0].skin);
                            $('textarea[name=skinRemark]').val(obj[0].skinRemark);
                            $('select[name=psychiatric]').val(obj[0].psychiatric);
                            $('textarea[name=psychiatricRemark]').val(obj[0].psychiatricRemark);
                            
                            if(obj[0].headNeck === 'Abnormal'){
                                
                                $('div.headNeck').show();
                            }
                            
                            if(obj[0].mtt === 'Abnormal'){
                                
                                $('div.mtt').show();
                            }
                            
                            if(obj[0].sinuses === 'Abnormal'){
                                
                                $('div.sinuses').show();
                            }
                            
                            if(obj[0].edv === 'Abnormal'){
                                
                                $('div.edv').show();
                            }
                            
                            if(obj[0].lungsChest === 'Abnormal'){
                                
                                $('div.lungsChest').show();
                            }
                            
                            if(obj[0].pheart === 'Abnormal'){
                                
                                $('div.pheart').show();
                            }
                            
                            if(obj[0].vSystem === 'Abnormal'){
                                
                                $('div.vSystem').show();
                            }
                            
                            if(obj[0].pRate === 'Abnormal'){
                                
                                $('div.pRate').show();
                            }
                            
                            if(obj[0].bp === 'Abnormal'){
                                
                                $('div.bp').show();
                            }
                            
                            if(obj[0].abdomenHernia === 'Abnormal'){
                                
                                $('div.abdomenHernia').show();
                            }
                            
                            if(obj[0].livSplGla === 'Abnormal'){
                                
                                $('div.livSplGla').show();
                            }
                            
                            if(obj[0].anusRectum === 'Abnormal'){
                                
                                $('div.anusRectum').show();
                            }
                            
                            if(obj[0].genito === 'Abnormal'){
                                
                                $('div.genito').show();
                            }
                            
                            if(obj[0].endocrine === 'Abnormal'){
                                
                                $('div.endocrine').show();
                            }
                            
                            if(obj[0].upperLowerJoints === 'Abnormal'){
                                
                                $('div.upperLowerJoints').show();
                            }
                            
                            if(obj[0].spine === 'Abnormal'){
                                
                                $('div.spine').show();
                            }
                            
                            if(obj[0].neurology === 'Abnormal'){
                                
                                $('div.neurology').show();
                            }
                            
                            if(obj[0].skin === 'Abnormal'){
                                
                                $('div.skin').show();
                            }
                            
                            if(obj[0].psychiatric === 'Abnormal'){
                                
                                $('div.psychiatric').show();
                            }
                        }
                    }
                });
               
            }else if(nextDiv === 'visual')
            {
               
                $.ajax({
                    type:'POST',
                    url:base_url+'User/visual_details',
                    data:{ref:reference},
                    success: function(data){

                            if(data !== ""){

                                var obj = JSON.parse(data); 
                                $('select[name=LPLMF]').val(obj[0].LPLMF);
                                $('textarea[name=LPLMFRemark]').val(obj[0].LPLMFRemark);
                                $('textarea[name=dVisionWithGRight]').val(obj[0].dVisionWithGRight);
                                $('textarea[name=dVisionWithGLeft]').val(obj[0].dVisionWithGLeft);
                                $('textarea[name=dVisionWithoutGRight]').val(obj[0].dVisionWithoutGRight);
                                $('textarea[name=dVisionWithoutGLeft]').val(obj[0].dVisionWithoutGLeft);
                                $('textarea[name=nVisionWithGRight]').val(obj[0].nVisionWithGRight);
                                $('textarea[name=nVisionWithGLeft]').val(obj[0].nVisionWithGLeft);
                                $('textarea[name=nVisionWithoutGRight]').val(obj[0].nVisionWithoutGRight);
                                $('textarea[name=nVisionWithoutGLeft]').val(obj[0].nVisionWithoutGLeft);
                                $('textarea[name=accomodationWithGRight]').val(obj[0].accomodationWithGRight);
                                $('textarea[name=accomodationWithGLeft]').val(obj[0].accomodationWithGLeft);
                                $('textarea[name=accomodationWithoutGRight]').val(obj[0].accomodationWithoutGRight);
                                $('textarea[name=accomodationWithoutGLeft]').val(obj[0].accomodationWithoutGLeft);
                                $('select[name=glassesP]').val(obj[0].glassesP);
                                $('input[name=distantSright]').val(obj[0].distantSright);
                                $('input[name=distantCright]').val(obj[0].distantCright);
                                $('input[name=distantAright]').val(obj[0].distantAright);
                                $('input[name=distantSleft]').val(obj[0].distantSleft);
                                $('input[name=distantCleft]').val(obj[0].distantCleft);
                                $('input[name=distantAleft]').val(obj[0].distantAleft);
                                $('input[name=nearSright]').val(obj[0].nearSright);
                                $('input[name=nearCright]').val(obj[0].nearCright);
                                $('input[name=nearAright]').val(obj[0].nearAright);
                                $('input[name=nearSleft]').val(obj[0].nearSleft);
                                $('input[name=nearCleft]').val(obj[0].nearCleft);
                                $('input[name=nearAleft]').val(obj[0].nearAleft);
                                $('select[name=fieldVision]').val(obj[0].fieldVision);
                                $('textarea[name=fieldVisionRemark]').val(obj[0].fieldVisionRemark);
                                $('input[name=powerConvergence]').val(obj[0].powerConvergence);
                                
                                if($('select[name=LPLMF]').val() === 'Abnormal'){
                                    $('div.LPLMF').show();
                                }

                                if($('select[name=fieldVision]') === 'Abnormal'){

                                   $('div.fieldVision').show();
                                }
                               
                                if($('select[name=glassesP]') === 'Yes'){

                                   $('div.glassesP').show();
                                }

                            }
                        }
                });
               
            }else if(nextDiv === 'eye'){
                
                $.ajax({
                    type:'POST',
                    url:base_url+'User/eye_details',
                    data:{ref:reference},
                    success: function(data){

                            if(data !== ""){

                                var obj = JSON.parse(data); 
                               
                                $('textarea[name=manifestHyperRight]').val(obj[0].manifestHyperRight);
                                $('textarea[name=manifestHyperLeft]').val(obj[0].manifestHyperLeft);
                                $('select[name=cPerceptionIshihara]').val(obj[0].cPerceptionIshihara);
                                $('textarea[name=cPerceptionIshiharaRemark]').val(obj[0].cPerceptionIshiharaRemark);
                                $('select[name=cPerceptionLantem]').val(obj[0].cPerceptionLantem);
                                $('textarea[name=cPerceptionLantemRemark]').val(obj[0].cPerceptionLantemRemark);
                                $('input[name=maddoxRodExo]').val(obj[0].maddoxRodExo);
                                $('input[name=maddoxRodEso]').val(obj[0].maddoxRodEso);
                                $('input[name=maddoxRodHyper]').val(obj[0].maddoxRodHyper);
                                $('input[name=maddoxWingExo]').val(obj[0].maddoxWingExo);
                                $('input[name=maddoxWingEso]').val(obj[0].maddoxWingEso);
                                $('input[name=maddoxWingHyper]').val(obj[0].maddoxWingHyper);
                                
                                
                                if(obj[0].cPerceptionIshihara === 'Abnormal'){
                                    
                                    $('div.cPerceptionIshihara').show();
                                }
                                
                                if(obj[0].cPerceptionLantem === 'Abnormal'){
                                    
                                    $('div.cPerceptionLantem').show();
                                }
                            }
                        }
                    });
            }else if(nextDiv === 'audio'){
                
                $.ajax({
                    type:'POST',
                    url:base_url+'User/audio_details',
                    data:{ref:reference},
                    success: function(data){

                            if(data !== ""){

                                var obj = JSON.parse(data); 
                               
                                $('select[name=hearingDifficulty]').val(obj[0].hearingDifficulty);
                                $('textarea[name=hearingDifficultyRemark]').val(obj[0].hearingDifficultyRemark);
                                $('textarea[name=forcedWhisperRight]').val(obj[0].forcedWhisperRight);
                                $('textarea[name=forcedWhisperLeft]').val(obj[0].forcedWhisperLeft);
                                $('input[name=audiometry3000Right]').val(obj[0].audiometry3000Right);
                                $('input[name=audiometry3000Left]').val(obj[0].audiometry3000Left);
                                $('textarea[name=audiometry3000Remark]').val(obj[0].audiometry3000Remark);
                                $('input[name=audiometry2000Right]').val(obj[0].audiometry2000Right);
                                $('input[name=audiometry2000Left]').val(obj[0].audiometry2000Left);
                                $('textarea[name=audiometry2000Remark]').val(obj[0].audiometry2000Remark);
                                $('input[name=audiometry1000Right]').val(obj[0].audiometry1000Right);
                                $('input[name=audiometry1000Left]').val(obj[0].audiometry1000Left);
                                $('textarea[name=audiometry1000Remark]').val(obj[0].audiometry1000Remark);
                                $('input[name=audiometry500Right]').val(obj[0].audiometry500Right);
                                $('input[name=audiometry500Left]').val(obj[0].audiometry500Left);
                                $('textarea[name=audiometry500Remark]').val(obj[0].audiometry500Remark);
                               
                                
                                if(obj[0].hearingDifficulty === 'Yes'){
                                    
                                    $('div.hearingDifficulty').show();
                                }
                                
                            }
                        }
                    });
            }else if(nextDiv === 'other'){
                
                $.ajax({
                    type:'POST',
                    url:base_url+'User/other_details',
                    data:{ref:reference},
                    success: function(data){

                            if(data !== ""){
                               
                                var obj = JSON.parse(data); 
                               
                                $('input[name=menstruationDate]').val(obj[0].menstruationDate);
                                $('textarea[name=menstruationDateRemark]').val(obj[0].menstruationDateRemark);
                                $('textarea[name=ecgReport]').val(obj[0].ecgReport);
                                $('input[name=ecgNextDate]').val(obj[0].ecgNextDate);
                                $('textarea[name=cxrReport]').val(obj[0].cxrReport);
                                $('input[name=cxrNextDate]').val(obj[0].cxrNextDate);
                                $('textarea[name=audioReport]').val(obj[0].audioReport);
                                $('input[name=audioNextDate]').val(obj[0].audioNextDate);
                                $('textarea[name=urinalysisProtein]').val(obj[0].urinalysisProtein);
                                $('textarea[name=urinalysisGlucose]').val(obj[0].urinalysisGlucose);
                                $('textarea[name=urinalysisOther]').val(obj[0].urinalysisOther);
                                $('textarea[name=urinalysisRemarks]').val(obj[0].urinalysisRemarks);
                                $('textarea[name=mhRemarks]').val(obj[0].mhRemarks);
                                
                            }
                        }
                    });
            }
        }
    });


