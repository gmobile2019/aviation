<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
           $(document).ready(function(){
            
            var flag=$('input#flag').val();
            
            if(flag == 'signup'){
                
                $('form#myform1').hide();
                $('form#myform2').show();
            }else{
               $('form#myform1').show();
               $('form#myform2').hide(); 
            }
            
            $('a#accountCreationLink').click(function(){
                    $('form#myform1').slideUp('slow');
                    $('form#myform2').slideDown('slow');
            });
            
            $('a#signInLink').click(function(){
                    $('form#myform1').slideDown('slow');
                    $('form#myform2').slideUp('slow');
            });
            
            $("#dob").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
        });
            $('#banner').blink({delay:1000});
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-9" id="banner">
                    Aviation Medical Examination System
                </div>
                <div class="col-3">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link <?php echo $login; ?>" href="<?php echo base_url()."index.php/Home"?>">Login</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $about; ?>" href="<?php echo base_url()."index.php/Home/about"?>">About</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $feedback; ?>" href="<?php echo base_url()."index.php/Home/feedback"?>">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane1.jpg" alt="First slide"/>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane2.jpg" alt="Second slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <input type="hidden" id="flag" value="<?php echo $flag?>"/>
                        <?php echo $message; ?>
                    <form id="myform1" method="POST" action="<?php echo base_url() ?>index.php/Home/user_signin">
                        <div class="row">
                            <h5 id="publicheader" class="offset-6 col-6">Users Login</h5>
                        </div>
                        <div class="form-group row">
                            <label for="usrnme" class="offset-4 col-2 col-form-label">Username</label>
                            <div class="col-4">
                                <input class="form-control" type="text" name="usrnme" id="usrnme" placeholder="username/email" value="<?php echo set_value('usrnme'); ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pswd" class="offset-4 col-2 col-form-label">Password</label>
                            <div class="col-4">
                                <input class="form-control" type="password" name="pswd" id="pswd" placeholder="password" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-6 col-4">
                                <input class="btn btn-outline-success btn-block" type="submit" value="Login"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-6 col-5">
                                <a id="accountCreationLink" href="#">Click here if you don't have an account</a>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal" id="myform2" role="form" action="<?php echo base_url() ?>index.php/Home/user_signup" method="POST">
                    <div class="row">
                        <h5 id="publicheader" class="offset-3 col-9">Please Fill In The Form Below To Create An Account</h5>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="offset-4 col-3 form-control-label">Title</label>
                        <select name="title" class="col-4 form-control" required>
                            <option value="Mr" <?php echo set_select('title','Mr')?>>Mr</option>
                            <option value="Miss" <?php echo set_select('title','Miss')?>>Miss</option>
                            <option value="Mrs" <?php echo set_select('title','Mrs')?>>Mrs</option>
                        </select>
                        <?php echo form_error('title'); ?>
                    </div>
                    <div class="form-group row">
                       <label for="fname" class="offset-4 col-3 form-control-label">First name</label>
                       <input type="text" class="col-4 form-control" name="fname" id="fname" placeholder="First name" value="<?php echo set_value("fname"); ?>" required>
                       <?php echo form_error('fname'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="mname" class="offset-4 col-3 form-control-label">Middle name</label>
                      <input type="text" class="col-4 form-control" name="mname" id="mname" placeholder="Middle name" value="<?php echo set_value("mname"); ?>">
                      <?php echo form_error('mname'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="surname" class="offset-4 col-3 form-control-label">Surname</label>
                      <input type="text" class="col-4 form-control" name="surname" id="surname" placeholder="Surname" value="<?php echo set_value("surname"); ?>" required>
                      <?php echo form_error('surname'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="offset-4 col-3 form-control-label">Email Address</label>
                      <input type="email" class="col-4 form-control" name="email" id="email" placeholder="Email Address" value="<?php echo set_value("email"); ?>" required>
                      <?php echo form_error('email'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="mobile" class="offset-4 col-3 form-control-label">Mobile No</label>
                      <input type="text" class="col-4 form-control" name="mobile" id="mobile" placeholder="Mobile No" value="<?php echo set_value("mobile"); ?>" required>
                      <?php echo form_error('mobile'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="gender" class="offset-4 col-3 form-control-label">Gender</label>
                      <select name="gender" class="col-4 form-control">
                          <option value="Male" <?php echo set_select('gender','Male')?>>Male</option>
                          <option value="Female" <?php echo set_select('gender','Female')?>>Female</option>
                      </select>
                      <?php echo form_error('gender'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="dob" class="offset-4 col-3 form-control-label">Date Of Birth</label>
                      <input type="date" class="col-4 form-control" name="dob" id="dob" placeholder="YYYY-MM-DD" value="<?php echo set_value("dob"); ?>" required>
                      <?php echo form_error('dob'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="pob" class="offset-4 col-3 form-control-label">Place Of Birth</label>
                      <input type="text" class="col-4 form-control" name="pob" id="pob" placeholder="Place Of Birth" value="<?php echo set_value("pob"); ?>" required>
                      <?php echo form_error('pob'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="nationality" class="offset-4 col-3 form-control-label">Nationality</label>
                      <input type="text" class="col-4 form-control" name="nationality" id="nationality" placeholder="Nationality" value="<?php echo set_value("nationality"); ?>" required>
                      <?php echo form_error('nationality'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="paddress" class="offset-4 col-3 form-control-label">Permanent Address</label>
                      <input type="text" class="col-4 form-control" name="paddress" id="paddress" placeholder="Permanent Address" value="<?php echo set_value("paddress"); ?>" required>
                      <?php echo form_error('paddress'); ?>
                    </div>
                    <div class="form-group row">
                      <label for="postal" class="offset-4 col-3 form-control-label">Postal Address</label>
                      <input type="text" class="col-4 form-control" name="postal" id="postal" placeholder="Postal Address" value="<?php echo set_value("postal"); ?>" required>
                      <?php echo form_error('postal'); ?>
                    </div>
                    <div class="form-group row">
                            <div class="offset-3 col-4">
                                <a id="signInLink" href="#">Click here if you have an account</a>
                            </div>
                            <div class="col-4">
                                <input id="signUpButton" class="btn btn-outline-success btn-block" type="submit" value="Sign Up"/>
                            </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>