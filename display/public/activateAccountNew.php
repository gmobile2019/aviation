<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
            $('#banner').blink({delay:1000});
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-9" id="banner">
                    Aviation Medical Examination System
                </div>
                <div class="col-3">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link <?php echo $login; ?>" href="<?php echo base_url()."index.php/Home"?>">Login</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $about; ?>" href="<?php echo base_url()."index.php/Home/about"?>">About</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $feedback; ?>" href="<?php echo base_url()."index.php/Home/feedback"?>">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane1.jpg" alt="First slide"/>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane2.jpg" alt="Second slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <?php echo $message; ?>
                    <form class="form-horizontal" id="myform2" role="form" action="<?php echo base_url() ?>index.php/Home/activateAccount/register" method="POST">
                    <div class="row">
                        <h5 id="publicheader" class="offset-6 col-6">Account Activation</h5>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="offset-3 col-3 control-label">Password</label>
                        <div class="col-4">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required/>
                            <?php echo form_error('password'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="conf_password" class="offset-3 col-3 control-label">Confirm Password</label>
                        <div class="col-4">
                            <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Confirm Password" required/>
                            <?php echo form_error('conf_password'); ?>
                        </div>
                    </div>
                    <input type="hidden" name="sessionID" value="<?php echo $sessionID; ?>"/>
                    <div class="form-group row">
                        <div class="offset-6 col-4">
                            <input type="submit" class="btn btn-outline-success btn-block" value="Activate Account"/>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>