<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
            $('#banner').blink({delay:1000});
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-9" id="banner">
                    Aviation Medical Examination System
                </div>
                <div class="col-3">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link <?php echo $login; ?>" href="<?php echo base_url()."index.php/Home"?>">Login</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $about; ?>" href="<?php echo base_url()."index.php/Home/about"?>">About</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $feedback; ?>" href="<?php echo base_url()."index.php/Home/feedback"?>">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane1.jpg" alt="First slide"/>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane2.jpg" alt="Second slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7" id="divform">
                    <h4>Introduction</h4>
                    <p>Aviation medical examination system is a platform that allows the centralization of all 
                        the information collected from the pilots, cabin crew members and doctors during medical examination.
                    </p>
                    <p>For pilots and cabin crew it gives them the ability to be able to fill up some of the personal/basic 
                        information prior to visiting the facility/examination center so as to be examined 
                        while for doctors it gives them access to online information about the patient to be examined.
                    </p>
                    <p>Since the information is centrally managed this gives the pilots/cabin crew members 
                        the freedom to visit any facility that uses the system for having their medical examinations 
                        in the quest of getting the certificate to be cleared for flying.
                    </p>
                    <h4>How To</h4>
                    <p>Doctors and patients (pilots and cabin crew members) are allowed to login to the system using usernames and email addresses respectively.</p>
                    <p>For a doctor to be able to access the system his/her account needs to be created by an Administrator. 
                        to create a doctor’s account he/she  will be needed to provide some basic information which will be requested by an Administrator prior to account creation. 
                        Once an account is created login details will be sent to the doctor’s email address and thus access to the system will be granted immediately.
                    </p>
                    <p>For a pilot or cabin crew member to be able to use the services he/she will be required to sign up first and after signing up an activation link will be sent to his/her email address. 
                        Once account has been activated he/she will be required to login to the system using the email and password provided during sign up and activation respectively.
                    </p>
                    <h4>Conclusion</h4>
                    <p>In cases of any comments and/or suggestions please don’t hesitate to provide us with the <?php echo anchor('Home/feedback','<span style="font-weight:bold;font-style:italic">feedback</span>'); ?> so as to achieve the comfort for your usage.
                        Enjoy!!
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>