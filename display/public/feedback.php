<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
           $(document).ready(function(){
            
           
            });
            $('#banner').blink({delay:1000});
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-9" id="banner">
                    Aviation Medical Examination System
                </div>
                <div class="col-3">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link <?php echo $login; ?>" href="<?php echo base_url()."index.php/Home"?>">Login</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $about; ?>" href="<?php echo base_url()."index.php/Home/about"?>">About</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $feedback; ?>" href="<?php echo base_url()."index.php/Home/feedback"?>">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane1.jpg" alt="First slide"/>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane2.jpg" alt="Second slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <?php echo $message; ?>
                    <form id="myform1" method="POST" action="<?php echo base_url() ?>index.php/Home/feedback">
                        <div class="row">
                            <h5 id="publicheader" class="offset-6 col-6">Feedback</h5>
                        </div>
                        <div class="form-group row">
                            <label for="fdbksubject" class="offset-4 col-3 col-form-label">Subject</label>
                            <div class="col-4">
                                <input class="form-control" type="text" name="fdbksubject" id="fdbksubject" placeholder="Subject" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fdbkemail" class="offset-4 col-3 col-form-label">Email Address</label>
                            <div class="col-4">
                                <input class="form-control" type="email" name="fdbkemail" id="fdbkemail" placeholder="Email Address" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fdbkemail" class="offset-4 col-3 col-form-label">Mobile No</label>
                            <div class="col-4">
                                <input class="form-control" type="text" name="fdbkmobile" id="fdbkemail" placeholder="Mobile No" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fdbkemail" class="offset-4 col-3 col-form-label">Suggestions/Comments</label>
                            <div class="col-4">
                                <textarea class="form-control" name="fdbkcomm" id="fdbkcomm" placeholder="Comments/Suggestions" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-7 col-4">
                                <input class="btn btn-outline-success btn-block" type="submit" value="Send Feedback"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>