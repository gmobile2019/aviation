<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
            $('#banner').blink({delay:1000});
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-9" id="banner">
                    Aviation Medical Examination System
                </div>
                <div class="col-3">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link <?php echo $login; ?>" href="<?php echo base_url()."index.php/Home"?>">Login</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $about; ?>" href="<?php echo base_url()."index.php/Home/about"?>">About</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link <?php echo $feedback; ?>" href="<?php echo base_url()."index.php/Home/feedback"?>">Feedback</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane1.jpg" alt="First slide"/>
                            </div>
                            <div class="carousel-item">
                              <img class="d-block img-fluid" id="imgslider" src="<?php echo base_url();?>img/homesliders/plane2.jpg" alt="Second slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row">
                        <div class="offset-2 col-10">
                            <?php echo $message; ?>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>