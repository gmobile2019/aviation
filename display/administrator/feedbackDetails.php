<div id="divform">
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed">
                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;Subject</td>
                        <td>&nbsp;&nbsp;<?php echo $data[0]->feedbackSubject; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Email Address</td>
                        <td>&nbsp;&nbsp;<?php echo $data[0]->feedbackemail; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Mobile No</td>
                        <td>&nbsp;&nbsp;<?php echo $data[0]->feedbackmobile; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Comments</td>
                        <td>&nbsp;&nbsp;<?php echo $data[0]->feedbackcomments; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Posted On</td>
                        <td>&nbsp;&nbsp;<?php echo $data[0]->createdon; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Read On</td>
                        <td>&nbsp;&nbsp;<?php echo $data[0]->readon; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>