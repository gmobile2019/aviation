<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
  
  $(document).ready(function(){
      
      $('span#full_bs').hide();
      $('span#full_bg').hide();
      $('span#full_mh').hide();
      $('span#full_oc').hide();
      $('span#full_ph').hide();
      $('span#full_ac').hide();
      $('span#full_apm').hide();
      $('span#full_py').hide();
      $('span#full_vs').hide();
      $('span#full_ey').hide();
      $('span#full_au').hide();
      $('span#full_ot').hide();
      
      $('span#full_bs').click(function(){
            
            $('table#table_bs').slideDown();
            $('span#full_bs').hide();
            $('span#small_bs').show();
      });
      
      $('span#small_bs').click(function(){
            
            $('table#table_bs').slideUp();
            $('span#small_bs').hide();
            $('span#full_bs').show();
      });
      
      $('span#full_bg').click(function(){
            
            $('table#table_bg').slideDown();
            $('span#full_bg').hide();
            $('span#small_bg').show();
      });
      
      $('span#small_bg').click(function(){
            
            $('table#table_bg').slideUp();
            $('span#small_bg').hide();
            $('span#full_bg').show();
      });
      
      $('span#full_mh').click(function(){
            
            $('table#table_mh').slideDown();
            $('span#full_mh').hide();
            $('span#small_mh').show();
      });
      
      $('span#small_mh').click(function(){
            
            $('table#table_mh').slideUp();
            $('span#small_mh').hide();
            $('span#full_mh').show();
      });
      
      $('span#full_oc').click(function(){
            
            $('table#table_oc').slideDown();
            $('span#full_oc').hide();
            $('span#small_oc').show();
      });
      
      $('span#small_oc').click(function(){
            
            $('table#table_oc').slideUp();
            $('span#small_oc').hide();
            $('span#full_oc').show();
      });
      
      $('span#full_ph').click(function(){
            
            $('table#table_ph').slideDown();
            $('span#full_ph').hide();
            $('span#small_ph').show();
      });
      
      $('span#small_ph').click(function(){
            
            $('table#table_ph').slideUp();
            $('span#small_ph').hide();
            $('span#full_ph').show();
      });
      
      $('span#full_ac').click(function(){
            
            $('table#table_ac').slideDown();
            $('span#full_ac').hide();
            $('span#small_ac').show();
      });
      
      $('span#small_ac').click(function(){
            
            $('table#table_ac').slideUp();
            $('span#small_ac').hide();
            $('span#full_ac').show();
      });
      
       $('span#full_apm').click(function(){
            
            $('table#table_apm').slideDown();
            $('span#full_apm').hide();
            $('span#small_apm').show();
      });
      
      $('span#small_apm').click(function(){
            
            $('table#table_apm').slideUp();
            $('span#small_apm').hide();
            $('span#full_apm').show();
      });
      
      $('span#full_py').click(function(){
            
            $('table#table_py').slideDown();
            $('span#full_py').hide();
            $('span#small_py').show();
      });
      
      $('span#small_py').click(function(){
            
            $('table#table_py').slideUp();
            $('span#small_py').hide();
            $('span#full_py').show();
      });
      
      $('span#full_vs').click(function(){
            
            $('table#table_vs').slideDown();
            $('span#full_vs').hide();
            $('span#small_vs').show();
      });
      
      $('span#small_vs').click(function(){
            
            $('table#table_vs').slideUp();
            $('span#small_vs').hide();
            $('span#full_vs').show();
      });
      
      $('span#full_ey').click(function(){
            
            $('table#table_ey').slideDown();
            $('span#full_ey').hide();
            $('span#small_ey').show();
      });
      
      $('span#small_ey').click(function(){
            
            $('table#table_ey').slideUp();
            $('span#small_ey').hide();
            $('span#full_ey').show();
      });
      $('span#full_au').click(function(){
            
            $('table#table_au').slideDown();
            $('span#full_au').hide();
            $('span#small_au').show();
      });
      
      $('span#small_au').click(function(){
            
            $('table#table_au').slideUp();
            $('span#small_au').hide();
            $('span#full_au').show();
      });
      $('span#full_ot').click(function(){
            
            $('table#table_ot').slideDown();
            $('span#full_ot').hide();
            $('span#small_ot').show();
      });
      
      $('span#small_ot').click(function(){
            
            $('table#table_ot').slideUp();
            $('span#small_ot').hide();
            $('span#full_ot').show();
      });
      
      $('a').find('span#pdf').css({
                                color: '#000000'
                            });
  });
</script>
<div class="row">
    <div class="offset-6">
        <?php echo anchor("Administrator/applicationDetails/$appRef/export",'<span id="pdf" class="fa fa-download fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
    </div>
</div>
<div id="divform">
<div id="div_bs">
    <div class="row">
        <div class="col-7">
             <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Basic Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_bs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_bs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed table-primary" id="table_bs">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Title</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->title; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;First Name</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->fname; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Middle Name</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->mname; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Surname</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->surname; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Email</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->email; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Mobile</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->mobile; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Gender</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->gender; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Age</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $this->User_model->dateDifference(date('Y-m-d'),$bs_details[0]->dob)->y; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Place of Birth</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->pob; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Nationality</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->nationality; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Permanent Address</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->paddress; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Postal Address</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->postal; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<div id="div_bg">
    <div class="row">
        <div class="col-7">
             <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Background Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_bg" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_bg" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
    </div>
    <table class="table table-bordered table-hover table-condensed table-primary" id="table_bg">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever served in Armed Forces?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bg_details[0]->armedForce; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever had previous Pilot or Aircrew experience?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bg_details[0]->pilotExperience; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="div_mh">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Medical History Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_mh" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_mh" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_mh">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of frequent or severe headaches?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->fsHeadaches; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->fsHeadaches <> 'No'?$mh_details[0]->fsHeadachesRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Dizziness,Fainting or Unconciousness?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->dfu; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->dfu <> 'No'?$mh_details[0]->dfuRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Eye trouble?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->eyeTrouble; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->eyeTrouble <> 'No'?$mh_details[0]->eyeTroubleRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Hay Fever?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hayFever; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hayFever <> 'No'?$mh_details[0]->hayFeverRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Asthma?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->asthma; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->asthma <> 'No'?$mh_details[0]->asthmaRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Heart trouble?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->heartTrouble; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->heartTrouble <> 'No'?$mh_details[0]->heartTroubleRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of High or Low Blood Pressure?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->bloodPressure; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->bloodPressure <> 'No'?$mh_details[0]->bloodPressureRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Stomach trouble?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->stomachTrouble; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->stomachTrouble <> 'No'?$mh_details[0]->stomachTroubleRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Kidney Stones or Blood in Urine?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->ksbu; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->ksbu <> 'No'?$mh_details[0]->ksbuRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Sugar in Urine?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->su; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->su <> 'No'?$mh_details[0]->suRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Epilepsy?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->epilepsy; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->epilepsy <> 'No'?$mh_details[0]->epilepsyRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Nervous trouble of any sort?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->nervousTrouble; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->nervousTrouble <> 'No'?$mh_details[0]->nervousTroubleRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Motion Sickness requiring Drugs?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->motionSickness; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->motionSickness <> 'No'?$mh_details[0]->motionSicknessRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Have you ever been refused Life Insurance?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->lifeInsurance; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->lifeInsurance <> 'No'?$mh_details[0]->lifeInsuranceRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Have you ever been admitted to Hospital?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hospitalAdmission; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hospitalAdmission <> 'No'?$mh_details[0]->hospitalAdmissionRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of any other Illness?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->otherIllness; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->otherIllness <> 'No'?$mh_details[0]->otherIllnessRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Head Injury?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->headInjury; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->headInjury <> 'No'?$mh_details[0]->headInjuryRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Malaria/Tropical Diseases?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->MTDiseases; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->MTDiseases <> 'No'?$mh_details[0]->MTDiseasesRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Have you ever been treated for Alcoholism/Drug addiction?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->addiction; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->addiction <> 'No'?$mh_details[0]->addictionRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Any member of the family with history of Diabetes,Epilepsy or Tuberculosis?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->familyHistory; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->familyHistory <> 'No'?$mh_details[0]->familyHistoryRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Have you ever been refused a flying license?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->flyingLicense; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->flyingLicense <> 'No'?$mh_details[0]->flyingLicenseRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have a history of being discharged form Armed Forces on Medical Grounds?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->discharge; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->discharge <> 'No'?$mh_details[0]->dischargeRemark:'N/A'; ?></td>
                    </tr>
                    <tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<div id="div_oc">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Occupation/Employment Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_oc" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_oc" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_oc">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Occupation</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $oc_details[0]->occupation; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Employer</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $oc_details[0]->employer; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>    
</div>
<div id="div_ph">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Health/Welfare Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_ph" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_ph" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_ph">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have an own General Practitioner?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gp; ?></td>
                    </tr>

                    <?php if($ph_details[0]->gp === 'Yes'){ ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Name of General Practitioner</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gpName; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Address of General Practitioner</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gpAddress; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Contact of General Practitioner</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gpContact; ?></td>
                    </tr>

                    <?php } ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Do you have any medication presently being prescribed?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medication; ?></td>
                    </tr>

                    <?php if($ph_details[0]->medication === 'Yes'){ ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Description of prescribed medication</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medDescription; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Purpose of prescribed medication</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medPurpose; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Medication prescribing doctor</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medDoctor; ?></td>
                    </tr>

                    <?php } ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;How many illness,accidents or disabilities suffered since the last Medical Examination</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->noIllAccDis; ?></td>
                    </tr>

                    <?php 
                        if($ph_details[0]->noIllAccDis >= 1){

                            $illAccDisDate=explode('&&',$ph_details[0]->IllAccDisDate);
                            $illAccDisDetails=explode('&&',$ph_details[0]->IllAccDisDetails);
                            $illAccDisDoctorName=explode('&&',$ph_details[0]->IllAccDisDoctorName);
                            $illAccDisDoctorAddress=explode('&&',$ph_details[0]->IllAccDisDoctorAddress);
                        ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Illness,accidents or disabilities</td>
                        <td id="tdData">
                            <table class="table table-condensed table-responsive">
                                <tr>
                                    <th id="tdData">Date</th>
                                    <th id="tdData">Details</th>
                                    <th id="tdData">Doctor Name</th>
                                    <th id="tdData"> Doctor Address</th>
                                </tr>
                                <?php for($i=0;$i<$ph_details[0]->noIllAccDis;$i++){ ?>
                                <tr>
                                    <td id="tdData"><?php echo $illAccDisDate[$i]; ?></td>
                                    <td id="tdData"><?php echo $illAccDisDetails[$i]; ?></td>
                                    <td id="tdData"><?php echo $illAccDisDoctorName[$i]; ?></td>
                                    <td id="tdData"><?php echo $illAccDisDoctorAddress[$i]; ?></td>
                                </tr>
                                <?php }?>
                            </table>
                        </td>
                    </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<div id="div_ac">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Aircraft Experience Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_ac" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_ac" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_ac">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Type of License held or applied for</td>
                        <td id="tdData">&nbsp;&nbsp;<?php $license=$this->User_model->getlicenceTypes($ac_details[0]->licenceType,NULL,NULL);echo $license[0]->licenceType; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;License Number</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->licenseNumber; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Expiry Date(s) of Last Medical Certificate(s)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->expDteLMC; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Expiry Date(s) of 5 Year License(s)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->expDte5YrL; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Total hours flown since last Medical Examination</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->hrsFlown; ?></td>
                    </tr>

                    <?php 
                    if($ac_details[0]->noAircraftType > 0 || $ac_details[0]->noAircraftRoute > 0){

                        $aircraftRoute=explode('&&',$ac_details[0]->aircraftRoute);
                        $aircraftType=explode('&&',$ac_details[0]->aircraftType);
                    ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Aircraft type(s) and route(s) flown since last medical examination</td>
                        <td id="tdData">
                            <table class="table table-condensed table-responsive">
                                <tr>
                                    <th id="tdData">Type</th>
                                    <th id="tdData">Route</th>
                                </tr>
                                <?php for($i=0;$i<count($aircraftType);$i++){ ?>
                                <tr>
                                    <td id="tdData"><?php echo $aircraftType[$i]; ?></td>
                                    <td id="tdData"><?php echo $aircraftRoute[$i]; ?></td>
                                </tr>
                                <?php }?>
                            </table>
                        </td>
                    </tr>

                    <?php } ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Have you been involved in an Aircraft Accident since Last Medical Examination?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->airAccident; ?></td>
                    </tr>

                    <?php if($ac_details[0]->airAccident === 'Yes'){ 

                            $airAccLocation=explode('&&',$ac_details[0]->airAccidentLocation);
                            $airAccDate=explode('&&',$ac_details[0]->aircraftAccidentDate);
                        ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Aircraft accident date(s) and location(s) details</td>
                        <td id="tdData">
                            <table class="table table-condensed table-responsive">
                                <tr>
                                    <th id="tdData">Date</th>
                                    <th id="tdData">Location</th>
                                </tr>
                                <?php for($i=0;$i<$ac_details[0]->noAircraftAccident;$i++){ ?>
                                <tr id="tdData">
                                    <td id="tdData"><?php echo $airAccDate[$i]; ?></td>
                                    <td id="tdData"><?php echo $airAccLocation[$i]; ?></td>
                                </tr>
                                <?php }?>
                            </table>
                        </td>
                    </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
</div>

<?php if($apm_details <> NULL){ ?>
<div id="div_apm">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Appearance & Measurement Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_apm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_apm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_apm">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Height</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->pHeight; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Weight</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->pWeight; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Chest Inspiration</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->pChestInsp; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Chest Expiration</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->pChestExp; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Waist</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->pWaist; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Identify Marks,Scars,Tattoos,Deformities</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->pMSTD; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Color of hair</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->hairColor; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Color of eyes</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->eyesColor; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Physical Impression</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $apm_details[0]->physicalImpression; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php } 
if($py_details <> NULL){ ?>
<div id="div_py">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Physical Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_py" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_py" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_py">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Head and Neck</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->headNeck; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->headNeckRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Mouth , Throat and Teeth</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->mtt; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->mttRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Sinuses</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->sinuses; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->sinusesRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Ears,Drums and Valsalva</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->edv; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->edvRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Lungs,Chest including breasts</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->lungsChest; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->lungsChestRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Heart Size,Auscultation</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->pheart; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->pheartRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Vascular System; Varicose Veins</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->vSystem; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->vSystemRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Pulse Rate (Sitting,Standing)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->pRate; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->pRateRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Blood Pressure - Systolic/Diastolic Pulse Rate</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->bp; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->bpRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Abdomen and Hernia</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->abdomenHernia; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->abdomenHerniaRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Liver,Spleen and Glands</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->livSplGla; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->livSplGlaRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Anus and Rectum (Haemorrhoids, Fistula, Prostate)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->anusRectum; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->anusRectumRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Genito-Urinary System</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->genito; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->genitoRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Endocrine System</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->endocrine; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->endocrineRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Upper limbs, Lower limbs and Joints</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->upperLowerJoints; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->upperLowerJointsRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Spine & Spinal movements</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->spine; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->spineRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Neurology (Reflexes,equilibrium etc)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->neurology; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->neurologyRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Skin</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->skin; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->skinRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Psychiatric</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->psychiatric; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $py_details[0]->psychiatricRemark; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php } 
if($vs_details <> NULL){ ?>
<div id="div_vs">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Visual Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_vs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_vs" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_vs">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Lids, Pupils, Lens, Media, Fundi</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->LPLMF; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->LPLMFRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Does the candidate possess glasses?</td>
                        <td colspan="2" id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->glassesP; ?></td>
                    </tr>
                    <?php if($vs_details[0]->glassesP == "Yes"){ ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Distant Vision with glasses (Standard Test Types)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->dVisionWithGRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->dVisionWithGLeft; ?></td>
                    </tr>
                    <?php } ?>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Distant Vision without glasses (Standard Test Types)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->dVisionWithoutGRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->dVisionWithoutGLeft; ?></td>
                    </tr>
                    <?php if($vs_details[0]->glassesP == "Yes"){ ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Near Vision with glasses (N type at 30cm to 50cm)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->nVisionWithGRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->nVisionWithGLeft; ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Near Vision without glasses (N type at 30cm to 50cm)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->nVisionWithoutGRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->nVisionWithoutGLeft; ?></td>
                    </tr>
                    <?php if($vs_details[0]->glassesP == "Yes"){ ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Accommodation in cm (Near point 30cm) with glasses</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->accomodationWithGRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->accomodationWithGLeft; ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Accommodation in cm (Near point 30cm) without glasses</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->accomodationWithoutGRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->accomodationWithoutGLeft; ?></td>
                    </tr>
                    <?php if($vs_details[0]->glassesP == "Yes"){ ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Prescription of the glasses</td>
                        <td colspan="2" id="tdData">
                            <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th id="tdData"></th>
                                <th id="tdData">S (Right)</th>
                                <th id="tdData">C (Right)</th>
                                <th id="tdData">A (Right)</th>
                                <th id="tdData">S (Left)</th>
                                <th id="tdData">C (Left)</th>
                                <th id="tdData">A (Left)</th>
                            </tr>
                            <tr>
                                <td id="tdData">Distant</td>
                                <td id="tdData"><?php echo $vs_details[0]->distantSright; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->distantCright; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->distantAright; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->distantSleft; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->distantCleft; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->distantAleft; ?></td>
                            </tr>
                            <tr>
                                <td id="tdData">Near</td>
                                <td id="tdData"><?php echo $vs_details[0]->nearSright; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->nearCright; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->nearAright; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->nearSleft; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->nearCleft; ?></td>
                                <td id="tdData"><?php echo $vs_details[0]->nearAleft; ?></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Field of vision by confrontation test</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->fieldVision; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->fieldVisionRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Power of convergence</td>
                        <td colspan="2" id="tdData">&nbsp;&nbsp;<?php echo $vs_details[0]->powerConvergence; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php } 
if($ey_details <> NULL){ ?>
<div id="div_ey">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Eyes Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_ey" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_ey" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>&nbsp;&nbsp;
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_ey">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Manifest Hypermetropia with +2.5 dioptre lens</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ey_details[0]->manifestHyperRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ey_details[0]->manifestHyperLeft; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Color Perception (Tested by pseudoisochromatic [Ishihara] plates)</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ey_details[0]->cPerceptionIshihara; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ey_details[0]->cPerceptionIshiharaRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Color Perception (Tested by an approved Colour Perception Lantem[giles-Archer,Martin])</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ey_details[0]->cPerceptionLantem; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ey_details[0]->cPerceptionLantemRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Measure of Heterophoria</td>
                        <td colspan="2" id="tdData">
                            <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th id="tdData"></th>
                                <th id="tdData">Exophoria</th>
                                <th id="tdData">Esophoria</th>
                                <th id="tdData">Hyperphoria</th>
                            </tr>
                            <tr>
                                <td id="tdData">Maddox Rod</td>
                                <td id="tdData"><?php echo $ey_details[0]->maddoxRodExo; ?></td>
                                <td id="tdData"><?php echo $ey_details[0]->maddoxRodEso; ?></td>
                                <td id="tdData"><?php echo $ey_details[0]->maddoxRodHyper; ?></td>
                            </tr>
                            <tr>
                                <td id="tdData">Maddox Wing</td>
                                <td id="tdData"><?php echo $ey_details[0]->maddoxWingExo; ?></td>
                                <td id="tdData"><?php echo $ey_details[0]->maddoxWingEso; ?></td>
                                <td id="tdData"><?php echo $ey_details[0]->maddoxWingHyper; ?></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php } 
if($au_details <> NULL){ ?>
<div id="div_au">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Audio Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_au" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_au" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_au">
                <tbody>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Any hearing difficulty with conversational voice at 8 feet with back to examiner?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $au_details[0]->hearingDifficulty; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $au_details[0]->hearingDifficultyRemark; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;At what distance from the examiner can forced whisper be heard in each ear separately?</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $au_details[0]->forcedWhisperRight; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $au_details[0]->forcedWhisperLeft; ?></td>
                    </tr>

                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Audiometry</td>
                        <td colspan="2" id="tdData">
                            <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th id="tdData">Right</th>
                                <th id="tdData">Frequency</th>
                                <th id="tdData">Left</th>
                                <th id="tdData">Max Permitted Loss</th>
                                <th id="tdData">Remarks</th>
                            </tr>
                            <tr>
                                <td id="tdData"><?php echo $au_details[0]->audiometry3000Right; ?></td>
                                <td id="tdData">3000</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry3000Left; ?></td>
                                <td id="tdData">50</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry3000Remark; ?></td>
                            </tr>
                            <tr>
                                <td id="tdData"><?php echo $au_details[0]->audiometry2000Right; ?></td>
                                <td id="tdData">2000</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry2000Left; ?></td>
                                <td id="tdData">35</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry2000Remark; ?></td>
                            </tr>
                            <tr>
                                <td id="tdData"><?php echo $au_details[0]->audiometry1000Right; ?></td>
                                <td id="tdData">1000</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry1000Left; ?></td>
                                <td id="tdData">35</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry1000Remark; ?></td>
                            </tr>
                            <tr>
                                <td id="tdData"><?php echo $au_details[0]->audiometry500Right; ?></td>
                                <td id="tdData">500</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry500Left; ?></td>
                                <td id="tdData">35</td>
                                <td id="tdData"><?php echo $au_details[0]->audiometry500Remark; ?></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php } 
if($ot_details <> NULL){ ?>
<div id="div_ot">
    <div class="row">
        <div class="col-7">
            <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;">Other Details</h5>
        </div>
        <div class="offset-4">
            <span class="fa fa-chevron-circle-down fa-1x" style="cursor: pointer" id="full_ot" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="maximize"></span><span class="fa fa-chevron-circle-up fa-1x" style="cursor: pointer" id="small_ot" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="minimize"></span>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover table-condensed" id="table_ot">
                <tbody>
                    <?php if ($bs_details[0]->gender === "Female"){ ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Last Menstruation Date</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ot_details[0]->menstruationDate; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ot_details[0]->menstruationDateRemark; ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Routine ECG Report</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ot_details[0]->ecgReport; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ot_details[0]->ecgNextDate; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Routine CXR Report</td>
                        <td id="tdData" colspan="2">&nbsp;&nbsp;<?php echo $ot_details[0]->cxrReport; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Routine Audio Report</td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ot_details[0]->audioReport; ?></td>
                        <td id="tdData">&nbsp;&nbsp;<?php echo $ot_details[0]->audioNextDate; ?></td>
                    </tr>
                    <tr>
                        <td id="tdTitle">&nbsp;&nbsp;Urinalysis</td>
                        <td colspan="2" id="tdData">
                            <table class="table table-bordered table-striped table-hover">
                            <tr>
                                <th id="tdData">Protein</th>
                                <th id="tdData">Glucose</th>
                                <th id="tdData">Other</th>
                                <th id="tdData">Remarks</th>
                            </tr>
                            <tr>
                                <td id="tdData"><?php echo $ot_details[0]->urinalysisProtein; ?></td>
                                <td id="tdData"><?php echo $ot_details[0]->urinalysisGlucose; ?></td>
                                <td id="tdData"><?php echo $ot_details[0]->urinalysisOther; ?></td>
                                <td id="tdData"><?php echo $ot_details[0]->urinalysisremarks; ?></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>
<?php } ?>
</div>