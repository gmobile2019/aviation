<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <link rel="stylesheet" href="<?php echo base_url();?>styles/font-awesome/css/font-awesome.min.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <script src="<?php echo base_url() ?>styles/js/aviation.js"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
            $('#banner').blink({delay:1000});
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" id="banner">
                    Aviation Medical Examination System
                </div>
                <div id="title" class="col-12">
                    <?php echo $title; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/Administrator/"> Home</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/Administrator/users"> Users</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/Administrator/institutions"> Institutions</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/Administrator/medical_applications"> Applications</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php  echo base_url()?>index.php/Administrator/user_feedback"> User Feedback&nbsp;&nbsp;<?php echo $this->User_model->user_feedback(NULL,NULL,NULL,'New') <> NULL?'<i class="fa fa-envelope-o fa-x1" aria-hidden="true"></i>':''?></a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/Administrator/profile"> Profile</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-danger" href="<?php echo base_url()?>index.php/Home/logout">Logout</a>
                    </div>
                </div>
                <div class="col-9">
                    <div class="row">
                        <div class="col-12">
                            <?php echo $message; ?>
                        </div>
                        <div class="col-12">
                            <?php $this->load->view($content); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>