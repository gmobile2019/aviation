<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        
      
       
    });
</script>
<div id="divform">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administrator/add_institution/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="institutionname" class="col-4 control-label">Institution Name</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="institutionname" id="institutionname" placeholder="Institution Name" value="<?php echo $id != null?$data[0]->institutionname:set_value('institutionname'); ?>" required/>
                        <?php echo form_error('institutionname'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="institutionaddress" class="col-4 control-label">Institution Address</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="institutionaddress" id="institutionaddress" placeholder="Institution Address" value="<?php echo $id != null?$data[0]->institutionaddress:set_value('institutionaddress'); ?>" required/>
                        
                            <?php echo form_error('institutionaddress'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="institutionlocation" class="col-4 control-label">Institution Location</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="institutionlocation" id="institutionlocation" placeholder="Institution Location" value="<?php echo $id != null?$data[0]->institutionlocation:set_value('institutionlocation'); ?>" required/>
                        <?php echo form_error('institutionlocation'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="institutioncontact" class="col-4 control-label">Institution Contact</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="institutioncontact" id="institutioncontact" placeholder="Institution Contact"  value="<?php echo $id != null?$data[0]->institutioncontact:set_value('institutioncontact'); ?>" required/>
                        <?php echo form_error('institutioncontact'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-3 col-xs-12 col-4 btn btn-link">
                        <button type="submit" class="btn btn-success">Save Information</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
