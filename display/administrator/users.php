<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('a').find('span#pdf').css({
                                color: '#000000'
                            });
    $('a').find('span#excel').css({
                                color: '#000000'
                            });
    });
</script>
<div class="row">
    <div class="offset-1">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administrator/users',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="First/Middle/Last Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Status</option>
                            <option value="1" <?php echo $status == 1?"selected='selected'":"" ?>>Active</option>
                            <option value="2" <?php echo $status == 2?"selected='selected'":"" ?>>Suspended</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="grp"></label>
                        <select class="form-control" name="grp" id="grp">
                            <option value="">Category</option>
                            <?php foreach($groups as $key=>$value){ ?>
                            
                            <option value="<?php echo $value->id?>" <?php echo $grp ==$value->id?"selected='selected'":"" ?>><?php echo $value->name; ?></option>
                            
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="facility"></label>
                        <select class="form-control" name="facility" id="facility">
                            <option value="">Institution</option>
                            <?php foreach($institutions as $key=>$value){ ?>
                            
                            <option value="<?php echo $value->institutioncode?>" <?php echo $facility ==$value->institutioncode?"selected='selected'":"" ?>><?php echo $value->institutionname; ?></option>
                            
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
</div>
<div id="divform">
    <table class="table table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th colspan="7" style="text-align: center"><?php echo anchor('Administrator/add_user','Add Doctor');?></th>
            </tr>
            <tr>
                <th style="text-align: center">Full Name</th>
                <th style="text-align: center">Username</th>
                <th style="text-align: center">Mobile</th>
                <th style="text-align: center">Category</th>
                <th style="text-align: center">Institution</th>
                <th style="text-align: center">Status</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>

            <?php 
            if($data <> NULL){
            foreach($data as $key=>$value){ ?>
            <tr>
                <td>&nbsp;&nbsp;<?php echo $value->first_name.' '.$value->middle_name.' '.$value->last_name; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->username; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->msisdn; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->groupname; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->institutionname; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->active == 1?'Active':'Suspended'; ?></td>
                <td style="text-align: center">
                    <div class="row">
                        <div class="col-3">
                           <?php echo $value->group_id == 2?'<a href="'.  base_url().'index.php/Administrator/add_user/'.$value->id.'" class="fa fa-pencil-square-o fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="edit" style="cursor: pointer"></a>':""; ?> 
                        </div>
                        <div class="col-9">
                            <?php echo $value->active == 1?'<a href="'.  base_url().'index.php/Administrator/activate_deactivate_user/'.$value->id.'/'.$value->active.'" class="btn btn-danger btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="deactivate" style="cursor: pointer">Suspend</a>':'<a href="'.  base_url().'index.php/Administrator/activate_deactivate_user/'.$value->id.'/'.$value->active.'" class="btn btn-success btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="activate" style="cursor: pointer">Activate</a>'; ?>
                        </div>
                    </div>
                </td>
            </tr>
            <?php } }else { ?>
                <td colspan="7" class="table-warning" style="text-align: center">No Data Found</td>
            <?php } ?>
        </tbody>
    </table>
</div>


