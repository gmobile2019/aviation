<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
      var idTags;
        var possibles=[];
        
        
            $('a').find('span#pdf').css({
                                color: '#000000'
                            });
    $('a').find('span#excel').css({
                                color: '#000000'
                            });
    });
</script>
<div  class="row">
    <div class="offset-1">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administrator/user_feedback',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="date" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="date" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Status</option>
                            <option value="New" <?php echo $status == "New"?"selected='selected'":"" ?>>New</option>
                            <option value="Read" <?php echo $status == "Read"?"selected='selected'":"" ?>>Read</option>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="offset-6">
        <?php echo anchor("Administrator/user_feedback/strt_".$start."_end_".$end."_status_".$status."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
    </div>
</div>
<div id="divform">
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th style="text-align: center">Subject</th>
                <th style="text-align: center">Email</th>
                <th style="text-align: center">Mobile</th>
                <th style="text-align: center">Date</th>
                <th style="text-align: center">Status</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>

            <?php 
            if($data <> NULL){

            foreach($data as $key=>$value){ 

                ?>

            <tr>
                <td>&nbsp;&nbsp;<?php echo $value->feedbackSubject."&nbsp;&nbsp;"; echo $value->status == 'New'?'<i class="fa fa-envelope-o fa-x1" aria-hidden="true"></i>':'<i class="fa fa-envelope-open-o fa-x1" aria-hidden="true"></i>'?></td>
                <td>&nbsp;&nbsp;<?php echo $value->feedbackemail; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->mobileNumber; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                <td style="text-align: center">
                    <?php echo '<a href="'.  base_url().'index.php/Administrator/openFeedback/'.$value->id.'" class="fa fa-arrows-alt fa-x1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click to read" style="cursor: pointer"></a>'; ?>
                </td>
            </tr>
            <?php } } else{ ?>
            <tr>
                <td colspan="6" style="text-align: center" class="table-warning">No Data Found</td>
            </tr>
            <?php }?>
        </tbody>
    </table>
</div>


