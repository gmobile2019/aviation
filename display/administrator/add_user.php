<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        
      
       
    });
</script>
<div id="divform">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administrator/add_user/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="first_name" class="col-4 control-label">First Name</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $id != null?$member[0]->first_name:set_value('first_name'); ?>" required/>
                        <?php echo form_error('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middle_name" class="col-4 control-label">Middle Name</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Middle Name" value="<?php echo $id != null?$member[0]->middle_name:set_value('middle_name'); ?>"/>
                        
                            <?php echo form_error('middle_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="last_name" class="col-4 control-label">Last Name</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $id != null?$member[0]->last_name:set_value('last_name'); ?>" required/>
                        <?php echo form_error('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-4 control-label">Email Address</label>
                    <div class="col-4">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $id != null?$member[0]->email:set_value('email'); ?>" required/>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-4 control-label">Mobile</label>
                    <div class="col-4">
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $id != null?$member[0]->msisdn:set_value('mobile'); ?>" required/>
                        <?php echo form_error('mobile'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="institution" class="col-4 control-label">Institution</label>
                    <div class="col-4">
                    <select name="institution" id="institution" class="form-control" required>
                        <option></option>
                        <?php foreach($institutions as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->institutioncode; ?>" <?php echo ($id != null && trim($value->institutioncode) == trim($member[0]->institution))?'selected="selected"':set_select('institution',$value->institutioncode); ?>><?php echo $value->institutionname; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('institution'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="group" class="col-4 control-label">User Group</label>
                    <div class="col-4">
                    <select name="group" id="group" class="form-control" required>
                        <option></option>
                        <?php foreach($groups as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->group_id))?'selected="selected"':set_select('group',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('group'); ?>
                    </div>
                </div>
                <input type="hidden" class="form-control" name="username" value="<?php echo $member[0]->username; ?>"/>
                <div class="form-group row">
                    <label for="password" class="col-4 control-label">Password</label>
                    <div class="col-4">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                        <?php echo form_error('password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="conf_password" class="col-4 control-label">Confirm Password</label>
                    <div class="col-4">
                        <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Confirm Password"/>
                        <?php echo form_error('conf_password'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-3 col-xs-12 col-4 btn btn-link">
                        <button type="submit" class="btn btn-success">Save Information</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
