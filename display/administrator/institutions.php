<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('a').find('span#pdf').css({
                                color: '#000000'
                            });
    $('a').find('span#excel').css({
                                color: '#000000'
                            });
    });
</script>
<div class="row">
    <div class="offset-4">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administrator/institutions',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Status</option>
                            <option value="1" <?php echo $status == 1?"selected='selected'":""; ?>>Active</option>
                            <option value="2" <?php echo $status == 2?"selected='selected'":""; ?>>Suspended</option>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
</div>
<div id="divform">
    <table class="table table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th colspan="9" style="text-align: center"><?php echo anchor('Administrator/add_institution','Add Institution');?></th>
            </tr>
            <tr>
                <th style="text-align: center">Institution Name</th>
                <th style="text-align: center">Institution Code</th>
                <th style="text-align: center">Institution Address</th>
                <th style="text-align: center">Institution Contact</th>
                <th style="text-align: center">Institution Location</th>
                <th style="text-align: center">Status</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>

            <?php 
            if($data <> NULL){
            foreach($data as $key=>$value){ ?>
            <tr>
                <td>&nbsp;&nbsp;<?php echo $value->institutionname; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->institutioncode; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->institutionaddress; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->institutioncontact; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->institutionlocation; ?></td>
                <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Suspended'; ?></td>
                <td style="text-align: center">
                    <div class="row">
                        <div class="col-3">
                            <?php echo '<a href="'.  base_url().'index.php/Administrator/add_institution/'.$value->id.'" class="fa fa-pencil-square-o fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="edit" style="cursor: pointer"></a>'; ?>
                        </div>
                        <div class="col-9">
                            <?php echo $value->status == 1?'<a href="'.  base_url().'index.php/Administrator/activate_deactivate_institution/'.$value->institutioncode.'/'.$value->status.'" class="btn btn-danger btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="deactivate" style="cursor: pointer">Suspend</a>':'<a href="'.  base_url().'index.php/Administrator/activate_deactivate_institution/'.$value->institutioncode.'/'.$value->status.'" class="btn btn-success btn-sm" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="activate" style="cursor: pointer">Activate</a>'; ?>
                        </div>
                    </div>
                </td>
            </tr>
            <?php } }else{ ?>
                <td colspan="9" class="table-warning" style="text-align: center">No Data Found</td>
            <?php }?>
        </tbody>
    </table>
</div>


