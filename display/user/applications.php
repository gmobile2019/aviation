<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
      var applicant='<?php echo $this->session->userdata('username')?>';
      var idTags;
        var possibles=[];
         $( "#applicationRef").keyup(function(){
             
               var applicationRef=$(this).val();
              
               $.ajax({
                   type:'POST',
                   url:'<?php echo site_url('Administrator/applications'); ?>',
                   data:{applicationRef:applicationRef,applicant:applicant},
                   success:function(data){
                       
                       idTags=data.split("=_");
                       var arrLength=idTags.length;
                       var i; 
                       possibles.splice(0);
                        for(i=0;i<arrLength;i++){

                            possibles.push(idTags[i]);
                        }
                   }
               });
           });
           
        $("#applicationRef").autocomplete({
                  
            source: possibles
          });
        
        $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-20:+0"
            });
            
        $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-20:+0"
            });
            
            $('a').find('span#pdf').css({
                                color: '#000000'
                            });
    $('a').find('span#excel').css({
                                color: '#000000'
                            });
    });
</script>
<div class="row">
    <div class="offset-2">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('User/medical_applications',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="applicationRef"></label>
                        <input type="text" class="form-control" name="applicationRef" id="applicationRef" placeholder="Application Reference" value="<?php echo $applicationRef; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="date" class="form-control" name="start" id="start" placeholder="Start" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="date" class="form-control" name="end" id="end" placeholder="End" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="status"></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">Status</option>
                            <option value="pending" <?php echo $status == "pending"?"selected='selected'":"" ?>>Pending</option>
                            <option value="complete" <?php echo $status == "complete"?"selected='selected'":"" ?>>Complete</option>
                            <option value="onprogress" <?php echo $status == "onprogress"?"selected='selected'":"" ?>>On Progress</option>
                            <option value="certified" <?php echo $status == "certified"?"selected='selected'":"" ?>>Certified</option>
                            <option value="declined" <?php echo $status == "declined"?"selected='selected'":"" ?>>Declined</option>
                        </select>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="search"value="Search" /> 
                    </div>
        
            <?php echo form_close(); ?>
    </div>
    <div class="offset-6">
        <?php echo anchor("User/medical_applications/ref_".$applicationRef."_strt_".$start."_end_".$end."_status_".$status."_docType_1/",'<span id="pdf" class="glyphicon glyphicon-print" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">pdf</span>'); ?>
    </div>
</div>
<div id="divform">
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th style="text-align: center">Application Reference</th>
            <th style="text-align: center">Applicant</th>
            <th style="text-align: center">Facility</th>
            <th style="text-align: center">Application Date</th>
            <th style="text-align: center">Doctor</th>
            <th style="text-align: center">Status</th>
            <th style="text-align: center">Action</th>
        </tr>
    </thead>
    <tbody>
        
        <?php 
        if($data <> NULL){
        
        foreach($data as $key=>$value){ 
           $applcnt=$this->User_model->registrationInfo(NULL,$value->applicant);
            $fclty=$value->facility <> NULL?$this->SuperAdministration_model->institutions(NULL,$value->facility):"";
            $appDate=explode(' ',$value->appliedOn);
            $applicationDate=explode('-',$appDate[0]);
            
            $doc=$value->attendedBy <> NULL?$this->SuperAdministration_model->get_member_info(NULL,$value->attendedBy):"";
            ?>
        <tr>
            <td>&nbsp;&nbsp;<?php echo $value->applicationRef; ?></td>
            <td>&nbsp;&nbsp;<?php echo $applcnt[0]->fname.' '.$applcnt[0]->surname; ?></td>
            <td>&nbsp;&nbsp;<?php echo $fclty[0]->institutionname; ?></td>
            <td>&nbsp;&nbsp;<?php echo $applicationDate[2].'/'.$applicationDate[1].'/'.$applicationDate[0]; ?></td>
            <td>&nbsp;&nbsp;<?php echo $doc[0]->first_name.' '.$doc[0]->last_name; ?></td>
            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
            <td style="text-align: center">
                <?php echo $value->status <> 'pending'?'<a href="'.  base_url().'index.php/User/applicationDetails/'.$value->applicationRef.'" class="fa fa-arrows-alt fa-x1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="more details" style="cursor: pointer"></a>':''; ?>
                &nbsp;&nbsp;
                <?php echo $value->status == 'pending'?'<a href="'.base_url().'index.php/User/reviewApplication/'.$value->applicationRef.'" class="fa fa-exchange fa-x1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="review/continue application" style="cursor: pointer" ></a>':'' ?></td>
        </tr>
        <?php } } else{ ?>
        <tr class="table-warning">
            <td colspan="7" style="text-align: center" >No Data Found</td>
        </tr>
        <?php }?>
    </tbody>
</table>
</div>

<div class="modalAnimate"><!-- Place at bottom of page --></div>

