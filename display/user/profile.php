<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        var clickId;
        var id;
        var dataValue;
        var msg;
        
        $('span.fa-check-circle').hide();
        $('div.notshow').hide();
        
        $('span.fa-pencil-square-o').each(function(){
           
           $(this).click(function(){
               clickId=$(this).attr('id');
               id=clickId.split('_');
            
               $(this).fadeOut();
               $(this).parent().parent().parent().find('div#'+id[0]+'_dsply_'+id[3]).fadeOut();
               $(this).parent().parent().parent().find('div#'+id[0]+'_edit_'+id[3]).fadeIn();
               $('span#'+id[0]+'_save_'+id[3]).fadeIn();
               $('#'+id[0]+'_'+id[3]).focus();
           });
        });
        
        $('span.fa-check-circle').each(function(){
           
           $(this).click(function(){
               clickId=$(this).attr('id');
               id=clickId.split('_');
               dataValue=$('#'+id[0]+'_'+id[2]).val();
               
               $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/save_profile'); ?>',
                data:{name:id[0],value:dataValue},
                success:function(data){
                       
                        if(data === "Data Successfully Saved!"){
                            $('div#'+id[0]+'_dsply_'+id[2]).empty();
                            $('div#'+id[0]+'_dsply_'+id[2]).text(dataValue);
                            msg='<span class="alert alert-success">data successfully updated!</span>';
                        }else{
                            msg='<span class="alert alert-danger" role="alert" style="text-align:center;">data saving error!</span>';
                        }
                        
                        $('div#'+id[0]+'_edit_'+id[2]).fadeOut();
                        $('span#'+id[0]+'_edit_action_'+id[2]).fadeIn();
                        $('span#'+id[0]+'_save_'+id[2]).fadeOut();
                        $('div#'+id[0]+'_dsply_'+id[2]).fadeIn();
                        $('div#responseMessage').empty();
                        $('div#responseMessage').append(msg);
                 }

                });
               
           });
        });
        
        $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
    });
</script>
<div id="divform">

<table class="table table-bordered table-hover table-condensed table-dark">
    <tbody>
    <tr>
        <th>Title</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="title_dsply_<?php echo $data[0]->id ?>">
                    <?php echo $data[0]->title;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="title_edit_<?php echo $data[0]->id ?>">
                    <select name="title_<?php echo $data[0]->id ?>" id="title_<?php echo $data[0]->id ?>" class="form-control">
                        <option value="Mr" <?php echo $data[0]->title == "Mr"?"selected='selected'":""?>>Mr</option>
                        <option value="Miss" <?php echo $data[0]->title == "Miss"?"selected='selected'":""?>>Miss</option>
                        <option value="Mrs" <?php echo $data[0]->title == "Mrs"?"selected='selected'":""?>>Mrs</option>
                    </select>
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="title_edit_action_<?php echo $data[0]->id ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="title_save_<?php echo $data[0]->id ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>First Name</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fname_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->fname;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="fname_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="fname_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->fname; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="fname_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="fname_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Middle Name</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mname_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->mname;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="mname_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="mname_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->mname; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="mname_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="mname_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Surname</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="surname_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->surname;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="surname_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="surname_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->surname; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="surname_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x " style="cursor: pointer" id="surname_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Email</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="email_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->email;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="email_edit_<?php echo $data[0]->id; ?>">
                    <input type="hidden" id="email_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->email; ?>">
                </div>  
            </div>
        </td>
        <td>
           
        </td>
    </tr>
    <tr>
        <th>Mobile No</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mobile_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->mobile;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="mobile_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="mobile_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->mobile; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="mobile_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="mobile_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Gender</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="gender_dsply_<?php echo $data[0]->id ?>">
                    <?php echo $data[0]->gender;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="gender_edit_<?php echo $data[0]->id ?>">
                    <select id="gender_<?php echo $data[0]->id ?>" class="form-control">
                        <option value="Male" <?php echo $data[0]->gender == "Male"?"selected='selected'":""?>>Male</option>
                        <option value="Female" <?php echo $data[0]->gender == "Female"?"selected='selected'":""?>>Female</option>
                    </select>
                </div>  
            </div>
        </td>
        <td>
            <div class="col-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="gender_edit_action_<?php echo $data[0]->id ?>"></span>
            </div>
            <div class="col-12">
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="gender_save_<?php echo $data[0]->id ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Date Of Birth</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="dob_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->dob;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="dob_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control dob" id="dob_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->dob; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="dob_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="dob_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Place of Birth</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="pob_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->pob;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="pob_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="pob_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->pob; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="pob_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="pob_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Nationality</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="nationality_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->nationality;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="nationality_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="nationality_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->nationality; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="nationality_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="nationality_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Permanent Address</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="paddress_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->paddress;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="paddress_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="paddress_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->paddress; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="paddress_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="paddress_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>Postal</th>
        <td>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="postal_dsply_<?php echo $data[0]->id; ?>">
                    <?php echo $data[0]->postal;?>
                </div>  
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="postal_edit_<?php echo $data[0]->id; ?>">
                    <input type="text" class="form-control" id="postal_<?php echo $data[0]->id; ?>" value="<?php echo $data[0]->postal; ?>">
                </div>  
            </div>
        </td>
        <td>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="postal_edit_action_<?php echo $data[0]->id; ?>"></span>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="postal_save_<?php echo $data[0]->id; ?>"></span>
            </div>
        </td>
    </tr>
    </tbody>
</table>

</div>

