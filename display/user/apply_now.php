<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        
        $("form#background").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/save_background'); ?>',
                data:$(this).serialize(),
                success: function(data){
                      
                        if(data !== ""){
                            
                            $('div#background').slideUp('slow');
                            $('div#medicalHistory').slideDown('slow');
                            $('input[name=applicationRef]').val(data);
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#medicalHistory").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/save_medicalHistory'); ?>',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                           
                            $('div#medicalHistory').slideUp('slow');
                            $('div#employment').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#employment").submit(function(e){
           
            $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/save_occupation'); ?>',
                data:$(this).serialize(),
                success: function(data){
                       
                        if(data !== ""){
                            
                            $('div#employment').slideUp('slow');
                            $('div#personalHealth').slideDown('slow');
                            $('input[name=applicationRef]').val(data);
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#personalHealth").submit(function(e){
           
            $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/save_personalHealth'); ?>',
                data:$(this).serialize(),
                success: function(data){
                       
                        if(data !== ""){
                           
                            $('div#personalHealth').slideUp('slow');
                            $('div#airCraft').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#airCraft").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/save_aircraftExperience'); ?>',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                          
                            $('div#airCraft').slideUp('slow');
                            $('div#declaration').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
        
        $("form#declaration").submit(function(e){
            
            $.ajax({
                type:'POST',
                url:'<?php echo site_url('User/complete_application'); ?>',
                data:$(this).serialize(),
                success: function(data){
                     
                        if(data !== ""){
                            
                            $('div#feedbackMessage').find('p').find('span').text(data);
                            $('div#declaration').slideUp('slow');
                            $('div#feedbackMessage').slideDown('slow');
                        }
                 }
                });
                 e.preventDefault();
        });
    });
</script>
<input type="hidden" name="initialExamination" value="<?php echo $initialExamination == NULL?TRUE:FALSE; ?>"/>
<input type="hidden" name="reviewContinue" value="<?php echo $reviewContinue; ?>"/>
<div id="divform">
<!---------------------------------------------------------back ground information------------------------------------------------------------>
<div id="background">
    <h5 class="applicationHeader">Background Information</h5>
    <form class="form-horizontal" id="background" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $referenceNo; ?>"/>
        <div class="row applicationDiv">
            <label for="armedForce" class="col-6 form-control-label">Have you ever served in Armed Forces?</label>
            <div class="col-4">
                <select name="armedForce" class="form-control" required>
                    <option value="" ></option>
                    <option value="Yes" <?php echo set_select('armedForce','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('armedForce','No')?>>No</option>
                </select>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="pilotExperience" class="col-6 form-control-label">Have you ever had previous Pilot or Aircrew experience?</label>
            <div class="col-4">
                <select name="pilotExperience" class="form-control" required>
                    <option value="" ></option>
                    <option value="Yes" <?php echo set_select('pilotExperience','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('pilotExperience','No')?>>No</option>
                </select>
            </div>
        </div>
        <div class="row applicationDiv">
            <button type="submit" class="offset-8 col-xs-12 col-4 btn btn-link submit" id="backgroundLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>
<!-------------------------------------------------Medical History Information------------------------------------------------------------>
<div id="medicalHistory">
    <h5 class="applicationHeader">Medical History Information</h5>
    <form class="form-horizontal" id="medicalHistory" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $referenceNo; ?>"/>
        <div class="row applicationDiv">
        <label for="fsHeadaches" class="col-6 form-control-label">Do you have a history of frequent or severe headaches?</label>
        <div class="col-xs-12 col-3">
            <select name="fsHeadaches" class="form-control" required>
                <option value=""></option>
                <option value="Yes" <?php echo set_select('fsHeadaches','Yes')?>>Yes</option>
                <option value="No" <?php echo set_select('fsHeadaches','No')?>>No</option>
            </select>
        </div>
        <div class="col-xs-12 col-3 fsHeadaches">
            <textarea class="form-control" name="fsHeadachesRemark" id="fsHeadachesRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
            <label for="dfu" class="col-6 form-control-label">Do you have a history of Dizziness,Fainting or Unconciousness?</label>
            <div class="col-xs-12 col-3">
                <select name="dfu" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('dfu','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('dfu','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 dfu">
                <textarea class="form-control" name="dfuRemark" id="dfuRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="eyeTrouble" class="col-6 form-control-label">Do you have a history of Eye trouble?</label>
            <div class="col-xs-12 col-3">
                <select name="eyeTrouble" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('eyeTrouble','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('eyeTrouble','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 eyeTrouble">
                <textarea class="form-control" name="eyeTroubleRemark" id="eyeTroubleRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="hayFever" class="col-6 form-control-label">Do you have a history of Hay Fever?</label>
            <div class="col-xs-12 col-3">
                <select name="hayFever" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('hayFever','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('hayFever','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 hayFever">
                <textarea class="form-control" name="hayFeverRemark" id="hayFeverRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="asthma" class="col-6 form-control-label">Do you have a history of Asthma?</label>
            <div class="col-xs-12 col-3">
                <select name="asthma" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('asthma','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('asthma','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 asthma">
                <textarea class="form-control" name="asthmaRemark" id="asthmaRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="heartTrouble" class="col-6 form-control-label">Do you have a history of Heart trouble?</label>
            <div class="col-xs-12 col-3">
                <select name="heartTrouble" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('heartTrouble','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('heartTrouble','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 heartTrouble">
                <textarea class="form-control" name="heartTroubleRemark" id="fsHeadachesRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="bloodPressure" class="col-6 form-control-label">Do you have a history of High or Low Blood Pressure?</label>
            <div class="col-xs-12 col-3">
                <select name="bloodPressure" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('bloodPressure','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('bloodPressure','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 bloodPressure">
                <textarea class="form-control" name="bloodPressureRemark" id="bloodPressureRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="stomachTrouble" class="col-6 form-control-label">Do you have a history of Stomach trouble?</label>
            <div class="col-xs-12 col-3">
                <select name="stomachTrouble" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('stomachTrouble','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('stomachTrouble','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 stomachTrouble">
                <textarea class="form-control" name="stomachTroubleRemark" id="stomachTroubleRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="ksbu" class="col-6 form-control-label">Do you have a history of Kidney Stones or Blood in Urine?</label>
            <div class="col-xs-12 col-3">
                <select name="ksbu" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('ksbu','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('ksbu','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 ksbu">
                <textarea class="form-control" name="ksbuRemark" id="ksbuRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="su" class="col-6 form-control-label">Do you have a history of Sugar in Urine?</label>
            <div class="col-xs-12 col-3">
                <select name="su" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('su','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('su','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 su">
                <textarea class="form-control" name="suRemark" id="suRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="epilepsy" class="col-6 form-control-label">Do you have a history of Epilepsy?</label>
            <div class="col-xs-12 col-3">
                <select name="epilepsy" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('epilepsy','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('epilepsy','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 epilepsy">
                <textarea class="form-control" name="epilepsyRemark" id="epilepsyRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="nervousTrouble" class="col-6 form-control-label">Do you have a history of Nervous trouble of any sort?</label>
            <div class="col-xs-12 col-3">
                <select name="nervousTrouble" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('nervousTrouble','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('nervousTrouble','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 nervousTrouble">
                <textarea class="form-control" name="nervousTroubleRemark" id="nervousTroubleRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="motionSickness" class="col-6 form-control-label">Do you have a history of Motion Sickness requiring Drugs?</label>
            <div class="col-xs-12 col-3">
                <select name="motionSickness" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('motionSickness','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('motionSickness','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 motionSickness">
                <textarea class="form-control" name="motionSicknessRemark" id="motionSicknessRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="lifeInsurance" class="col-6 form-control-label">Have you ever been refused Life Insurance?</label>
            <div class="col-xs-12 col-3">
                <select name="lifeInsurance" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('lifeInsurance','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('lifeInsurance','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 lifeInsurance">
                <textarea class="form-control" name="lifeInsuranceRemark" id="lifeInsuranceRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="hospitalAdmission" class="col-6 form-control-label">Have you ever been admitted to Hospital?</label>
            <div class="col-xs-12 col-3">
                <select name="hospitalAdmission" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('hospitalAdmission','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('hospitalAdmission','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 hospitalAdmission">
                <textarea class="form-control" name="hospitalAdmissionRemark" id="hospitalAdmissionRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="otherIllness" class="col-6 form-control-label">Do you have a history of any other Illness?</label>
            <div class="col-xs-12 col-3">
                <select name="otherIllness" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('otherIllness','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('otherIllness','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 otherIllness">
                <textarea class="form-control" name="otherIllnessRemark" id="otherIllnessRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="headInjury" class="col-6 form-control-label">Do you have a history of Head Injury?</label>
            <div class="col-xs-12 col-3">
                <select name="headInjury" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('headInjury','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('headInjury','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 headInjury">
                <textarea class="form-control" name="headInjuryRemark" id="headInjuryRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="MTDiseases" class="col-6 form-control-label">Do you have a history of Malaria/Tropical Diseases?</label>
            <div class="col-xs-12 col-3">
                <select name="MTDiseases" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('MTDiseases','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('MTDiseases','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 MTDiseases">
                <textarea class="form-control" name="MTDiseasesRemark" id="MTDiseasesRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="addiction" class="col-6 form-control-label">Have you ever been treated for Alcoholism/Drug addiction?</label>
            <div class="col-xs-12 col-3">
                <select name="addiction" class="form-control" required>
                    <option value="" ></option>
                    <option value="Yes" <?php echo set_select('addiction','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('addiction','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 addiction">
                <textarea class="form-control" name="addictionRemark" id="addictionRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="familyHistory" class="col-6 form-control-label">Any family history of Diabetes,Epilepsy or Tuberculosis?</label>
            <div class="col-xs-12 col-3">
                <select name="familyHistory" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('familyHistory','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('familyHistory','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 familyHistory">
                <textarea class="form-control" name="familyHistoryRemark" id="familyHistoryRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="flyingLicense" class="col-6 form-control-label">Have you ever been refused a flying license?</label>
            <div class="col-xs-12 col-3">
                <select name="flyingLicense" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('flyingLicense','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('flyingLicense','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 flyingLicense">
                <textarea class="form-control" name="flyingLicenseRemark" id="flyingLicenseRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv armedForce">
            <label for="discharge" class="col-6 form-control-label">Do you have a history of being discharged form Armed Forces on Medical Grounds?</label>
            <div class="col-xs-12 col-3">
                <select name="discharge" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('discharge','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('discharge','No')?>>No</option>
                </select>
            </div>
            <div class="col-xs-12 col-3 discharge">
                <textarea class="form-control" name="dischargeRemark" id="dischargeRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <button type="submit" class="offset-8 col-xs-12 col-4 btn btn-link" id="medicalHistoryLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
</div>
<!------------------------------------employment / occupation information---------------------------------------------------------------------------->   
<div id="employment">
    <h5 class="applicationHeader">Occupation/Employment Information</h5>
    <form class="form-horizontal" id="employment" role="form" method="POST">
        <input type="hidden" name="applicationRef" value=""/>
        <div class="row applicationDiv">
            <label for="occupation" class="col-6 form-control-label">Occupation</label>
            <div class="col-4">
                <input type="text" class="form-control" name="occupation" id="occupation" placeholder="Occupation" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="employer" class="col-6 form-control-label">Employer</label>
            <div class="col-4">
                <input type="text" class="form-control" name="employer" id="employer" placeholder="Employer"  required>
            </div>
        </div>
        <div class="row applicationDiv">
            <button type="submit" class="offset-8 col-xs-12 col-4 btn btn-link" id="employmentLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>
<!--------------------------------------personal health information------------------------------------------------------------------------>
<div id="personalHealth">
    <h5 class="applicationHeader">Health/Welfare Information</h5>
    <form class="form-horizontal" id="personalHealth" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $referenceNo; ?>" required/>
        <div class="row applicationDiv">
            <label for="gp" class="col-6 form-control-label">Do you have an own General Practitioner?</label>
            <div class="col-4">
                <select name="gp" class="form-control" required>
                    <option value="" ></option>
                    <option value="Yes" <?php echo set_select('gp','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('gp','No')?>>No</option>
                </select>
            </div>
        </div>
        <div class="row applicationDiv gp">
            <label for="gpName" class="col-6 form-control-label">Name of General Practitioner</label>
            <div class="col-4">
                <input type="text" class="form-control" name="gpName" id="gpName" placeholder="GP Name">
            </div>
        </div>
        <div class="row applicationDiv gp">
            <label for="gpAddress" class="col-6 form-control-label">Address of General Practitioner</label>
            <div class="col-4">
                <input type="text" class="form-control" name="gpAddress" id="gpAddress" placeholder="GP Address">
            </div>
        </div>
        <div class="row applicationDiv gp">
            <label for="gpContact" class="col-6 form-control-label">Contact of General Practitioner</label>
            <div class="col-4">
                <input type="text" class="form-control" name="gpContact" id="gpContact" placeholder="GP Contact">
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="medication" class="col-6 form-control-label">Do you have any medication presently being prescribed?</label>
            <div class="col-4">
                <select name="medication" class="form-control" required>
                    <option value="" ></option>
                    <option value="Yes" <?php echo set_select('medication','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('medication','No')?>>No</option>
                </select>
            </div>
        </div>
        <div class="row applicationDiv medication">
            <label for="medDescription" class="col-6 form-control-label">Describe the medication being prescribed</label>
            <div class="col-4">
                <textarea class="form-control" name="medDescription" id="medDescription"></textarea>
            </div>
        </div>
        <div class="row applicationDiv medication">
            <label for="medPurpose" class="col-6 form-control-label">What is the purpose of the medication being prescribed?</label>
            <div class="col-4">
                <textarea class="form-control" name="medPurpose" id="medPurpose"></textarea>
            </div>
        </div>
        <div class="row applicationDiv medication">
            <label for="medDoctor" class="col-6 form-control-label">Who prescribed the medication?</label>
            <div class="col-4">
                <input type="text" class="form-control" name="medDoctor" id="medDoctor"  placeholder="Prescribing Doctor"/>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="noIllAccDis" class="col-6 form-control-label">How many illness,accidents or disabilities have you suffered since the last Medical Examination ?</label>
            <div class="col-4">
                <input type="number" class="form-control" name="noIllAccDis" id="noIllAccDis"  required>
            </div>
        </div>
        <div class="row applicationDiv">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="IllAccDis">

            </div>
        </div>
        <div class="row applicationDiv">
            <button type="submit" class="offset-8 col-xs-12 col-4 btn btn-link" id="personalHealthLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
</div>
<!--------------------------------------aircraft experience information------------------------------------------------------------------------>
<div id="airCraft">
    <h5 class="applicationHeader">Aircraft Experience Information</h5>
    <form class="form-horizontal" id="airCraft" role="form" method="POST">
            <input type="hidden" name="applicationRef" value="<?php echo $referenceNo; ?>"/>
            <div class="row applicationDiv">
                <label for="licenceType" class="col-6 form-control-label">Type of License held or applied for</label>
                <div class="col-4">
                    <select name="licenceType" class="form-control" required>
                        <?php foreach($licenceTypes as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo set_select('licenceType',$value->id)?>><?php echo $value->licenceType ?></option>

                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="row applicationDiv">
                <label for="licenseNumber" class="col-6 form-control-label">License Number</label>
                <div class="col-4">
                    <input type="text" class="form-control" name="licenseNumber" id="licenseNumber" placeholder="License Number" />
                </div>
            </div>
            <div class="row applicationDiv">
                <label for="expDteLMC" class="col-6 form-control-label">Expiry Date(s) of Last Medical Certificate(s)</label>
                <div class="col-4">
                    <input type="text" class="form-control" name="expDteLMC" id="expDteLMC" placeholder="dd/mm/yyyy - dd/mm/yyyy"  />
                </div>
            </div>
            <div class="row applicationDiv">
                <label for="expDte5YrL" class="col-6 form-control-label">Expiry Date(s) of 5 Year License(s)</label>
                <div class="col-4">
                    <input type="text" class="form-control" name="expDte5YrL" id="expDte5YrL" placeholder="dd/mm/yyyy - dd/mm/yyyy"  />
                </div>
            </div>
            <div class="row applicationDiv">
                <label for="hrsFlown" class="col-6 form-control-label">Total hours flown since last Medical Examination</label>
                <div class="col-4">
                    <input type="number" class="form-control" name="hrsFlown" id="hrsFlown" placeholder="Hours Flown"  />
                </div>
            </div>
            <div class="row applicationDiv">
                <label for="noAirType" class="col-6 form-control-label">How many Aircraft Types have you flown since last Medical Examination</label>
                <div class="col-4">
                    <input type="number" class="form-control" name="noAirType" id="noAirType"  required>
                </div>
            </div>
            <div class="row applicationDiv">
                <label for="noAirRoute" class="col-6 form-control-label">How many Aircraft Routes have you flown since last Medical Examination</label>
                <div class="col-4">
                    <input type="number" class="form-control" name="noAirRoute" id="noAirRoute"  required>
                </div>
            </div>
            <div class="row applicationDiv">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="airCraftTypeRoute">

                </div>
            </div>
            <div class="row applicationDiv">
                <label for="airAccident" class="col-6 form-control-label">Have you been involved in an Aircraft Accident since Last Medical Examination?</label>
                <div class="col-4">
                    <select name="airAccident" class="form-control" required>
                        <option value="" ></option>
                        <option value="Yes" <?php echo set_select('airAccident','Yes')?>>Yes</option>
                        <option value="No" <?php echo set_select('airAccident','No')?>>No</option>
                    </select>
                </div>
            </div>
            <div class="row applicationDiv airAccident">
                <label for="noAirAccident" class="col-6 form-control-label">How many Aircraft Accidents have you been involved since Last Medical Examination?</label>
                <div class="col-4">
                    <input type="number" class="form-control" name="noAirAccident" id="noAirAccident">
                </div>
            </div>
            <div class="row applicationDiv airAccident">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="noAirAccident">

                </div>
            </div>
            <div class="row applicationDiv">
                <button type="submit" class="offset-8 col-xs-12 col-4 btn btn-link" id="airCraftLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
            </div>
    </form>
</div>

<!------------------------------------declaration information---------------------------------------------------------------------------->   
<div id="declaration">
    <h5 class="applicationHeader">Declaration</h5>
    <form class="form-horizontal" id="declaration" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $referenceNo; ?>"/>
        <div class="row applicationDiv">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-12">
                <p>
                    I here by declare that I have carefully considered the statements made above 
                    and that to the best of my belief they are complete and correct 
                    and that I have not withheld any relevant information or made any misleading statement.
                </p>
                <p>
                    By Checking the box below you agree to the declaration stated above.
                </p>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="dclr" class="col-6 form-control-label">Please check the box</label>
            <div class="col-4">
                <input type="checkbox" class="form-control" name="dclr" id="dclr" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <button type="submit" class="offset-8 col-xs-12 col-4 btn btn-link" id="declaration">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>
<!------------------------------------------message to the applicant------------------------------------------------------------------------------>
<div class="row applicationDiv" id="feedbackMessage">
    <div class="col-xs-12 col-sm-12 col-md-12 col-md-12">
        <p>
            Online application for medical examination certificate has been successfully completed.
            Please visit the appropriate medical health facility for medical checkup so as to receive the certificate.
            Application reference number is <span style="font-style: oblique;font-weight: bold"></span>
        </p>
    </div>
</div>

</div>
<div class="modalAnimate"><!-- Place at bottom of page --></div>
