<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/aviation.css">
        <link rel="stylesheet" href="<?php echo base_url();?>styles/font-awesome/css/font-awesome.min.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>
        <script src="<?php echo base_url() ?>styles/js/aviation.js"></script>
        <title>Aviation</title>
        <script type="text/javascript">
           
            $('#banner').blink({delay:1000});
            
            $( document ).ajaxStart(function() {
               
                $('body').addClass("loading");
              });
              
            $( document ).ajaxStop(function() {
                
                $('body').removeClass("loading");
              });
            
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" id="banner">
                    Aviation Medical Examination System
                </div>
                <div id="title" class="col-12">
                    <?php echo $title; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/User/"> Home</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/User/medical_applications"> Applications</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/User/apply"> Apply Now</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-link" href="<?php echo base_url()?>index.php/User/profile"> Profile</a>
                    </div>
                    <div class="userLinks">
                        <a class="btn btn-danger" href="<?php echo base_url()?>index.php/Home/logout">Logout</a>
                    </div>
                </div>
                <div class="col-9">
                    <div class="row">
                        <div class="col-12">
                            <?php echo $message; ?>
                        </div>
                        <div class="col-12">
                            <?php $this->load->view($content); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
                <div class="col" id="footer">Allsee Technologies &commat;<?php echo date('Y'); ?></div>
            </div>
        </div>
    </body>
</html>