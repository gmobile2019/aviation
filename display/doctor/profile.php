<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        var clickId;
        var id;
        var dataValue;
        var msg;
        
        $('span.fa-check-circle').hide();
        $('div.notshow').hide();
        
        $('span.fa-pencil-square-o').each(function(){
           
           $(this).click(function(){
               clickId=$(this).attr('id');
               id=clickId.split('_');
            
               $(this).fadeOut();
               $(this).parent().parent().parent().find('div#'+id[0]+'_dsply_'+id[3]).fadeOut();
               $(this).parent().parent().parent().find('div#'+id[0]+'_edit_'+id[3]).fadeIn();
               $('span#'+id[0]+'_save_'+id[3]).fadeIn();
               $('#'+id[0]+'_'+id[3]).focus();
           });
        });
        
        $('span.fa-check-circle').each(function(){
           
           $(this).click(function(){
               clickId=$(this).attr('id');
               id=clickId.split('_');
               dataValue=$('#'+id[0]+'_'+id[2]).val();
               
               $.ajax({
                type:'POST',
                url:'<?php echo site_url('Administrator/save_profile'); ?>',
                data:{name:id[0],value:dataValue},
                success:function(data){
                       
                        if(data === "Data Successfully Saved!"){
                            $('div#'+id[0]+'_dsply_'+id[2]).empty();
                            $('div#'+id[0]+'_dsply_'+id[2]).text(dataValue);
                            msg='<span class="alert alert-success">data successfully updated!</span>';
                        }else{
                            msg='<span class="alert alert-danger" role="alert" style="text-align:center;">data saving error!</span>';
                        }
                        
                        $('div#'+id[0]+'_edit_'+id[2]).fadeOut();
                        $('span#'+id[0]+'_edit_action_'+id[2]).fadeIn();
                        $('span#'+id[0]+'_save_'+id[2]).fadeOut();
                        $('div#'+id[0]+'_dsply_'+id[2]).fadeIn();
                        $('div#responseMessage').empty();
                        $('div#responseMessage').append(msg);
                 }

                });
               
           });
        });
        
        $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
    });
</script>
<div id="divform">
    <table class="table table-bordered table-hover table-condensed table-dark">
        <tbody>
        <tr>
            <th>First Name</th>
            <td>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fname_dsply_<?php echo $data->id; ?>">
                        <?php echo $data->first_name;?>
                    </div>  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="fname_edit_<?php echo $data->id; ?>">
                        <input type="text" class="form-control" id="fname_<?php echo $data->id; ?>" value="<?php echo $data->first_name; ?>">
                    </div>  
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="fname_edit_action_<?php echo $data->id; ?>"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="fname_save_<?php echo $data->id; ?>"></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>Middle Name</th>
            <td>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mname_dsply_<?php echo $data->id; ?>">
                        <?php echo $data->middle_name;?>
                    </div>  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="mname_edit_<?php echo $data->id; ?>">
                        <input type="text" class="form-control" id="mname_<?php echo $data->id; ?>" value="<?php echo $data->middle_name; ?>">
                    </div>  
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="mname_edit_action_<?php echo $data->id; ?>"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="mname_save_<?php echo $data->id; ?>"></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>Surname</th>
            <td>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="surname_dsply_<?php echo $data->id; ?>">
                        <?php echo $data->last_name;?>
                    </div>  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="surname_edit_<?php echo $data->id; ?>">
                        <input type="text" class="form-control" id="surname_<?php echo $data->id; ?>" value="<?php echo $data->last_name; ?>">
                    </div>  
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="surname_edit_action_<?php echo $data->id; ?>"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <span class="fa fa-check-circle fa-1x " style="cursor: pointer" id="surname_save_<?php echo $data->id; ?>"></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>Email</th>
            <td>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="email_dsply_<?php echo $data->id; ?>">
                        <?php echo $data->email;?>
                    </div>  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="email_edit_<?php echo $data->id; ?>">
                        <input type="email" class="form-control" id="email_<?php echo $data->id; ?>" value="<?php echo $data->email; ?>">
                    </div>  
                </div>
            </td>
            <td>
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="email_edit_action_<?php echo $data->id; ?>"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="email_save_<?php echo $data->id; ?>"></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>Mobile No</th>
            <td>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mobile_dsply_<?php echo $data->id; ?>">
                        <?php echo $data->msisdn;?>
                    </div>  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="mobile_edit_<?php echo $data->id; ?>">
                        <input type="text" class="form-control" id="mobile_<?php echo $data->id; ?>" value="<?php echo $data->msisdn; ?>">
                    </div>  
                </div>
            </td>
            <td>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="fa fa-pencil-square-o fa-1x" style="cursor: pointer" id="mobile_edit_action_<?php echo $data->id; ?>"></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <span class="fa fa-check-circle fa-1x" style="cursor: pointer" id="mobile_save_<?php echo $data->id; ?>"></span>
                </div>
            </td>
        </tr>
        <tr>
            <th>Username</th>
            <td>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="username_dsply_<?php echo $data->id; ?>">
                        <?php echo $data->username;?>
                    </div>  
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 notshow" id="username_edit_<?php echo $data->id; ?>">
                        <input type="text" class="form-control" id="username_<?php echo $data->id; ?>" value="<?php echo $data->username; ?>">
                    </div>  
                </div>
            </td>
            <td></td>
        </tr>

        </tbody>
    </table>
</div>


