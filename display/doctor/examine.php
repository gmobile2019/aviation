<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
  $(document).ready(function(){
      var divID=$('input[name=next]').val();
      
     if(divID === "physical"){
         
         $('div#physical').show();
         $('div#physicalDetailed').hide();
         $('div#visual').hide();
         $('div#eye').hide();
         $('div#audio').hide();
         $('div#other').hide();
         $('div#approval').hide();
         
     }else if(divID === "physicalDetailed"){
         
         $('div#physicalDetailed').show();
         $('div#physical').hide();
         $('div#visual').hide();
         $('div#eye').hide();
         $('div#audio').hide();
         $('div#other').hide();
         $('div#approval').hide();
         
     }else if(divID === "visual"){
         
         $('div#visual').show();
         $('div#physicalDetailed').hide();
         $('div#physical').hide();
         $('div#eye').hide();
         $('div#audio').hide();
         $('div#other').hide();
         $('div#approval').hide();
         
     }else if(divID === "eye"){
         
         $('div#eye').show();
         $('div#physicalDetailed').hide();
         $('div#physical').hide();
         $('div#visual').hide();
         $('div#audio').hide();
         $('div#other').hide();
         $('div#approval').hide();
         
     }else if(divID === "audio"){
         
         $('div#audio').show();
         $('div#physicalDetailed').hide();
         $('div#physical').hide();
         $('div#visual').hide();
         $('div#eye').hide();
         $('div#other').hide();
         $('div#approval').hide();
         
     }else if(divID === "other"){
         
         $('div#other').show();
         $('div#physicalDetailed').hide();
         $('div#physical').hide();
         $('div#visual').hide();
         $('div#eye').hide();
         $('div#audio').hide();
         $('div#approval').hide();
         
     }else{
         
         $('div#approval').show();
         $('div#physicalDetailed').hide();
         $('div#physical').hide();
         $('div#visual').hide();
         $('div#eye').hide();
         $('div#audio').hide();
         $('div#other').hide();
     }
    
  });
</script>
<script src="<?php echo base_url() ?>styles/js/examine.js"></script>
<input type="hidden" name="next" value="<?php echo $next; ?>"/>
<div id="divform">
<!---------------------------------------------------------physical examination information------------------------------------------------------------>
<div id="physical">
    <h5 class="applicationHeader">Physical Examination (Appearance and Measurement)</h5>
    <form class="form-horizontal" id="physical" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <div class="row applicationDiv">
            <label for="pheight" class="col-4 form-control-label">Height (in or cm)</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
                <input type="text" class="form-control" name="pHeight" id="pheight" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="pWeight" class="col-4 form-control-label">Weight (lb or cm)</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <input type="text" class="form-control" name="pWeight" id="pWeight" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="pChestInsp" class="col-4 form-control-label">Chest Inspiration (in or cm)</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <input type="text" class="form-control" name="pChestInsp" id="pChestInsp" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="pChestExp" class="col-4 form-control-label">Chest Expiration (in or cm)</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <input type="text" class="form-control" name="pChestExp" id="pChestExp" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="pWaist" class="col-4 form-control-label">Waist (in or cm)</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <input type="text" class="form-control" name="pWaist" id="pWaist" required>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="pMSTD" class="col-4 form-control-label">Identify Marks,Scars,Tattoos,Deformities</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
                <textarea class="form-control" name="pMSTD" id="pMSTD" placeholder="Marks,Scars,Tattoos, Deformities" required></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="hairColor" class="col-4 form-control-label">Color of hair</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <textarea class="form-control" name="hairColor" id="hairColor" placeholder="Color of hair" required></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="eyesColor" class="col-4 form-control-label">Color of eyes</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <textarea class="form-control" name="eyesColor" id="eyesColor" placeholder="Color of eyes" required></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="physicalImpression" class="col-4 form-control-label">Physical Impression</label>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-4">
               <textarea class="form-control" name="physicalImpression" id="physicalImpression" placeholder="Physical Impression" required></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <button type="submit" class="offset-9 btn btn-link submit" id="physicalLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>
<!-------------------------------------------------Physical examination detailed Information------------------------------------------------------------>
<div id="physicalDetailed">
    <h5 class="applicationHeader">Physical Examination (Detailed)</h5>
    <form class="form-horizontal" id="physicalDetailed" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <div class="row applicationDiv">
        <label for="headNeck" class="col-4 form-control-label">Head and Neck</label>
        <div class="col-3">
            <select name="headNeck" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('headNeck','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('headNeck','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 headNeck">
            <textarea class="form-control" name="headNeckRemark" id="headNeckRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="mtt" class="col-4 form-control-label">Mouth , Throat and Teeth</label>
        <div class="col-3">
            <select name="mtt" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('mtt','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('mtt','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 mtt">
            <textarea class="form-control" name="mttRemark" id="mttRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="sinuses" class="col-4 form-control-label">Sinuses</label>
        <div class="col-3">
            <select name="sinuses" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('sinuses','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('sinuses','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 sinuses">
            <textarea class="form-control" name="sinusesRemark" id="sinusesRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="edv" class="col-4 form-control-label">Ears,Drums and Valsalva</label>
        <div class="col-3">
            <select name="edv" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('edv','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('edv','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 edv">
            <textarea class="form-control" name="edvRemark" id="edvRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="lungsChest" class="col-4 form-control-label">Lungs,Chest including breasts</label>
        <div class="col-3">
            <select name="lungsChest" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('lungsChest','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('lungsChest','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 lungsChest">
            <textarea class="form-control" name="lungsChestRemark" id="lungsChestRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="pheart" class="col-4 form-control-label">Heart Size,Auscultation</label>
        <div class="col-3">
            <select name="pheart" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('pheart','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('pheart','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 pheart">
            <textarea class="form-control" name="pheartRemark" id="pheartRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="vSystem" class="col-4 form-control-label">Vascular System; Varicose Veins</label>
        <div class="col-3">
            <select name="vSystem" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('vSystem','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('vSystem','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 vSystem">
            <textarea class="form-control" name="vSystemRemark" id="vSystemRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="pRate" class="col-4 form-control-label">Pulse Rate (Sitting,Standing)</label>
        <div class="col-3">
            <select name="pRate" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('pRate','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('pRate','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 pRate">
            <textarea class="form-control" name="pRateRemark" id="pRateRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="bp" class="col-4 form-control-label">Blood Pressure - Systolic/Diastolic Pulse Rate</label>
        <div class="col-3">
            <select name="bp" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('bp','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('bp','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 bp">
            <textarea class="form-control" name="bpRemark" id="bpRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="abdomenHernia" class="col-4 form-control-label">Abdomen and Hernia</label>
        <div class="col-3">
            <select name="abdomenHernia" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('abdomenHernia','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('abdomenHernia','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 abdomenHernia">
            <textarea class="form-control" name="abdomenHerniaRemark" id="abdomenHerniaRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="livSplGla" class="col-4 form-control-label">Liver,Spleen and Glands</label>
        <div class="col-3">
            <select name="livSplGla" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('livSplGla','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('livSplGla','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 livSplGla">
            <textarea class="form-control" name="livSplGlaRemark" id="livSplGlaRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="anusRectum" class="col-4 form-control-label">Anus and Rectum (Haemorrhoids, Fistula, Prostate)</label>
        <div class="col-3">
            <select name="anusRectum" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('anusRectum','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('anusRectum','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 anusRectum">
            <textarea class="form-control" name="anusRectumRemark" id="anusRectumRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="genito" class="col-4 form-control-label">Genito-Urinary System</label>
        <div class="col-3">
            <select name="genito" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('genito','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('genito','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 genito">
            <textarea class="form-control" name="genitoRemark" id="genitoRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="endocrine" class="col-4 form-control-label">Endocrine System</label>
        <div class="col-3">
            <select name="endocrine" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('endocrine','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('endocrine','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 endocrine">
            <textarea class="form-control" name="endocrineRemark" id="endocrineRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="upperLowerJoints" class="col-4 form-control-label">Upper limbs, Lower limbs and Joints</label>
        <div class="col-3">
            <select name="upperLowerJoints" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('upperLowerJoints','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('upperLowerJoints','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 upperLowerJoints">
            <textarea class="form-control" name="upperLowerJointsRemark" id="upperLowerJointsRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="spine" class="col-4 form-control-label">Spine & Spinal movements</label>
        <div class="col-3">
            <select name="spine" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('spine','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('spine','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 spine">
            <textarea class="form-control" name="spineRemark" id="spineRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="neurology" class="col-4 form-control-label">Neurology (Reflexes,equilibrium etc)</label>
        <div class="col-3">
            <select name="neurology" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('neurology','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('neurology','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 neurology">
            <textarea class="form-control" name="neurologyRemark" id="neurologyRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="skin" class="col-4 form-control-label">Skin</label>
        <div class="col-3">
            <select name="skin" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('skin','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('skin','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 skin">
            <textarea class="form-control" name="skinRemark" id="skinRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="psychiatric" class="col-4 form-control-label">Psychiatric</label>
        <div class="col-3">
            <select name="psychiatric" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('psychiatric','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('psychiatric','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 psychiatric">
            <textarea class="form-control" name="psychiatricRemark" id="psychiatricRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
            <a type="submit" class="col-3 btn btn-link" id="physicalDetailedLink"><i class="fa fa-arrow-circle-left fa-1x">Back</i></a>
            <button type="submit" class="offset-6 col-3 btn btn-link" id="physicalDetailedLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
</div>

<!--------------------------------------------------------visual---------------------------------------------------------------------------------------->
<div id="visual">
    <h5 class="applicationHeader">Eyes Visual Acuity Examination</h5>
    <form class="form-horizontal" id="visual" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <div class="row applicationDiv">
        <label for="LPLMF" class="col-4 form-control-label">Lids, Pupils, Lens, Media, Fundi</label>
        <div class="col-3">
            <select name="LPLMF" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('LPLMF','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('LPLMF','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 LPLMF">
            <textarea class="form-control" name="LPLMFRemark" id="LPLMFRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="glassesP" class="col-4 form-control-label">Does the candidate possess glasses?</label>
        <div class="col-3">
            <select name="glassesP" class="form-control" required>
                <option value=""></option>
                <option value="Yes" <?php echo set_select('glassesP','Yes')?>>Yes</option>
                <option value="No" <?php echo set_select('glassesP','No')?>>No</option>
            </select>
        </div>
        </div>
        <div class="row applicationDiv glassesP">
            <label for="dVisionWithG" class="col-4 form-control-label">Distant Vision with glasses (Standard Test Types)</label>
            <div class="col-3">
            <textarea class="form-control" name="dVisionWithGRight" id="dVisionWithGRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="dVisionWithGLeft" id="dVisionWithGLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="dVisionWithoutG" class="col-4 form-control-label">Distant Vision without glasses (Standard Test Types)</label>
            <div class="col-3">
            <textarea class="form-control" name="dVisionWithoutGRight" id="dVisionWithoutGRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="dVisionWithoutGLeft" id="dVisionWithoutGLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <div class="row applicationDiv glassesP">
            <label for="nVisionWithG" class="col-4 form-control-label">Near Vision with glasses (N type at 30cm to 50cm)</label>
            <div class="col-3">
            <textarea class="form-control" name="nVisionWithGRight" id="nVisionWithGRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="nVisionWithGLeft" id="nVisionWithGLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="nVisionWithoutG" class="col-4 form-control-label">Near Vision without glasses (N type at 30cm to 50cm)</label>
            <div class="col-3">
            <textarea class="form-control" name="nVisionWithoutGRight" id="nVisionWithoutGRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="nVisionWithoutGLeft" id="nVisionWithoutGLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <div class="row applicationDiv glassesP">
            <label for="accomodationWithG" class="col-4 form-control-label">Accommodation in cm (Near point 30cm) with glasses</label>
            <div class="col-3">
            <textarea class="form-control" name="accomodationWithGRight" id="accomodationWithGRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="accomodationWithGLeft" id="accomodationWithGLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="accomodationWithoutG" class="col-4 form-control-label">Accommodation in cm (Near point 30cm) without glasses</label>
            <div class="col-3">
            <textarea class="form-control" name="accomodationWithoutGRight" id="accomodationWithoutGRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="accomodationWithoutGLeft" id="accomodationWithoutGLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <div class="row applicationDiv glassesP">
            <label for="gprescription" class="col-4 form-control-label">Prescription of the glasses</label>
            <div class="col-8">
                <table class="table table-bordered table-striped table-hover">
                    <tr>
                        <th></th>
                        <th>S (Right)</th>
                        <th>C (Right)</th>
                        <th>A (Right)</th>
                        <th>S (Left)</th>
                        <th>C (Left)</th>
                        <th>A (Left)</th>
                    </tr>
                    <tr>
                        <td>Distant</td>
                        <td><input name="distantSright" class="form-control"/></td>
                        <td><input name="distantCright" class="form-control"/></td>
                        <td><input name="distantAright" class="form-control"/></td>
                        <td><input name="distantSleft" class="form-control"/></td>
                        <td><input name="distantCleft" class="form-control"/></td>
                        <td><input name="distantAleft" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Near</td>
                        <td><input name="nearSright" class="form-control"/></td>
                        <td><input name="nearCright" class="form-control"/></td>
                        <td><input name="nearAright" class="form-control"/></td>
                        <td><input name="nearSleft" class="form-control"/></td>
                        <td><input name="nearCleft" class="form-control"/></td>
                        <td><input name="nearAleft" class="form-control"/></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row applicationDiv">
        <label for="fieldVision" class="col-4 form-control-label">Field of vision by confrontation test</label>
        <div class="col-3">
            <select name="fieldVision" class="form-control" required>
                <option value=""></option>
                <option value="Normal" <?php echo set_select('fieldVision','Normal')?>>Normal</option>
                <option value="Abnormal" <?php echo set_select('fieldVision','Abnormal')?>>Abnormal</option>
            </select>
        </div>
        <div class="col-3 fieldVision">
            <textarea class="form-control" name="fieldVisionRemark" id="fieldVisionRemark" placeholder="Remarks"></textarea>
        </div>
        </div>
        <div class="row applicationDiv">
        <label for="powerConvergence" class="col-4 form-control-label">Power of convergence in cm</label>
        <div class="col-3">
            <input type="text" class="form-control" name="powerConvergence" id="powerConvergence" required/>
        </div>
        </div>
        <div class="row applicationDiv">
            <a type="submit" class="col-3 btn btn-link" id="visualLink"><i class="fa fa-arrow-circle-left fa-1x">Back</i></a>
            <button type="submit" class="offset-6 col-3 btn btn-link" id="visualLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>

<!--------------------------------------------------------eyes---------------------------------------------------------------------------------------->
<div id="eye">
    <h5 class="applicationHeader">Eyes Examination</h5>
    <form class="form-horizontal" id="eye" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <?php if($examination == NULL && ($ac_details[0]->licenceType <> 4 && $ac_details[0]->licenceType <> 5)){ ?>
        <div class="row applicationDiv">
            <label for="manifestHyper" class="col-4 form-control-label">Manifest Hypermetropia with +2.5 dioptre lens</label>
            <div class="col-3">
            <textarea class="form-control" name="manifestHyperRight" id="manifestHyperRight" placeholder="Right"></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="manifestHyperLeft" id="manifestHyperLeft" placeholder="Left"></textarea>
            </div>
        </div>
        <?php } 
        if($examination == NULL){
        ?>
        <div class="row applicationDiv">
            <label for="cPerceptionIshihara" class="col-4 form-control-label">Color Perception (Tested by pseudoisochromatic [Ishihara] plates)</label>
            <div class="col-3">
                <select name="cPerceptionIshihara" class="form-control">
                    <option value=""></option>
                    <option value="Normal" <?php echo set_select('cPerceptionIshihara','Normal')?>>Normal</option>
                    <option value="Abnormal" <?php echo set_select('cPerceptionIshihara','Abnormal')?>>Abnormal</option>
                </select>
            </div>
            <div class="col-3 cPerceptionIshihara">
                <textarea class="form-control" name="cPerceptionIshiharaRemark" id="cPerceptionIshiharaRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="cPerceptionLantem" class="col-4 form-control-label">Color Perception (Tested by an approved Colour Perception Lantem[giles-Archer,Martin])</label>
            <div class="col-3">
                <select name="cPerceptionLantem" class="form-control">
                    <option value=""></option>
                    <option value="Normal" <?php echo set_select('cPerceptionLantem','Normal')?>>Normal</option>
                    <option value="Abnormal" <?php echo set_select('cPerceptionLantem','Abnormal')?>>Abnormal</option>
                </select>
            </div>
            <div class="col-3 cPerceptionLantem">
                <textarea class="form-control" name="cPerceptionLantemRemark" id="cPerceptionLantemRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <?php } ?>
        <div class="row applicationDiv">
            <label for="measureHeterophoria" class="col-4 form-control-label">Measure of Heterophoria</label>
            <div class="col-8">
                <table class="table table-bordered table-striped table-hover">
                    <tr>
                        <th></th>
                        <th>Exophoria</th>
                        <th>Esophoria</th>
                        <th>Hyperphoria</th>
                    </tr>
                    <tr>
                        <td>Maddox Rod</td>
                        <td><input name="maddoxRodExo" class="form-control"/></td>
                        <td><input name="maddoxRodEso" class="form-control"/></td>
                        <td><input name="maddoxRodHyper" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>Maddox Wing</td>
                        <td><input name="maddoxWingExo" class="form-control"/></td>
                        <td><input name="maddoxWingEso" class="form-control"/></td>
                        <td><input name="maddoxWingHyper" class="form-control" /></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row applicationDiv">
            <a type="submit" class="col-3 btn btn-link" id="eyeLink"><i class="fa fa-arrow-circle-left fa-1x">Back</i></a>
            <button type="submit" class="offset-6 col-3 btn btn-link" id="eyeLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>

<!--------------------------------------------------------audio---------------------------------------------------------------------------------------->
<div id="audio">
    <h5 class="applicationHeader">Audio Examination</h5>
    <form class="form-horizontal" id="audio" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <div class="row applicationDiv">
            <label for="hearingDifficulty" class="col-4 form-control-label">Any hearing difficulty with conversational voice at 8 feet with back to examiner?</label>
            <div class="col-3">
                <select name="hearingDifficulty" class="form-control" required>
                    <option value=""></option>
                    <option value="Yes" <?php echo set_select('hearingDifficulty','Yes')?>>Yes</option>
                    <option value="No" <?php echo set_select('hearingDifficulty','No')?>>No</option>
                </select>
            </div>
            <div class="col-3 hearingDifficulty">
                <textarea class="form-control" name="hearingDifficultyRemark" id="hearingDifficultyRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="forcedWhisper" class="col-4 form-control-label">At what distance from the examiner can forced whisper be heard in each ear separately?</label>
            <div class="col-3">
            <textarea class="form-control" name="forcedWhisperRight" id="forcedWhisperRight" placeholder="Right" required></textarea>
            </div>
            <div class="col-3">
            <textarea class="form-control" name="forcedWhisperLeft" id="forcedWhisperLeft" placeholder="Left" required></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="audiometry" class="col-xs-12 col-sm-3 col-md-2 col-lg-2 form-control-label">Audiometry</label>
            <div class="col-xs-12 col-sm-9 col-md-10 col-md-10">
                <table class="table table-bordered table-striped table-hover">
                    <tr>
                        <th>Right</th>
                        <th>Frequency</th>
                        <th>Left</th>
                        <th>Max Permitted Loss</th>
                        <th>Remarks</th>
                    </tr>
                    <tr>
                        <td><input name="audiometry3000Right" class="form-control"/></td>
                        <td>3000</td>
                        <td><input name="audiometry3000Left" class="form-control"/></td>
                        <td>50</td>
                        <td><textarea name="audiometry3000Remark" class="form-control"></textarea></td>
                    </tr>
                    <tr>
                        <td><input name="audiometry2000Right" class="form-control"/></td>
                        <td>2000</td>
                        <td><input name="audiometry2000Left" class="form-control"/></td>
                        <td>35</td>
                        <td><textarea name="audiometry2000Remark" class="form-control"></textarea></td>
                    </tr>
                    <tr>
                        <td><input name="audiometry1000Right" class="form-control"/></td>
                        <td>1000</td>
                        <td><input name="audiometry1000Left" class="form-control"/></td>
                        <td>35</td>
                        <td><textarea name="audiometry1000Remark" class="form-control"></textarea></td>
                    </tr>
                    <tr>
                        <td><input name="audiometry500Right" class="form-control"/></td>
                        <td>500</td>
                        <td><input name="audiometry500Left" class="form-control"/></td>
                        <td>35</td>
                        <td><textarea name="audiometry500Remark" class="form-control"></textarea></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row applicationDiv">
            <a type="submit" class="col-3 btn btn-link" id="audioLink"><i class="fa fa-arrow-circle-left fa-1x">Back</i></a>
            <button type="submit" class="offset-6 col-3 btn btn-link" id="audioLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>
<!-----------------------------------------------------other------------------------------------------------------------------------------------->
<div id="other">
    <h5 class="applicationHeader">Other Examinations</h5>
    <form class="form-horizontal" id="other" role="form" method="POST">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <?php if ($bs_details[0]->gender === "Female"){ ?>
        <div class="row applicationDiv">
            <label for="menstruationDate" class="col-4 form-control-label">Last Menstruation Date</label>
            <div class="col-3">
                <input type="date" name="menstruationDate" class="form-control" placeholder="Mentruation Date" required>
            </div>
            <div class="col-3 menstruationDate">
                <textarea class="form-control" name="menstruationDateRemark" id="menstruationDateRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <?php } ?>
        <div class="row applicationDiv">
            <label for="ecgReport" class="col-4 form-control-label">Routine ECG Report</label>
            <div class="col-3">
                <textarea class="form-control" name="ecgReport" id="ecgReport" placeholder="Remarks"></textarea>
            </div>
            <div class="col-3">
                <input type="date" class="form-control" name="ecgNextDate" id="ecgNextDate" placeholder="Date of Next ECG" />
            </div>
            <div class="col-2">
                <input type="file" class="form-control" name="ecgReportFile" id="ecgReportFile"/>
            </div>
        </div>
        <?php if($examination == NULL){ ?>
            
        <div class="row applicationDiv">
            <label for="cxrReport" class="col-4 form-control-label">Routine CXR Report</label>
            <div class="col-3">
                <textarea class="form-control" name="cxrReport" id="cxrReport" placeholder="Routine CXR Report"></textarea>
            </div>
        </div>
        <?php } ?>
        <div class="row applicationDiv">
            <label for="audioReport" class="col-4 form-control-label">Routine Audio Report</label>
            <div class="col-3">
                <textarea class="form-control" name="audioReport" id="audioReport" placeholder="Routine Audio Report"></textarea>
            </div>
            <div class="col-3">
                <input type="date" class="form-control" name="audioNextDate" id="audioNextDate" placeholder="Date of Next Audio" />
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="urinalysis" class="col-4 form-control-label">Urinalysis</label>
            <div class="col-8">
                <table class="table table-bordered table-striped table-hover">
                    <tr>
                        <th>Protein</th>
                        <th>Glucose</th>
                        <th>Other</th>
                        <th>Remarks</th>
                    </tr>
                    <tr>
                        <td><textarea name="urinalysisProtein" class="form-control" required></textarea></td>
                        <td><textarea name="urinalysisGlucose" class="form-control" required></textarea></td>
                        <td><textarea name="urinalysisOther" class="form-control" required></textarea></td>
                        <td><textarea name="urinalysisRemarks" class="form-control" ></textarea></td>
                    </tr>
                </table>
            </div>
        </div>
        <?php if($examination == NULL){ ?>
        <div class="row applicationDiv">
            <label for="mhRemarks" class="col-4 form-control-label">Medical History Remarks</label>
            <div class="col-3">
                <textarea class="form-control" name="mhRemarks" id="mhRemarks" placeholder="Remarks"></textarea>
            </div>
        </div>
        <?php } ?>
        <div class="row applicationDiv">
            <a type="submit" class="col-3 btn btn-link" id="otherLink"><i class="fa fa-arrow-circle-left fa-1x">Back</i></a>
            <button type="submit" class="offset-6 col-3 btn btn-link" id="otherLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
    
</div>
<!------------------------------------------certification------------------------------------------------------------------------------>
<div id="approval">
    <form class="form-horizontal" role="form" method="POST" action="<?php echo base_url() .'index.php/Doctor/completeExamination/'.$applicationRef ?>">
        <input type="hidden" name="applicationRef" value="<?php echo $applicationRef; ?>"/>
        <input type="hidden" name="applicant" value="<?php echo $applicant; ?>"/>
        <div class="row applicationDiv">
            <label for="certStatus" class="col-4 form-control-label">Medical Certificate Status</label>
            <div class="col-3">
                <select name="certStatus" class="form-control" required>
                    <option value=""></option>
                    <option value="certified" <?php echo set_select('certStatus','certified')?>>Certified</option>
                    <option value="declined" <?php echo set_select('certStatus','declined')?>>Declined</option>
                </select>
            </div>
            <div class="col-3 LPLMF">
                <textarea class="form-control" name="certStatusRemark" id="certStatusRemark" placeholder="Remarks"></textarea>
            </div>
        </div>
        <div class="row applicationDiv">
            <label for="certStatus" class="col-4 form-control-label">Class</label>
            <div class="col-3">
                <input type="text" class="form-control" value="<?php echo $lType[0]->certClass; ?>" readonly/>
                <input type="hidden" name="certClass" value="<?php echo $lType[0]->certClass; ?>"/>
            </div>
        </div>
        <div class="row applicationDiv">
            <a type="submit" class="col-3 btn btn-link" id="approvalLink"><i class="fa fa-arrow-circle-left fa-1x">Back</i></a>
            <button type="submit" class="offset-6 col-3 btn btn-link" id="approvalLink">Next&nbsp;<i class="fa fa-arrow-circle-right fa-1x"></i></button>
        </div>
    </form>
</div>
</div>
<div class="modalAnimate"><!-- Place at bottom of page --></div>