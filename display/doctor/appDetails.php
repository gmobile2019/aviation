<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
  
  $(document).ready(function(){
      
      $('span#full_bg').hide();
      $('span#full_mh').hide();
      $('span#full_oc').hide();
      $('span#full_ph').hide();
      $('span#full_ac').hide();
      $('span#full_bs').hide();
      
      $('span#full_bs').click(function(){
            
            $('table#table_bs').slideDown();
            $('span#full_bs').hide();
            $('span#small_bs').show();
      });
      
      $('span#small_bs').click(function(){
            
            $('table#table_bs').slideUp();
            $('span#small_bs').hide();
            $('span#full_bs').show();
      });
      
      $('span#full_bg').click(function(){
            
            $('table#table_bg').slideDown();
            $('span#full_bg').hide();
            $('span#small_bg').show();
      });
      
      $('span#small_bg').click(function(){
            
            $('table#table_bg').slideUp();
            $('span#small_bg').hide();
            $('span#full_bg').show();
      });
      
      $('span#full_mh').click(function(){
            
            $('table#table_mh').slideDown();
            $('span#full_mh').hide();
            $('span#small_mh').show();
      });
      
      $('span#small_mh').click(function(){
            
            $('table#table_mh').slideUp();
            $('span#small_mh').hide();
            $('span#full_mh').show();
      });
      
      $('span#full_oc').click(function(){
            
            $('table#table_oc').slideDown();
            $('span#full_oc').hide();
            $('span#small_oc').show();
      });
      
      $('span#small_oc').click(function(){
            
            $('table#table_oc').slideUp();
            $('span#small_oc').hide();
            $('span#full_oc').show();
      });
      
      $('span#full_ph').click(function(){
            
            $('table#table_ph').slideDown();
            $('span#full_ph').hide();
            $('span#small_ph').show();
      });
      
      $('span#small_ph').click(function(){
            
            $('table#table_ph').slideUp();
            $('span#small_ph').hide();
            $('span#full_ph').show();
      });
      
      $('span#full_ac').click(function(){
            
            $('table#table_ac').slideDown();
            $('span#full_ac').hide();
            $('span#small_ac').show();
      });
      
      $('span#small_ac').click(function(){
            
            $('table#table_ac').slideUp();
            $('span#small_ac').hide();
            $('span#full_ac').show();
      });
  });
</script>
<div class="offset-3" style="padding-bottom: 10px">
        <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Doctor/retrieveNewApplication',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="applicationRef"></label>
                        <input type="text" class="form-control" name="applicationRef" id="applicationRef" placeholder="Application Reference" value="<?php echo $applicationRef; ?>" required/>
                    </div>
                    <div class="form-group">
                       <input type="submit" class="btn btn-success" name="retrieve"value="Retrieve Details" /> 
                    </div>
        
            <?php echo form_close(); ?>
</div>
<?php if($applicationRef <> NULL && $examination <> NULL){ ?>
<div id="divform">
<div class="col-12">
    <a class="btn btn-outline-success" href="<?php echo base_url()?>index.php/Doctor/examine/<?php echo $applicationRef?>">click here to examine patient</a>
</div>
<div id="div_bs">
    <div class="pull-right"><span class="glyphicon glyphicon-resize-full" style="cursor: pointer" id="full_bs"></span><span class="glyphicon glyphicon-resize-small" style="cursor: pointer" id="small_bs"></span>&nbsp;&nbsp;</div>
    <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;text-align: center">Basic Details</h5>
    <table class="table table-bordered table-hover table-condensed table-primary" id="table_bs">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Title</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->title; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;First Name</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->fname; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Middle Name</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->mname; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Surname</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->surname; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Email</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->email; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Mobile</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->mobile; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Gender</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->gender; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Age</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $this->User_model->dateDifference(date('Y-m-d'),$bs_details[0]->dob)->y; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Place of Birth</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->pob; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Nationality</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->nationality; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Permanent Address</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->paddress; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Postal Address</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bs_details[0]->postal; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="div_bg">
    <div class="pull-right"><span class="glyphicon glyphicon-resize-full" style="cursor: pointer" id="full_bg"></span><span class="glyphicon glyphicon-resize-small" style="cursor: pointer" id="small_bg"></span>&nbsp;&nbsp;</div>
    <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;text-align: center">Background Details</h5>
    <table class="table table-bordered table-hover table-condensed table-primary" id="table_bg">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever served in Armed Forces?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bg_details[0]->armedForce; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever had previous Pilot or Aircrew experience?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $bg_details[0]->pilotExperience; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="div_mh">
    <div class="pull-right"><span class="glyphicon glyphicon-resize-full" style="cursor: pointer" id="full_mh"></span><span class="glyphicon glyphicon-resize-small" style="cursor: pointer" id="small_mh"></span>&nbsp;&nbsp;</div>
    <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;text-align: center">Medical History Details</h5>
    <table class="table table-bordered table-hover table-condensed table-responsive" id="table_mh">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of frequent or severe headaches?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->fsHeadaches; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->fsHeadaches <> 'No'?$mh_details[0]->fsHeadachesRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Dizziness,Fainting or Unconciousness?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->dfu; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->dfu <> 'No'?$mh_details[0]->dfuRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Eye trouble?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->eyeTrouble; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->eyeTrouble <> 'No'?$mh_details[0]->eyeTroubleRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Hay Fever?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hayFever; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hayFever <> 'No'?$mh_details[0]->hayFeverRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Asthma?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->asthma; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->asthma <> 'No'?$mh_details[0]->asthmaRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Heart trouble?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->heartTrouble; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->heartTrouble <> 'No'?$mh_details[0]->heartTroubleRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of High or Low Blood Pressure?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->bloodPressure; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->bloodPressure <> 'No'?$mh_details[0]->bloodPressureRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Stomach trouble?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->stomachTrouble; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->stomachTrouble <> 'No'?$mh_details[0]->stomachTroubleRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Kidney Stones or Blood in Urine?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->ksbu; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->ksbu <> 'No'?$mh_details[0]->ksbuRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Sugar in Urine?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->su; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->su <> 'No'?$mh_details[0]->suRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Epilepsy?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->epilepsy; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->epilepsy <> 'No'?$mh_details[0]->epilepsyRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Nervous trouble of any sort?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->nervousTrouble; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->nervousTrouble <> 'No'?$mh_details[0]->nervousTroubleRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Motion Sickness requiring Drugs?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->motionSickness; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->motionSickness <> 'No'?$mh_details[0]->motionSicknessRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever been refused Life Insurance?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->lifeInsurance; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->lifeInsurance <> 'No'?$mh_details[0]->lifeInsuranceRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever been admitted to Hospital?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hospitalAdmission; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->hospitalAdmission <> 'No'?$mh_details[0]->hospitalAdmissionRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of any other Illness?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->otherIllness; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->otherIllness <> 'No'?$mh_details[0]->otherIllnessRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Head Injury?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->headInjury; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->headInjury <> 'No'?$mh_details[0]->headInjuryRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of Malaria/Tropical Diseases?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->MTDiseases; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->MTDiseases <> 'No'?$mh_details[0]->MTDiseasesRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever been treated for Alcoholism/Drug addiction?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->addiction; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->addiction <> 'No'?$mh_details[0]->addictionRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Any family history of Diabetes,Epilepsy or Tuberculosis?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->familyHistory; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->familyHistory <> 'No'?$mh_details[0]->familyHistoryRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you ever been refused a flying license?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->flyingLicense; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->flyingLicense <> 'No'?$mh_details[0]->flyingLicenseRemark:'N/A'; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have a history of being discharged form Armed Forces on Medical Grounds?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->discharge; ?></td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $mh_details[0]->discharge <> 'No'?$mh_details[0]->dischargeRemark:'N/A'; ?></td>
            </tr>
            <tr>
        </tbody>
    </table>
</div>
<div id="div_oc">
    <div class="pull-right"><span class="glyphicon glyphicon-resize-full" style="cursor: pointer" id="full_oc"></span><span class="glyphicon glyphicon-resize-small" style="cursor: pointer" id="small_oc"></span>&nbsp;&nbsp;</div>
    <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;text-align: center">Occupation/Employment Details</h5>
    <table class="table table-bordered table-hover table-condensed table-responsive" id="table_oc">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Occupation</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $oc_details[0]->occupation; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Employer</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $oc_details[0]->employer; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="div_ph">
    <div class="pull-right"><span class="glyphicon glyphicon-resize-full" style="cursor: pointer" id="full_ph"></span><span class="glyphicon glyphicon-resize-small" style="cursor: pointer" id="small_ph"></span>&nbsp;&nbsp;</div>
    <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;text-align: center">Health/Welfare Details</h5>
    <table class="table table-bordered table-hover table-condensed table-responsive" id="table_ph">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have an own General Practitioner?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gp; ?></td>
            </tr>
            
            <?php if($ph_details[0]->gp === 'Yes'){ ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Name of General Practitioner</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gpName; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Address of General Practitioner</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gpAddress; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Contact of General Practitioner</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->gpContact; ?></td>
            </tr>
            
            <?php } ?>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Do you have any medication presently being prescribed?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medication; ?></td>
            </tr>
            
            <?php if($ph_details[0]->medication === 'Yes'){ ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Description of prescribed medication</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medDescription; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Purpose of prescribed medication</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medPurpose; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Medication prescribing doctor</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->medDoctor; ?></td>
            </tr>
            
            <?php } ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;How many illness,accidents or disabilities suffered since the last Medical Examination</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ph_details[0]->noIllAccDis; ?></td>
            </tr>
            
            <?php 
                if($ph_details[0]->noIllAccDis >= 1){
                    
                    $illAccDisDate=explode('&&',$ph_details[0]->IllAccDisDate);
                    $illAccDisDetails=explode('&&',$ph_details[0]->IllAccDisDetails);
                    $illAccDisDoctorName=explode('&&',$ph_details[0]->IllAccDisDoctorName);
                    $illAccDisDoctorAddress=explode('&&',$ph_details[0]->IllAccDisDoctorAddress);
                ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Illness,accidents or disabilities</td>
                <td id="tdData">
                    <table class="table table-condensed table-responsive">
                        <tr>
                            <th class="bg-success">Date</th>
                            <th class="bg-success">Details</th>
                            <th class="bg-success">Doctor Name</th>
                            <th class="bg-success"> Doctor Address</th>
                        </tr>
                        <?php for($i=0;$i<$ph_details[0]->noIllAccDis;$i++){ ?>
                        <tr>
                            <td class="bg-success"><?php echo $illAccDisDate[$i]; ?></td>
                            <td class="bg-success"><?php echo $illAccDisDetails[$i]; ?></td>
                            <td class="bg-success"><?php echo $illAccDisDoctorName[$i]; ?></td>
                            <td class="bg-success"><?php echo $illAccDisDoctorAddress[$i]; ?></td>
                        </tr>
                        <?php }?>
                    </table>
                </td>
            </tr>
            
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="div_ac">
    <div class="pull-right"><span class="glyphicon glyphicon-resize-full" style="cursor: pointer" id="full_ac"></span><span class="glyphicon glyphicon-resize-small" style="cursor: pointer" id="small_ac"></span>&nbsp;&nbsp;</div>
    <h5 style="font-style: oblique;font-weight: bolder;text-decoration: underline;text-align: center">Aircraft Experience Details</h5>
    <table class="table table-bordered table-hover table-condensed table-responsive" id="table_ac">
        <tbody>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Type of Licence held or applied for</td>
                <td id="tdData">&nbsp;&nbsp;<?php $license=$this->User_model->getlicenceTypes($ac_details[0]->licenceType,NULL,NULL);echo $license[0]->licenceType; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;License Number</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->licenseNumber; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Expiry Date(s) of Last Medical Certificate(s)</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->expDteLMC; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Expiry Date(s) of 5 Year License(s)</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->expDte5YrL; ?></td>
            </tr>
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Total hours flown since last Medical Examination</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->hrsFlown; ?></td>
            </tr>
            
            <?php 
            if($ac_details[0]->noAircraftType > 0 || $ac_details[0]->noAircraftRoute > 0){

                $aircraftRoute=explode('&&',$ac_details[0]->aircraftRoute);
                $aircraftType=explode('&&',$ac_details[0]->aircraftType);
            ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Aircraft type(s) and route(s) flown since last medical examination</td>
                <td id="tdData">
                    <table class="table table-condensed table-responsive">
                        <tr>
                            <th class="bg-success">Type</th>
                            <th class="bg-success">Route</th>
                        </tr>
                        <?php for($i=0;$i<count($aircraftType);$i++){ ?>
                        <tr>
                            <td class="bg-success"><?php echo $aircraftType[$i]; ?></td>
                            <td class="bg-success"><?php echo $aircraftRoute[$i]; ?></td>
                        </tr>
                        <?php }?>
                    </table>
                </td>
            </tr>
            
            <?php } ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Have you been involved in an Aircraft Accident since Last Medical Examination?</td>
                <td id="tdData">&nbsp;&nbsp;<?php echo $ac_details[0]->airAccident; ?></td>
            </tr>
            
            <?php if($ac_details[0]->airAccident === 'Yes'){ 
                
                    $airAccLocation=explode('&&',$ac_details[0]->airAccidentLocation);
                    $airAccDate=explode('&&',$ac_details[0]->aircraftAccidentDate);
                ?>
            
            <tr>
                <td id="tdTitle">&nbsp;&nbsp;Aircraft accident date(s) and location(s) details</td>
                <td id="tdData">
                    <table class="table table-condensed table-responsive">
                        <tr>
                            <th class="bg-success">Date</th>
                            <th class="bg-success">Location</th>
                        </tr>
                        <?php for($i=0;$i<$ac_details[0]->noAircraftAccident;$i++){ ?>
                        <tr>
                            <td class="bg-success"><?php echo $airAccDate[$i]; ?></td>
                            <td class="bg-success"><?php echo $airAccLocation[$i]; ?></td>
                        </tr>
                        <?php }?>
                    </table>
                </td>
            </tr>
            
            <?php } ?>
        </tbody>
    </table>
</div>

<?php } ?>
</div>
