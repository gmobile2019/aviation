<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
      
           
        $("#applicationRef").autocomplete({
                  
            source: possibles
          });
            
            $('a').find('span#pdf').css({
                                color: '#000000'
                            });
    });
</script>
<div  class="row">
    <div class="offset-6">
        <?php echo anchor("Doctor/medical_certificate/".$appRef."/export",'<span id="pdf" class="fa fa-download fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pdf">export</span>'); ?>
    </div>
</div>
<div id="divform">
    <div clas="row">
        <div class="col-12">
            <table class="table table-bordered table-hover ">
                <thead>
                    <tr>
                        <th>&nbsp;&nbsp;<img src="<?php echo base_url().'img/aviation.png'?>" style="border-radius: 5px"/></th>
                        <th>&nbsp;&nbsp;<h3>Tanzania Civil Aviation Authority</h3>&nbsp;&nbsp;<h4>Aviation Medical Certificate</h4></th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-hover ">
                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;Name&nbsp;:&nbsp;<?php echo $bs_details[0]->fname.' '.$bs_details[0]->mname.' '.$bs_details[0]->surname; ?></td>
                        <td>&nbsp;&nbsp;Nationality&nbsp;:&nbsp;<?php echo $bs_details[0]->nationality; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Date of birth&nbsp;:&nbsp;<?php echo $bs_details[0]->dob; ?></td>
                        <td>&nbsp;&nbsp;Sex&nbsp;:&nbsp;<?php echo $bs_details[0]->gender; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Height&nbsp;:&nbsp;<?php echo $apm_details[0]->pHeight; ?></td>
                        <td>&nbsp;&nbsp;Weight&nbsp;:&nbsp;<?php echo $apm_details[0]->pWeight; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;Licence Type&nbsp;:&nbsp;<?php echo $lType[0]->licenceType; ?></td>
                        <td>&nbsp;&nbsp;Licence Number&nbsp;:&nbsp;<?php echo $ac_details[0]->licenseNumber; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="offset-4 col-8">
                    <h4>Advisory Information</h4>
                </div>
                <div class="col-12">
                    <table class="table table-bordered table-hover ">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Date of Examination</th>
                                <th>Date of next Examination</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>&nbsp;&nbsp;Electrocardiogram</td>
                                <td>&nbsp;&nbsp;<?php echo $ecg[0]->attendedOn; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $ecg[0]->ecgNextDate; ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;Audiogram</td>
                                <td>&nbsp;&nbsp;<?php echo $audio[0]->attendedOn; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $audio[0]->audioNextDate; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        <div class="col-12">
            <div class="row">
                <div class="offset-4 col-8">
                    <h4>Conditions</h4>
                </div>
                <div class="col-12">
                    <p style="font-style: italic;font-weight: bold;font-family: serif">
                        A holder of this licence shall not exercise the privileges of his/her license and related ratings at any time when the holder
                        is aware of any decrease in his medical fitness which might render the holder unable to safely and properly exercise those privileges.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="offset-4 col-8">
                    <h4>Validity Periods</h4>
                </div>
                <div class="col-12">
                    <table class="table table-bordered table-hover ">
                        <thead>
                            <tr>
                                <th>Class</th>
                                <th>Licence Type</th>
                                <th>Validity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>&nbsp;&nbsp;<?php echo $lType[0]->certClass?></td>
                                <td>&nbsp;&nbsp;<?php echo $lType[0]->licenceType; ?></td>
                                <td>&nbsp;&nbsp;<?php echo $validity; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <table class="table table-bordered table-hover ">
                        <thead>
                            <tr>
                                <th colspan="2">For all medical classes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>&nbsp;&nbsp;Electrocardiogram (ECG) Examination validity</td>
                                <td>&nbsp;&nbsp;<?php echo $ecg_validity; ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;Audiogram Examination validity</td>
                                <td>&nbsp;&nbsp;<?php echo $audio_validity; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>


